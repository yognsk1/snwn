<!DOCTYPE html>
<html lang="zxx">
	
	<head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="icon" href="img/fevicon.png" sizes="192x192">
		<style>
            .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);
}
			ul.tabs-6{
			margin: 0px;
			padding: 0px;
			list-style: none;
      background: linear-gradient(to left,#23428b 0,#1d094c 100%);
      border-radius:15px 15px 0 0;
		}
		ul.tabs-6 li{
			color: #fff;
			display: inline-block;
			padding: 10px 15px;
			cursor: pointer;
      
      margin:0 ;
		}


		ul.tabs-6 li.current{
			background: #007bff;
			color: #fff;
        
		}

    ul.tabs-6 li:nth-child(1).current{border-radius:15px 0px 0 0;}
    ul.tabs-6 li:nth-child(2).current{border-radius:0px 0px 0 0;}
    ul.tabs-6 li:nth-child(3).current{border-radius:0px 0px 0 0;}
    ul.tabs-6 li:nth-child(4).current{border-radius:0px 0px 0 0;}
    ul.tabs-6 li:nth-child(5).current{border-radius:0px 0px 0 0;}
    ul.tabs-6 li:nth-child(6).current{border-radius:0px 0px 0 0;}

		.tab-content-6{
			display: none;
			background: #f7f5f5;
			padding: 30px 15px;
      border: 1px solid #e8e2e2;
      border-radius:0 0px 15px 15px;
		}

		.tab-content-6.current{
			display: inherit;
		}
		</style>
	</head>
<body>
<?php include("header.php"); ?>
    <!-- Header End -->
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="" style=" background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);">
            <div class="hero__item set-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1 class="ttl-heading" style="color:#fff; font-weight: 900; font-size: 56px;">SEO Services for  <span class="rotate-text">Speak Performance</span></h1>
                                    <!--<h4 class="service-cont">Service And Solutions</h4>-->
								<p class="automate">Keep costs low and resouces lean with complimentary cloud scaling technology</p>
								
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
							<div class="row">
					<div class="col-md-4">
					<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Get Started</a>			
					</div>
					<div class="col-md-4">
								<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Contact Us</a>
				</div>
        <div class="col-md-4">
								&nbsp;
				</div>
				</div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
								<img src="img/autoscaling-hero-logo.png" >
                                
                            </div>
                        </div>
                    </div>
                </div>
				
					<a href="#" class="Btn"><img class="mus" src="img/mouse-accest-1.png"><i class="fa fa-arrow-down chev-dn"></i>
				
				</a>
								
            </div>
        </div>
    </section>

<div class=" seo-feature">
    <div class="container">

      <ul class="tabs-6">
        <li class="tab-link current" data-tab="tab-1">Online Reputation Management</li>
        <li class="tab-link" data-tab="tab-2">SEO</li>
        <li class="tab-link" data-tab="tab-3">SEM</li>
        <li class="tab-link" data-tab="tab-4">Social Media Marketing</li>
        <li class="tab-link" data-tab="tab-5">Content Marketing Services </li>
        <li class="tab-link" data-tab="tab-6">Display Advertising </li>
      </ul>
      <div id="tab-1" class="tab-content-6 current">
        <div class="row">
          <div class="col-md-4">
            <img src="https://www.snwntechsolution.com/images/dm-seo-page-img1.jpg">
          </div>
          <div class="col-md-8">
            <h3>Online Reputation Management</h3>
            <h4>Negativity can destroy you. We build and protect indestructible online reputations.</h4>
            <h6><strong>We build a spotless online reputation.</strong></h6>
            <p>The internet have gifted web users the ability to speak their mind online. While this is great for free speech, it also allows anyone to create negativity online. Our reputation management services aim to build and maintain a positive online reputation in search results and across different social channels while reducing and eliminating negative results and coverage. We do this through building your online presence across different channels, including press release distributors, web forums, and popular social media websites.</p>
          </div>
        </div>
        
      </div>
      <div id="tab-2" class="tab-content-6">
         Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <div id="tab-3" class="tab-content-6">
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      </div>
      <div id="tab-4" class="tab-content-6">
        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      </div>
      <div id="tab-5" class="tab-content-6">
        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      </div>
      <div id="tab-6" class="tab-content-6">
        Display Advertising 
      </div>
    </div>
  </div>
  <div class="snwn-benefits">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Brand Reputation Management</h2>
                <p>Don't fall victim to a false online narrative or competitor jealousy.</p>
                <div class="row">
                <div class="col-md-4">
                  <div class="project">
                      <div class="img">
                    <img src="img/automatic.png">
                          </div>
                    <div class="freatures-project">
                    <h5>Press Releases</h5>
                    <p>We'll submit newsworthy press releases on behalf of your company that will display prominently in search results.</p>
                    
                    </div>
                  </div>  
                </div>
                <div class="col-md-4">
                 <div class="project">
                     <div class="img">
                    <img src="img/cost-saving.png">

                     </div>
                    <div class="freatures-project">
                    <h5>Forums </h5>
                    <p>We search forums for negativity and also have our content experts make meaningful positive posts about your brand online.</p>
                    
                    </div>
                 </div>  
                </div>
                    
                    <div class="col-md-4">
                 <div class="project">
                     <div class="img">
                    <img src="img/schedulable.png">
                     </div>
                    <div class="freatures-project">
                    <h5>Competitor Websites</h5>
                    <p>Our team will identify competitors who have posted negativity about your brand and suppress the content through all channels.</p>
                    
                    </div>
                 </div>  
                </div>
                <div class="col-md-4">
                 <div class="project">
                     <div class="img">
                     <img src="img/fault-tolerant.png">
                     </div>
                    <div class="freatures-project">
                    <h5>Social Media</h5>
                    <p>We manage social media accounts for maximum visibility of positive feedback to increase visibility of your site. </p>
                    </div>
                 </div> 
            </div>


            <div class="col-md-4">
                 <div class="project">
                     <div class="img">
                    <img src="img/easy-to-audit.png">
                     </div>
                    <div class="freatures-project">
                    <h5>Review Sites</h5>
                    <p>Positive reviews on a number of review websites will help produce positive results and further protect your brand online.</p>
                    
                    </div>
                 </div>  
                </div>


                <div class="col-md-4">
                 <div class="project">
                     <div class="img">
                    <img src="img/schedulable.png">
                     </div>
                    <div class="freatures-project">
                    <h5>Blog Posts</h5>
                    <p>We publish articles on your website and other major sites to get interesting positive stories about your brand into search results.</p>
                    
                    </div>
                 </div>  
                </div>
                    </div>
                
                    </div>
            </div>
        </div>
    </div>
</div>



		
		<div class=" reviews-new" style="">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    
				<div class="col-md-6">
					<div class="">
						
						<h3>We help build positive reviews.</h3>
            <h4>Use online reviews to enhance your brand.</h4>
						<p>Auto Scaling (AS) provides you with a highly
efficient management policy for computing
resources. You can set the time to execute the
management policies regularly or create a real
time monitoring policy to manage the number
of CVM instances and deploy the environment
for the instances, ensuring that your business
runs smoothly.</p>
<!--<a href="#" class="clr-3">Get a free qoutes <i class="fa fa-arrow-right mg-right"></i></a>-->
					
					</div>
				</div>
				<div class="col-md-6">
                    <div class="img-overview">
					<img src="img/online-business-illustration.png">
                </div>

					
</div>
						
					
					</div>
				</div>
				
		</div>
		
	</div>
</div>





<div class="reviews-new" style="">
  <div class="container">
  <div class="row">
    <div class="col-md-12">
      
        <div class="row">
          <div class="col-md-6">
            <div class="img-overview">
  <img src="img/online-business-illustration.png">
        </div>
</div>
                      
          <div class="col-md-6">
            <div class="">
              
              <h3>Legal Removals</h3>
            <h4>Powerful Legal Framework</h4>
              <p>Auto Scaling (AS) provides you with a highly
  efficient management policy for computing
  resources. You can set the time to execute the
  management policies regularly or create a real
  time monitoring policy to manage the number
  of CVM instances and deploy the environment
  for the instances, ensuring that your business
  runs smoothly.</p>
  <!--<a href="#" class="clr-3">Get a free qoutes <i class="fa fa-arrow-right mg-right"></i></a>-->
            </div>
          </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="reviews-new" style="">
    <div class="container">
    <div class="row">
      <div class="col-md-12">
          <div class="row">        
            <div class="col-md-6">
              <div class="">
                
                <h3>Build Powerful Online Profiles for Visibility.</h3>
            <h4>Let our team optimize your business profiles.</h4>
                <p>Auto Scaling (AS) provides you with a highly
    efficient management policy for computing
    resources. You can set the time to execute the
    management policies regularly or create a real
    time monitoring policy to manage the number
    of CVM instances and deploy the environment
    for the instances, ensuring that your business
    runs smoothly.</p>
    <!--<a href="#" class="clr-3">Get a free qoutes <i class="fa fa-arrow-right mg-right"></i></a>-->
              </div>
            </div>
            <div class="col-md-6">
              <div class="img-overview">
    <img src="img/online-business-illustration.png">
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
<div class="faq-section">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12">
				<h3>Want to know more?</h3>
				<h2>Frequently Asked Questions</h2>
				<div class="row">
				<div class="col-md-6">
				<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd3">
    <label for="rd3" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
	<div class="tab">
    <input type="radio" name="accordion-2" id="rd4">
    <label for="rd4" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
  
</section>
					</div>
<div class="col-md-6">
<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd1">
    <label for="rd1" class="tab__label">What are the benefits of DevOps?</label>
    <div class="tab__content">
      <p>If you want to have only one tab open, you can use .</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd2">
    <label for="rd2" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
</section>
	</div>
				</div>	
			</div>
		</div>
	</div>
</div>
	<div class="know-more">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/smart-discussing-meeting-report-phone.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/engineers-looking-bridge-construction.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		</div>
	</div>
	<a href="#" class="clr-1">Read our Blog <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>
<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
		  <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
		  <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>
      <div class="col-md-7">
		  <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
	</div>
      </div>
    </div>
  </div>
</section>
<?php include("footer.php"); ?>
    <!-- Footer Section End -->
    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script>
    $(document).ready(function(){
	
	$('ul.tabs-6 li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs-6 li').removeClass('current');
		$('.tab-content-6').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})
  </script>
  <script type="text/javascript">
        $(document).ready(function() {
            var text = ['DevOps', 'Cloud Services', 'Site Services', 'Support']; //Text to be shown
            var index = 0; //Starting index for rotating text
            var changeText = setInterval(function() {
                if (index == text.length - 1)
                    index = 0;
                else
                    index++;
                $('.rotate-text').animate({
                    "opacity": "0",
                    "margin-left": "10px"
                }, 500, function() {
                    $('.rotate-text').text(text[index]);
                    $('.rotate-text').animate({
                        "opacity": "1",
                        "margin-left": "0"
                    }, 500);
                });

            }, 2000);
        });
    </script>
						<script>
						jQuery(document).ready(function($) {
    var tabwrapWidth= $('.tabs-wrapper').outerWidth();
    var totalWidth=0;
    jQuery("ul li").each(function() { 
      totalWidth += jQuery(this).outerWidth(); 
    });
    if(totalWidth > tabwrapWidth){
      $('.scroller-btn').removeClass('inactive');
    }
    else{
      $('.scroller-btn').addClass('inactive');
    }

    if($("#scroller").scrollLeft() == 0 ){
      $('.scroller-btn.left').addClass('inactive');
    }
    else{
       $('.scroller-btn.left').removeClass('inactive');
    }
		var liWidth= $('#scroller li').outerWidth();
		var liCount= $('#scroller li').length;
		var scrollWidth = liWidth * liCount;

				$('.right').on('click', function(){
          $('.nav-tabs').animate({scrollLeft: '+=200px'}, 300);
          console.log($("#scroller").scrollLeft() + " px");
				});
				
				$('.left').on('click', function(){
					$('.nav-tabs').animate({scrollLeft: '-=200px'}, 300);
				});
      scrollerHide()
     
      function scrollerHide(){
        var scrollLeftPrev = 0;
        $('#scroller').scroll(function () {
            var $elem=$('#scroller');
            var newScrollLeft = $elem.scrollLeft(),
                width=$elem.outerWidth(),
                scrollWidth=$elem.get(0).scrollWidth;
            if (scrollWidth-newScrollLeft==width) {
                $('.right.scroller-btn').addClass('inactive');
            }
            else{

                 $('.right.scroller-btn').removeClass('inactive');
            }
            if (newScrollLeft === 0) {
              $('.left.scroller-btn').addClass('inactive');
            }
            else{

                 $('.left.scroller-btn').removeClass('inactive');
            }
            scrollLeftPrev = newScrollLeft;
        });
      }
	});
						</script>
		
    
	<script>
	$(document).ready(function () {
  $(".at-title").click(function () {
    $(this).toggleClass("active").next(".at-tab").slideToggle().parent().siblings().find(".at-tab").slideUp().prev().removeClass("active");
  });
});
	</script>
<script>
const accordionItemHeaders = document.querySelectorAll(
  ".accordion-item-header"
);

accordionItemHeaders.forEach((accordionItemHeader) => {
  accordionItemHeader.addEventListener("click", (event) => {
    // Uncomment in case you only want to allow for the display of only one collapsed item at a time!

    const currentlyActiveAccordionItemHeader = document.querySelector(
      ".accordion-item-header.active"
    );
    if (
      currentlyActiveAccordionItemHeader &&
      currentlyActiveAccordionItemHeader !== accordionItemHeader
    ) {
      currentlyActiveAccordionItemHeader.classList.toggle("active");
      currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
    }
    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if (accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    } else {
      accordionItemBody.style.maxHeight = 0;
    }
  });
});
</script>


<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>