<!DOCTYPE html>
<html lang="zxx"><head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="icon" href="img/fevicon.png" sizes="192x192">	
<style>
    .header-content-animated-img h1 {
  color:#000;
  font-family:Oswald,sans-serif;
  font-size:60px;
  line-height:70px
}
.header-content-animated-img .btn-group-wrap {
  display:flex;
  gap:16px;
  text-align:center
}
.header-content-animated-img .content-wrapper {
  position:relative
}
.cm-hero-animated__animation {
  align-items:center;
  display:flex;
  height:100%;
  left:34.6%;
  position:absolute;
  right:-20px;
  top:0
}
.cm-hero-animated__animation img {
  height:auto;
  object-fit:contain;
  width:100%
}
.mosaic-container {
  left:17%;
  padding-bottom:70%;
  position:absolute;
  top:50%;
  transform:translateY(-50%);
  width:100%
}
.mosaic-container[data-inview=false] .mosaic-element {
  opacity:0;
  transform:scale(.9)
}
.mosaic-container[data-inview=false] .mosaic-element-child {
  opacity:0
}
.mosaic-element,
.mosaic-element-child {
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  position:absolute;
  transition:opacity .6s ease-in-out
}
.mosaic-element[data-index="1"] {
  background-color:#f4f5f7;
  height:16%;
  left:30%;
  top:13%;
  transition-delay:.5s;
  width:10%
}
.mosaic-element[data-index="2"] {
  background-color:#fff;
  height:57%;
  left:calc(40% + 3px);
  top:0;
  transition-delay:.1s;
  width:calc(60% - 3px)
}
.mosaic-element[data-index="3"] {
  background-color:#1fb3d9;
  height:calc(57% - 3px);
  left:0;
  top:calc(29% + 3px);
  transition-delay:1.1s;
  width:40%
}
.mosaic-element[data-index="4"] {
  background-color:#00213a;
  height:28.5%;
  left:calc(40% + 3px);
  top:calc(57% + 3px);
  transition-delay:.3s;
  width:calc(20% - 3px)
}
.mosaic-element[data-index="5"] {
  background-color:#f4f5f7;
  height:43%;
  left:calc(60% + 3px);
  top:calc(57% + 3px);
  transition-delay:.8s;
  width:calc(20% - 3px)
}
.mosaic-element[data-index="6"] {
  background:#1fb3d9;
  height:43%;
  left:calc(80% + 3px);
  top:calc(57% + 3px);
  transition-delay:1.3s;
  width:calc(20% - 3px)
}
.mosaic-element[data-index="7"] {
  background:#0abc8a;
  height:calc(14% - 3px);
  left:30%;
  top:calc(86% + 3px);
  transition-delay:1s;
  width:10%
}
.mosaic-element[data-index="2"] .mosaic-element-child[data-index="1"] {
  background-color:transparent;
  border-left:3px solid #fff;
  border-top:3px solid #fff;
  height:50%;
  left:33%;
  top:50%;
  transition-delay:.7s;
  width:35%
}
.mosaic-element[data-index="2"] .mosaic-element-child[data-index="2"] {
  background-color:#0abc8a;
  border-left:3px solid #fff;
  border-top:3px solid #fff;
  height:50%;
  left:calc(66% + 3px);
  top:50%;
  transition-delay:.2s;
  width:calc(33% + 3px)
}
.mosaic-container[data-inview=true] .mosaic-element {
  opacity:1;
  transform:scale(1)
}
.mosaic-container[data-inview=true] .mosaic-element-child {
  opacity:1
}
@media(max-width:400px) {
  #digital-transformation .header-banner-content-wrap .color-blue {
    display:block
  }
}
@media (max-width:767px) {
  .header-content-animated-img .content-wrapper {
    padding:0
  }
  .header-content-animated-img .header-banner-content-wrap {
    padding:65px 36px 0
  }
  .mosaic-container {
    left:0
  }
  .header-content-animated-img h1 {
    font-size:50px;
    line-height:60px
  }
}
@media (max-width:1023px) {
  .cm-hero-animated__animation {
    left:0;
    min-height:auto;
    order:1;
    padding-bottom:70%;
    position:relative;
    right:0;
    top:0;
    width:100%
  }
}
*,
:after,
:before {
  box-sizing:border-box
}
/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */
html {
  -webkit-text-size-adjust:100%;
  line-height:1.15
}
body {
  margin:0
}
main {
  display:block
}
h1 {
  font-size:2em;
  margin:.67em 0
}
hr {
  box-sizing:content-box;
  height:0;
  overflow:visible
}
pre {
  font-family:monospace,monospace;
  font-size:1em
}
a {
  background-color:transparent
}
abbr[title] {
  border-bottom:none;
  text-decoration:underline;
  text-decoration:underline dotted
}
b,
strong {
  font-weight:bolder
}
code,
kbd,
samp {
  font-family:monospace,monospace;
  font-size:1em
}
small {
  font-size:80%
}
img {
  border-style:none;
  height:auto;
  max-width:100%
}
button,
input,
optgroup,
select,
textarea {
  font-family:inherit;
  font-size:100%;
  line-height:1.15;
  margin:0
}
button,
input {
  overflow:visible
}
button,
select {
  text-transform:none
}
[type=button],
[type=reset],
[type=submit],
button {
  -webkit-appearance:button
}
[type=button]::-moz-focus-inner,
[type=reset]::-moz-focus-inner,
[type=submit]::-moz-focus-inner,
button::-moz-focus-inner {
  border-style:none;
  padding:0
}
[type=button]:-moz-focusring,
[type=reset]:-moz-focusring,
[type=submit]:-moz-focusring,
button:-moz-focusring {
  outline:1px dotted ButtonText
}
fieldset {
  padding:.35em .75em .625em
}
legend {
  box-sizing:border-box;
  color:inherit;
  display:table;
  max-width:100%;
  padding:0;
  white-space:normal
}
progress {
  vertical-align:baseline
}
textarea {
  overflow:auto
}
[type=checkbox],
[type=radio] {
  box-sizing:border-box;
  padding:0
}
[type=number]::-webkit-inner-spin-button,
[type=number]::-webkit-outer-spin-button {
  height:auto
}
[type=search] {
  -webkit-appearance:textfield;
  outline-offset:-2px
}
[type=search]::-webkit-search-decoration {
  -webkit-appearance:none
}
::-webkit-file-upload-button {
  -webkit-appearance:button;
  font:inherit
}
details {
  display:block
}
summary {
  display:list-item
}
[hidden],
template {
  display:none
}
.row-fluid {
  *zoom:1;
  width:100%
}
.row-fluid:after,
.row-fluid:before {
  content:"";
  display:table
}
.row-fluid:after {
  clear:both
}
.row-fluid [class*=span] {
  -webkit-box-sizing:border-box;
  -moz-box-sizing:border-box;
  -ms-box-sizing:border-box;
  box-sizing:border-box;
  display:block;
  float:left;
  margin-left:2.127659574%;
  *margin-left:2.0744680846382977%;
  min-height:1px;
  width:100%
}
.row-fluid [class*=span]:first-child {
  margin-left:0
}
.row-fluid .span12 {
  width:99.99999998999999%;
  *width:99.94680850063828%
}
.row-fluid .span11 {
  width:91.489361693%;
  *width:91.4361702036383%
}
.row-fluid .span10 {
  width:82.97872339599999%;
  *width:82.92553190663828%
}
.row-fluid .span9 {
  width:74.468085099%;
  *width:74.4148936096383%
}
.row-fluid .span8 {
  width:65.95744680199999%;
  *width:65.90425531263828%
}
.row-fluid .span7 {
  width:57.446808505%;
  *width:57.3936170156383%
}
.row-fluid .span6 {
  width:48.93617020799999%;
  *width:48.88297871863829%
}
.row-fluid .span5 {
  width:40.425531911%;
  *width:40.3723404216383%
}
.row-fluid .span4 {
  width:31.914893614%;
  *width:31.8617021246383%
}
.row-fluid .span3 {
  width:23.404255317%;
  *width:23.3510638276383%
}
.row-fluid .span2 {
  width:14.89361702%;
  *width:14.8404255306383%
}
.row-fluid .span1 {
  width:6.382978723%;
  *width:6.329787233638298%
}
.container-fluid {
  *zoom:1
}
.container-fluid:after,
.container-fluid:before {
  content:"";
  display:table
}
.container-fluid:after {
  clear:both
}
@media (max-width:767px) {
  .row-fluid {
    width:100%
  }
  .row-fluid [class*=span] {
    display:block;
    float:none;
    margin-left:0;
    width:auto
  }
}
@media (min-width:768px) and (max-width:1139px) {
  .row-fluid {
    *zoom:1;
    width:100%
  }
  .row-fluid:after,
  .row-fluid:before {
    content:"";
    display:table
  }
  .row-fluid:after {
    clear:both
  }
  .row-fluid [class*=span] {
    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
    -ms-box-sizing:border-box;
    box-sizing:border-box;
    display:block;
    float:left;
    margin-left:2.762430939%;
    *margin-left:2.709239449638298%;
    min-height:1px;
    width:100%
  }
  .row-fluid [class*=span]:first-child {
    margin-left:0
  }
  .row-fluid .span12 {
    width:99.999999993%;
    *width:99.9468085036383%
  }
  .row-fluid .span11 {
    width:91.436464082%;
    *width:91.38327259263829%
  }
  .row-fluid .span10 {
    width:82.87292817100001%;
    *width:82.8197366816383%
  }
  .row-fluid .span9 {
    width:74.30939226%;
    *width:74.25620077063829%
  }
  .row-fluid .span8 {
    width:65.74585634900001%;
    *width:65.6926648596383%
  }
  .row-fluid .span7 {
    width:57.182320438000005%;
    *width:57.129128948638304%
  }
  .row-fluid .span6 {
    width:48.618784527%;
    *width:48.5655930376383%
  }
  .row-fluid .span5 {
    width:40.055248616%;
    *width:40.0020571266383%
  }
  .row-fluid .span4 {
    width:31.491712705%;
    *width:31.4385212156383%
  }
  .row-fluid .span3 {
    width:22.928176794%;
    *width:22.874985304638297%
  }
  .row-fluid .span2 {
    width:14.364640883%;
    *width:14.311449393638298%
  }
  .row-fluid .span1 {
    width:5.801104972%;
    *width:5.747913482638298%
  }
}
@media (min-width:1280px) {
  .row-fluid {
    *zoom:1;
    width:100%
  }
  .row-fluid:after,
  .row-fluid:before {
    content:"";
    display:table
  }
  .row-fluid:after {
    clear:both
  }
  .row-fluid [class*=span] {
    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
    -ms-box-sizing:border-box;
    box-sizing:border-box;
    display:block;
    float:left;
    margin-left:2.564102564%;
    *margin-left:2.510911074638298%;
    min-height:1px;
    width:100%
  }
  .row-fluid [class*=span]:first-child {
    margin-left:0
  }
  .row-fluid .span12 {
    width:100%;
    *width:99.94680851063829%
  }
  .row-fluid .span11 {
    width:91.45299145300001%;
    *width:91.3997999636383%
  }
  .row-fluid .span10 {
    width:82.905982906%;
    *width:82.8527914166383%
  }
  .row-fluid .span9 {
    width:74.358974359%;
    *width:74.30578286963829%
  }
  .row-fluid .span8 {
    width:65.81196581200001%;
    *width:65.7587743226383%
  }
  .row-fluid .span7 {
    width:57.264957265%;
    *width:57.2117657756383%
  }
  .row-fluid .span6 {
    width:48.717948718%;
    *width:48.6647572286383%
  }
  .row-fluid .span5 {
    width:40.170940171000005%;
    *width:40.117748681638304%
  }
  .row-fluid .span4 {
    width:31.623931624%;
    *width:31.5707401346383%
  }
  .row-fluid .span3 {
    width:23.076923077%;
    *width:23.0237315876383%
  }
  .row-fluid .span2 {
    width:14.529914530000001%;
    *width:14.4767230406383%
  }
  .row-fluid .span1 {
    width:5.982905983%;
    *width:5.929714493638298%
  }
}
.clearfix {
  *zoom:1
}
.clearfix:after,
.clearfix:before {
  content:"";
  display:table
}
.clearfix:after {
  clear:both
}
.hide {
  display:none
}
.show {
  display:block
}
.hidden,
.invisible {
  visibility:hidden
}
.hidden {
  display:none
}
.hidden-desktop,
.visible-phone,
.visible-tablet {
  display:none!important
}
@media (max-width:767px) {
  .visible-phone {
    display:inherit!important
  }
  .hidden-phone {
    display:none!important
  }
  .hidden-desktop {
    display:inherit!important
  }
  .visible-desktop {
    display:none!important
  }
}
@media (min-width:768px) and (max-width:1139px) {
  .visible-tablet {
    display:inherit!important
  }
  .hidden-tablet {
    display:none!important
  }
  .hidden-desktop {
    display:inherit!important
  }
  .visible-desktop {
    display:none!important
  }
}
.content-wrapper {
  margin:0 auto;
  max-width:1240px;
  padding:0 20px
}
@media screen and (min-width:1380px) {
  .content-wrapper {
    padding:0
  }
}
.content-wrapper--vertical-spacing,
.dnd-section {
  padding:120px 20px
}
.dnd-section>.row-fluid {
  margin:0 auto;
  max-width:1200px
}
.dnd-section .dnd-column {
  padding:0 20px
}
@media (max-width:767px) {
  .dnd-section .dnd-column {
    padding:0
  }
  .content-wrapper--vertical-spacing,
  .dnd-section {
    padding-left:36px!important;
    padding-right:36px!important
  }
}
.product-list-item {
  display:block
}
.product-list-item a,
.product-list-item a:hover {
  color:#000
}
.product-list-item a:hover {
  text-decoration:none
}
.product-list-item p {
  margin:10px 0
}
.product-list-item__image {
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  display:block;
  width:100%
}
.product-list-item__image:before {
  content:"";
  display:block;
  padding-bottom:100%
}
.product-list-item__price span {
  font-weight:700
}
.flex-image {
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  display:block;
  max-width:100%
}
.hs-form.hs-form fieldset.form-columns-1 .hs-input {
  width:100%
}
@media (max-device-width:480px) and (min-device-width:320px),(max-width:400px) {
  form.hs-form.hs-form.hs-form .form-columns-2 .hs-form-field .hs-input,
  form.hs-form.hs-form.hs-form .form-columns-3 .hs-form-field .hs-input {
    width:100%
  }
}
html {
  font-size:24px
}
@media (max-width:767px) {
  html {
    font-size:18px
  }
}
@media (max-width:480px) {
  html {
    font-size:16px
  }
}
body {
  color:#494a52;
  font-family:Lato,sans-serif;
  line-height:1.4;
  word-break:break-word
}
html[lang^=ja] body,
html[lang^=ko] body,
html[lang^=zh] body {
  line-break:strict;
  word-break:break-all
}
p {
  font-size:1rem;
  margin:0 0 1.4rem
}
strong {
  font-weight:700
}
a {
  color:#0270e0;
  cursor:pointer;
  text-decoration:none
}
a:focus,
a:hover {
  text-decoration:underline
}
h1,
h2,
h3,
h4,
h5,
h6 {
  color:#494a52;
  font-family:Merriweather,serif;
  font-weight:700;
  margin:0 0 1.4rem
}
h1 {
  font-size:2.1rem
}
h2 {
  font-size:1.6rem
}
h3 {
  font-size:1.25rem
}
h4 {
  font-family:Lato,sans-serif;
  font-size:1.175rem;
  font-weight:400
}
h5 {
  font-size:1rem
}
h6 {
  font-size:.9rem
}
ol,
ul {
  margin:0 0 1.5rem
}
ol ol,
ol ul,
ul ol,
ul ul {
  margin:0
}
ul.no-list {
  list-style:none
}
code {
  vertical-align:bottom
}
blockquote {
  border-left:2px solid #a9a9a9;
  margin:0 0 1.4rem;
  padding-left:15px
}
hr {
  background-color:#ccc;
  border:none;
  color:#ccc;
  height:1px
}
sub,
sup {
  font-size:75%;
  line-height:0;
  position:relative;
  vertical-align:baseline
}
sup {
  top:-.5em
}
sub {
  bottom:-.25em
}
:focus {
  outline:auto;
  outline-color:green
}
.disable-focus-styles :focus {
  outline:none
}
.button,
button {
  background-color:#494a52;
  border:1px solid #494a52;
  border-radius:6px;
  color:#fff;
  cursor:pointer;
  display:inline-block;
  font-size:.92rem;
  font-weight:400;
  height:auto;
  line-height:1.1;
  margin:0;
  padding:15px 53px;
  position:relative;
  text-align:center;
  text-decoration:none;
  transition:all .15s linear;
  white-space:normal;
  width:auto;
  word-break:break-word
}
.button:disabled,
button:disabled {
  background-color:#d0d0d0;
  border-color:#d0d0d0;
  color:#e6e6e6
}
.button:focus,
.button:hover,
button:focus,
button:hover {
  background-color:#21222a;
  border-color:#21222a;
  color:#fff;
  text-decoration:none
}
.button:active,
button:active {
  background-color:#71727a;
  border-color:#71727a;
  color:#fff;
  text-decoration:none
}
.ui-button {
  border-radius:0!important;
  display:inline-block!important;
  font-family:Oswald!important;
  font-size:18px!important;
  font-weight:400!important;
  line-height:130%!important;
  padding:20px 46px!important;
  text-transform:uppercase!important;
  transition:all .3s ease
}
.ui-button,
.ui-button:hover {
  text-decoration:none!important
}
.ui-button.ui-button--primary {
  background:#fff!important;
  border:1px solid #1fb3d9!important;
  color:#1fb3d9!important
}
.ui-button.ui-button--primary:hover {
  background:#1fb3d9!important;
  border:1px solid #fff!important;
  color:#fff!important
}
.ui-button.ui-button--secondary {
  background:#1fb3d9!important;
  border:1px solid #1fb3d9!important;
  color:#fff!important
}
.ui-button.ui-button--secondary:hover {
  background:#fff!important;
  border:1px solid #1fb3d9!important;
  color:#1fb3d9!important
}
@media (max-width:767px) {
  body .ui-button {
    box-sizing:border-box!important;
    max-width:100%;
    text-align:center;
    width:100%
  }
}
form {
  font-family:Lato,sans-serif
}
.hs-form-field {
  margin-bottom:1.4rem
}
form label {
  display:block;
  margin-bottom:.35rem;
  padding-top:0;
  text-align:left;
  width:auto
}
form label,
form legend {
  color:#33475b;
  font-size:.875rem
}
.input {
  position:relative
}
input[type=email],
input[type=file],
input[type=number],
input[type=password],
input[type=tel],
input[type=text],
select,
textarea {
  background-color:#fff;
  border:1px solid #d1d6dc;
  border-radius:0;
  color:#33475b;
  display:inline-block;
  font-size:.875rem;
  padding:14.5px;
  width:100%
}
input[type=email]:focus,
input[type=file]:focus,
input[type=number]:focus,
input[type=password]:focus,
input[type=tel]:focus,
input[type=text]:focus,
select:focus,
textarea:focus {
  outline-color:rgba(82,168,236,.8)
}
fieldset {
  max-width:100%!important
}
::-webkit-input-placeholder {
  color:#bfbfbf
}
::-moz-placeholder {
  color:#bfbfbf
}
:-ms-input-placeholder {
  color:#bfbfbf
}
:-moz-placeholder {
  color:#bfbfbf
}
::placeholder {
  color:#bfbfbf
}
form .inputs-list {
  list-style:none;
  margin:0;
  padding:0
}
.inputs-list>li {
  display:block;
  margin:.7rem 0;
  padding:0;
  width:100%
}
.inputs-list input,
.inputs-list span {
  font-size:.875rem;
  vertical-align:middle
}
.hs-input[type=checkbox],
.hs-input[type=radio] {
  border:none;
  cursor:pointer;
  height:auto;
  line-height:normal;
  margin-right:.35rem;
  padding:0;
  width:auto
}
.hs-fieldtype-date .input .hs-dateinput:before {
  color:#33475b;
  content:"\01F4C5";
  position:absolute;
  right:10px;
  top:50%;
  -webkit-transform:translateY(-50%);
  -ms-transform:translateY(-50%);
  transform:translateY(-50%)
}
.fn-date-picker .pika-table thead th {
  color:#fff
}
.fn-date-picker td.is-today .pika-button {
  color:#343a40
}
.fn-date-picker td.is-selected .pika-button {
  background:#343a40;
  border-radius:0;
  box-shadow:none
}
.fn-date-picker td .pika-button:hover {
  background-color:#343a40!important;
  border-radius:0!important;
  color:#fff
}
input[type=file] {
  background-color:transparent;
  border:initial;
  box-shadow:none;
  line-height:normal;
  padding:initial
}
form .hs-richtext,
form .hs-richtext p {
  font-size:.875rem;
  margin:0 0 1.4rem
}
form .hs-richtext img {
  max-width:100%!important
}
form .header {
  background-color:transparent;
  border:none
}
.legal-consent-container .hs-form-booleancheckbox-display>span,
.legal-consent-container .hs-form-booleancheckbox-display>span p {
  font-size:.875rem;
  margin-left:1rem!important
}
.hs-form-required {
  color:red
}
.hs-input.invalid.error {
  border-color:#ef6b51
}
.hs-error-msg {
  color:#ef6b51;
  margin-top:.35rem
}
form .hs-button,
form input[type=submit] {
  background-color:#494a52;
  border:1px solid #494a52;
  border-radius:0;
  color:#fff;
  cursor:pointer;
  display:inline-block;
  font-size:.92rem;
  font-weight:400;
  height:auto;
  line-height:1.1;
  margin:0;
  padding:15px 53px;
  position:relative;
  text-align:center;
  text-decoration:none;
  transition:all .15s linear;
  white-space:normal;
  width:auto;
  word-break:break-word
}
form .hs-button:focus,
form .hs-button:hover,
form input[type=submit]:focus,
form input[type=submit]:hover {
  background-color:#21222a;
  border-color:#21222a;
  color:#fff
}
form .hs-button:active,
form input[type=submit]:active {
  background-color:#71727a;
  border-color:#71727a;
  color:#fff
}
.grecaptcha-badge {
  margin:0 auto
}
table {
  border:1px solid #dee2e6;
  margin-bottom:1.4rem;
  overflow-wrap:break-word
}
tbody+tbody {
  border-top:2px solid #dee2e6
}
td,
th {
  border:1px solid #dee2e6;
  padding:.75rem;
  vertical-align:top
}
thead td,
thead th {
  background-color:#343a40;
  border-bottom:2px solid #dee2e6;
  color:#fff;
  vertical-align:bottom
}
.header {
  background-color:#f8fafc;
  padding:33px 110px 33px 100px;
  position:fixed;
  top:0;
  transition:all .3s linear;
  width:100%;
  z-index:10
}
.header.scrolled {
  padding:3px 110px 2px 100px
}
.header__container {
  -webkit-box-pack:justify;
  -ms-flex-pack:justify;
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  justify-content:space-between
}
body .header__container.content-wrapper {
  max-width:1700px
}
.header__row-1,
.header__row-2 {
  -webkit-box-align:center;
  -ms-flex-align:center;
  -webkit-box-pack:end;
  -ms-flex-pack:end;
  align-items:center;
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  justify-content:flex-end;
  width:100%
}
.header__row-1 {
  padding-top:0
}
.header__skip {
  height:1px;
  left:-1000px;
  overflow:hidden;
  position:absolute;
  text-align:left;
  top:-1000px;
  width:1px
}
.header__skip:active,
.header__skip:focus,
.header__skip:hover {
  height:auto;
  left:0;
  overflow:visible;
  top:0;
  width:auto
}
.header__logo {
  -webkit-box-align:center;
  -ms-flex-align:center;
  align-items:center;
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  height:auto;
  margin-right:auto;
  max-width:200px;
  overflow:hidden
}
.header__logo img {
  max-width:100%
}
.header__logo .logo-company-name {
  font-size:28px;
  margin-top:7px
}
.header__logo--main {
  padding-top:0
}
.header__search {
  padding:0 22px;
  width:auto
}
.header__search .hs-search-field__input {
  background-color:#fff;
  background-image:url("data:image/svg+xml;charset=utf-8,%3Csvg width='20' height='20' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M8.333 14.167a5.833 5.833 0 1 0 0-11.667 5.833 5.833 0 0 0 0 11.667ZM17.5 17.5l-5-5' stroke='%234F4F4F' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E");
  background-position:center right 15px;
  background-repeat:no-repeat;
  border:1px solid #d1d6dc;
  border-radius:0;
  color:#494a52;
  font-family:Lato,serif;
  font-size:22px;
  font-weight:300;
  height:45px;
  padding:0 15px
}
.header__search .hs-search-field--open .hs-search-field__input {
  background-color:#fff;
  border:1px solid #d1d6dc;
  border-bottom:none;
  border-radius:0
}
.header__search .hs-search-field__input::placeholder {
  color:transparent
}
.header__search .hs-search-field--open .hs-search-field__suggestions {
  background-color:#fff;
  border:1.79px solid #d1d6dc;
  border-radius:0;
  border-top:-2px solid #fff;
  position:absolute;
  width:100%;
  z-index:10
}
.header__search .hs-search-field__suggestions li {
  border-top:1px solid #d1d6dc;
  font-family:Lato,serif;
  font-size:22px
}
.header__search .hs-search-field__suggestions li a {
  color:#494a52;
  padding:3px 15px;
  text-decoration:none;
  transition:background-color .3s
}
.header__search .hs-search-field__suggestions #results-for {
  display:none
}
.header__language-switcher {
  cursor:pointer;
  padding-right:35px
}
.header__language-switcher .lang_switcher_class {
  position:static
}
.header__language-switcher .lang_list_class {
  border:1px solid #d1d6dc;
  border-radius:6px;
  color:#494a52;
  display:block;
  font-family:Lato,serif;
  font-size:.8rem;
  left:calc(100% - 24px);
  min-width:100px;
  opacity:0;
  padding-top:0;
  text-align:left;
  top:100%;
  transition:opacity .3s;
  visibility:hidden
}
.header__language-switcher:hover .lang_list_class {
  opacity:1;
  transition:opacity .3s;
  visibility:visible
}
.header__language-switcher .lang_list_class:before {
  border-bottom-color:#d1d6dc;
  left:70%;
  top:-23px
}
.header__language-switcher .lang_list_class:after {
  left:70%;
  top:-22px
}
.header__language-switcher .lang_list_class.first-active:after {
  border-bottom-color:#ebeff3;
  top:-22px;
  transition:.3s
}
.header__language-switcher .lang_list_class li {
  border:none;
  font-size:18px;
  padding:10px 15px
}
.header__language-switcher .lang_list_class li:first-child {
  border-radius:6px 6px 0 0;
  border-top:none
}
.header__language-switcher .lang_list_class li:last-child {
  border-bottom:none;
  border-radius:0 0 6px 6px
}
.header__language-switcher .lang_list_class li:hover {
  background-color:#ebeff3;
  transition:background-color .3s
}
.header__language-switcher .lang_list_class li a {
  color:#494a52;
  font-family:Lato,serif
}
.header__language-switcher .lang_list_class li a:hover {
  color:#494a52
}
.header__language-switcher--label {
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  position:relative
}
.header__language-switcher--label-current {
  -webkit-box-align:center;
  -ms-flex-align:center;
  align-items:center;
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  font-size:18px;
  margin-bottom:5px;
  margin-left:10px
}
.header__language-switcher--label-current:after {
  border-left:4px solid transparent;
  border-right:4px solid transparent;
  border-top:6px solid #494a52;
  content:"";
  display:block;
  height:0;
  margin-left:10px;
  margin-top:3px;
  width:0
}
#nav-toggle {
  display:none
}
.header__menu--flex {
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex
}
#nav-col-hidden {
  display:none
}
@media (max-width:1024px) {
  .header__container {
    -webkit-box-orient:vertical;
    -webkit-box-direction:normal;
    -ms-flex-direction:column;
    flex-direction:column;
    padding:20px 0 0
  }
  .header__container form {
    max-width:100%
  }
  .header__logo {
    margin:0 auto;
    width:100%
  }
  .header__language-switcher,
  .header__navigation,
  .header__search {
    display:none;
    width:100%
  }
  .header__language-switcher.open,
  .header__navigation.open,
  .header__search.open {
    background-color:#f8fafc;
    display:block;
    height:calc(100vh - 72px);
    left:0;
    position:absolute;
    right:0;
    top:72px;
    z-index:2
  }
  .header__close--toggle,
  .header__language-switcher--toggle,
  .header__navigation--toggle,
  .header__search--toggle {
    cursor:pointer;
    margin:0 5vw;
    position:relative
  }
  .header__language-switcher--toggle.hide,
  .header__search--toggle.hide {
    display:none
  }
  .header__language-switcher--toggle.open,
  .header__navigation--toggle.open,
  .header__search--toggle.open {
    display:block;
    margin-left:0;
    margin-right:auto
  }
  .header__language-switcher--toggle.open:after,
  .header__navigation--toggle.open:after,
  .header__search--toggle.open:after {
    display:block;
    word-break:normal
  }
  .header__language-switcher--toggle:after,
  .header__navigation--toggle:after,
  .header__search--toggle:after {
    display:none;
    font-size:26px;
    font-weight:600;
    left:40px;
    position:absolute;
    text-transform:uppercase;
    top:-10px
  }
  .header__column {
    position:relative
  }
  .header__row-1 {
    padding-top:0
  }
  .header__row-2 {
    -webkit-box-pack:center;
    -ms-flex-pack:center;
    justify-content:center;
    padding:30px
  }
  .header__navigation--toggle {
    background-image:url(https://8725880.fs1.hubspotusercontent-na1.net/hubfs/8725880/menu-icon.svg);
    background-position:50%;
    background-repeat:no-repeat;
    background-size:contain;
    cursor:pointer;
    height:25px;
    width:25px
  }
  .header__navigation--toggle:after {
    content:"Menu"
  }
  .header__language-switcher--toggle {
    background-image:url(//static.hsappstatic.net/cos-LanguageSwitcher/static-1.1/img/globe.png);
    background-size:cover;
    height:25px;
    width:25px
  }
  .header__language-switcher--toggle:after {
    content:"Language"
  }
  .header__language-switcher {
    border-top:2px solid #ced4db;
    padding-left:30px;
    padding-right:0
  }
  .header__language-switcher .lang_list_class {
    background-color:inherit;
    border:none;
    border-radius:0;
    box-shadow:unset;
    display:block;
    left:30px;
    opacity:1;
    padding:0 30px;
    top:0;
    visibility:visible
  }
  .header__language-switcher .lang_list_class li {
    background-color:inherit;
    font-size:22px
  }
  .header__language-switcher--label-current {
    display:none
  }
  .header__language-switcher .globe_class {
    background-image:none
  }
  .header__language-switcher .lang_list_class li:hover {
    background-color:inherit
  }
  .header__language-switcher .lang_list_class:after,
  .header__language-switcher .lang_list_class:before {
    content:none
  }
  .header__search--toggle {
    background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxwYXRoIGQ9Ik05LjEzOSAxNS44OTNhNi43NjEgNi43NjEgMCAwIDEtNi43NTUtNi43NTQgNi43NjMgNi43NjMgMCAwIDEgNi43NTUtNi43NTUgNi43NjIgNi43NjIgMCAwIDEgNi43NTQgNi43NTUgNi43NiA2Ljc2IDAgMCAxLTYuNzU0IDYuNzU0TTkuMTM5IDBjNS4wMzkgMCA5LjEzNyA0LjEgOS4xMzcgOS4xNCAwIDIuMDktLjcwNSA0LjAxOC0xLjg5IDUuNTZsNy4yNjUgNy4yNjVhMS4xOTIgMS4xOTIgMCAwIDEtMS42ODYgMS42ODZMMTQuNyAxNi4zODVhOS4wOTMgOS4wOTMgMCAwIDEtNS41NjEgMS44OTFDNC4wOTkgMTguMjc2IDAgMTQuMTc4IDAgOS4xNCAwIDQuMSA0LjEgMCA5LjEzOSAwWiIgaWQ9ImEiLz48L2RlZnM+PHVzZSBmaWxsPSIjNDk0QTUyIiB4bGluazpocmVmPSIjYSIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9zdmc+);
    background-size:cover;
    height:25px;
    width:25px
  }
  .header__search--toggle:after {
    content:"Search"
  }
  .header__search {
    -webkit-box-ordinal-group:2;
    -ms-flex-order:1;
    border-top:2px solid #ced4db;
    order:1;
    padding:30px
  }
  .header__search .hs-search-field__input {
    padding-left:15px
  }
  .header__search .hs-search-field__suggestions li {
    padding:10px 0
  }
  .header__close--toggle {
    background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMTkiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMiAtMSkiIHN0cm9rZT0iIzQ5NEE1MiIgc3Ryb2tlLXdpZHRoPSIzIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxyZWN0IHRyYW5zZm9ybT0icm90YXRlKC00NSAxMCAxMC41KSIgeD0iLS41IiB5PSIxMC41IiB3aWR0aD0iMjEiIGhlaWdodD0iMSIgcng9Ii41Ii8+PHJlY3QgdHJhbnNmb3JtPSJyb3RhdGUoNDUgMTAgMTAuNSkiIHg9Ii0uNSIgeT0iMTAuNSIgd2lkdGg9IjIxIiBoZWlnaHQ9IjEiIHJ4PSIuNSIvPjwvZz48L3N2Zz4=);
    background-repeat:no-repeat;
    background-size:110%;
    display:none;
    height:25px;
    margin-right:0;
    width:25px
  }
  .header__close--toggle.show {
    display:block
  }
  .header__search--toggle {
    display:none
  }
  .header__container {
    align-items:center;
    flex-direction:row;
    justify-content:space-between;
    padding:0
  }
  .header__logo {
    max-width:120px;
    order:2
  }
  .header {
    padding:22px 36px
  }
  .header.scrolled {
    padding:11px 36px
  }
  .header__row-2 {
    padding:0
  }
  .header__close--toggle,
  .header__language-switcher--toggle,
  .header__navigation--toggle,
  .header__search--toggle {
    margin:0
  }
  #nav-col {
    order:1
  }
  #nav-col-hidden {
    display:block;
    opacity:0;
    order:3;
    visibility:hidden
  }
  .header__column {
    position:static
  }
  #nav-col .header--toggle.header__navigation--toggle.hide.open {
    display:none
  }
  #nav-col-hidden .header__navigation--toggle.hide {
    display:block
  }
}
.footer {
  background-color:#f8fafc;
  text-align:center
}
.footer__top {
  padding:107px 0 100px;
  position:relative
}
.footer__top:before {
  bottom:0;
  content:"";
  left:0;
  margin-left:calc(50% - 50vw);
  position:absolute;
  top:0;
  width:100vw;
  z-index:-1
}
.footer__address {
  font-size:10px;
  padding-top:15px
}
.footer__navigation {
  margin-top:60px
}
.footer__navigation .navigation-primary ul {
  align-items:center;
  display:flex;
  flex-wrap:wrap;
  justify-content:center
}
.footer__navigation .navigation-primary li {
  border-right:1px solid #595959;
  margin:10px 0;
  padding:0 30px!important
}
.footer__navigation .navigation-primary li:last-child {
  border:0
}
.footer__navigation .navigation-primary li a {
  display:block;
  font-size:14px;
  line-height:150%;
  width:160px
}
.footer__bottom {
  align-items:center;
  display:flex;
  justify-content:space-between;
  padding:24px 0
}
.footer__copyright {
  font-size:14px;
  margin:.5rem 0
}
.footer__links {
  display:flex;
  font-size:14px;
  justify-content:center
}
.footer__link {
  padding:8px 15px
}
@media(min-width:1024px) {
  .footer__bottom>div:first-child,
  .footer__bottom>div:nth-child(3) {
    width:280px
  }
  .social-links {
    justify-content:flex-end
  }
}
@media(max-width:1023px) {
  .footer__navigation .navigation-primary li {
    border:0!important;
    margin:0
  }
  .footer__navigation .navigation-primary li a {
    text-align:center;
    width:auto
  }
}
@media (max-width:767px) {
  .footer__bottom {
    display:flex;
    flex-direction:column
  }
  .footer__top {
    padding:70px 0 50px
  }
  .footer__navigation {
    margin-top:30px
  }
  .footer__navigation .navigation-primary li {
    border:0!important;
    margin:0
  }
  .footer__navigation .navigation-primary li a {
    text-align:center;
    width:auto
  }
  .footer__copyright {
    order:3
  }
  .footer__links {
    order:2
  }
  #hs_cos_wrapper_footer_social {
    margin-bottom:15px;
    order:1
  }
}
.hs-menu-wrapper ul {
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  -ms-flex-wrap:wrap;
  flex-wrap:wrap;
  list-style:none;
  margin:0;
  padding-left:0
}
.hs-menu-wrapper.hs-menu-flow-horizontal .hs-menu-children-wrapper {
  -webkit-box-orient:vertical;
  -webkit-box-direction:normal;
  -ms-flex-direction:column;
  flex-direction:column
}
@media (max-width:767px) {
  .hs-menu-wrapper.hs-menu-flow-horizontal ul {
    -webkit-box-orient:vertical;
    -webkit-box-direction:normal;
    -ms-flex-direction:column;
    flex-direction:column
  }
}
.hs-menu-wrapper.hs-menu-flow-vertical ul {
  -webkit-box-orient:vertical;
  -webkit-box-direction:normal;
  -ms-flex-direction:column;
  flex-direction:column
}
.hs-menu-wrapper.hs-menu-flow-vertical.flyouts ul {
  display:-webkit-inline-box;
  display:-ms-inline-flexbox;
  display:inline-flex
}
@media (max-width:767px) {
  .hs-menu-wrapper.hs-menu-flow-vertical ul {
    display:-webkit-box;
    display:-ms-flexbox;
    display:flex
  }
}
.hs-menu-wrapper.flyouts .hs-item-has-children {
  position:relative
}
.hs-menu-wrapper.flyouts .hs-menu-children-wrapper {
  left:-9999px;
  opacity:0;
  position:absolute
}
.hs-menu-wrapper.flyouts .hs-menu-children-wrapper a {
  display:block;
  white-space:nowrap
}
.hs-menu-wrapper.hs-menu-flow-horizontal.flyouts .hs-item-has-children:hover>.hs-menu-children-wrapper {
  left:0;
  opacity:1;
  top:100%
}
.hs-menu-wrapper.hs-menu-flow-vertical.flyouts .hs-item-has-children:hover>.hs-menu-children-wrapper {
  left:100%;
  opacity:1;
  top:0
}
@media (max-width:767px) {
  .hs-menu-wrapper.flyouts .hs-menu-children-wrapper,
  .hs-menu-wrapper.hs-menu-flow-horizontal.flyouts .hs-item-has-children:hover>.hs-menu-children-wrapper,
  .hs-menu-wrapper.hs-menu-flow-vertical.flyouts .hs-item-has-children:hover>.hs-menu-children-wrapper {
    left:0;
    opacity:1;
    position:relative;
    top:auto
  }
}
.shopping-cart {
  --transition-duration:0.2s;
  height:100%;
  left:0;
  overflow:hidden;
  pointer-events:none;
  position:fixed;
  top:0;
  width:100%
}
.shopping-cart__backdrop {
  background:rgba(0,0,0,.5);
  height:100%;
  left:0;
  opacity:0;
  position:absolute;
  top:0;
  transition:opacity var(--transition-duration);
  width:100%
}
.shopping-cart__content {
  background:#000;
  color:#fff;
  display:grid;
  grid-template-rows:auto 1fr auto;
  height:100%;
  max-width:85vw;
  position:absolute;
  right:0;
  top:0;
  transform:translateX(100%);
  transition:transform var(--transition-duration);
  width:500px
}
.shopping-cart--inline .shopping-cart,
.shopping-cart--open .shopping-cart {
  pointer-events:auto
}
.shopping-cart--inline .shopping-cart__backdrop {
  display:none
}
.shopping-cart--open .shopping-cart__backdrop {
  opacity:1
}
.shopping-cart--open .shopping-cart__content {
  transform:translateX(0)
}
.shopping-cart--inline .shopping-cart,
.shopping-cart--inline .shopping-cart__content {
  max-width:none;
  position:static;
  transform:none;
  width:100%
}
.shopping-cart h3 {
  border-bottom:1px solid #444;
  color:inherit;
  margin:0;
  padding:20px var(--container-h-padding)
}
.shopping-cart ul {
  margin:0;
  overflow:auto;
  padding:0 var(--container-h-padding)
}
.shopping-cart__item {
  grid-column-gap:20px;
  align-items:start;
  border-bottom:1px solid #444;
  display:grid;
  grid-template-areas:"image link    price" "image actions actions";
  grid-template-columns:auto 1fr auto;
  padding:var(--container-h-padding) 0
}
.shopping-cart__item__image {
  grid-area:image;
  max-width:25vw;
  width:100px
}
.shopping-cart__item__image:before {
  content:"";
  display:block;
  padding-bottom:100%
}
.shopping-cart__item a {
  color:inherit;
  font-weight:700;
  grid-area:link
}
.shopping-cart__item a:active,
.shopping-cart__item a:focus,
.shopping-cart__item a:hover {
  color:inherit
}
.shopping-cart__item__link {
  -webkit-line-clamp:2;
  -webkit-box-orient:vertical;
  display:-webkit-box;
  overflow:hidden;
  text-overflow:ellipsis
}
.shopping-cart__item__actions {
  align-items:center;
  align-self:end;
  display:inline-flex;
  grid-area:actions
}
.shopping-cart__item__actions button {
  background:#666;
  border-color:#666;
  border-radius:3px;
  height:24px;
  padding:0;
  width:24px
}
.shopping-cart__item__price {
  grid-area:price
}
.shopping-cart__item__quantity {
  line-height:1;
  margin:0 1em;
  min-width:1em;
  text-align:center
}
.shopping-cart__item__actions a {
  font-size:150%;
  line-height:1;
  margin-left:1em
}
.shopping-cart__footer {
  background:#444;
  padding:var(--container-h-padding)
}
.shopping-cart__checkout a {
  border-radius:0;
  display:block
}
#hs_login_reset {
  float:left
}
.hs-membership-global-error {
  color:#f2545b;
  text-align:center
}
#hs-register-widget-password,
#hs-reset-widget-password {
  margin-bottom:8px
}
.password-requirements {
  color:#425b76;
  line-height:1.5em
}
#hs-membership-form a {
  float:right
}
#hs-membership-form label {
  display:inline
}
.body-wrapper {
  overflow:hidden
}
.body-container-wrapper {
  margin-bottom:0
}
.blog-header {
  background-color:#f8fafc;
  text-align:center
}
.blog-header__inner {
  margin:0 auto;
  max-width:600px
}
.blog-header__title {
  font-size:2rem
}
.blog-header__subtitle {
  margin:1rem 0 2rem
}
.blog-header__author-avatar {
  border-radius:50%;
  box-shadow:0 0 12px 0 rgba(0,0,0,.15);
  display:block;
  height:auto;
  margin:0 auto 1.5rem;
  width:200px
}
.blog-header__author-social-links a {
  background-color:#000;
  border-radius:50%;
  display:inline-block;
  height:40px;
  margin:0 5px;
  position:relative;
  width:40px
}
.blog-header__author-social-links a:hover {
  background-color:#494a52
}
.blog-header__author-social-links svg {
  fill:#fff;
  height:15px;
  left:50%;
  position:absolute;
  top:50%;
  -webkit-transform:translate(-50%,-50%);
  -ms-transform:translate(-50%,-50%);
  transform:translate(-50%,-50%);
  width:auto
}
.blog-index {
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  -ms-flex-wrap:wrap;
  flex-wrap:wrap
}
.blog-index:after {
  -webkit-box-flex:1;
  content:"";
  -ms-flex:auto;
  flex:auto
}
.blog-index__tag-header {
  -webkit-box-flex:1;
  -ms-flex:1 0 100%;
  flex:1 0 100%;
  padding:1rem
}
.blog-index__tag-subtitle {
  font-size:1.16rem;
  line-height:1.1
}
.blog-index__tag-heading {
  border-bottom:3px solid #d1d6dc;
  padding-bottom:1rem
}
.blog-index__post {
  -webkit-box-flex:0;
  -ms-flex:0 0 100%;
  flex:0 0 100%;
  padding:2rem 1rem
}
@media screen and (min-width:768px) {
  .blog-index__post {
    -webkit-box-flex:0;
    -ms-flex:0 0 50%;
    flex:0 0 50%
  }
}
.blog-index__post--large {
  display:flex!important
}
@media screen and (min-width:768px) {
  .blog-index__post--large {
    -webkit-box-flex:1;
    display:-webkit-box;
    display:-ms-flexbox;
    display:flex;
    -ms-flex:1 0 100%;
    flex:1 0 100%;
    justify-items:space-between
  }
}
.blog-index__post-image {
  height:auto;
  max-width:100%
}
@media screen and (min-width:768px) {
  .blog-index__post-content--large,
  .blog-index__post-image-wrapper--large {
    -webkit-box-flex:1;
    -ms-flex:1 1 48%;
    flex:1 1 48%
  }
  .blog-index__post-content--large {
    padding-left:2rem
  }
  .blog-index__post-content--full-width {
    -webkit-box-flex:1;
    -ms-flex:1 1 100%;
    flex:1 1 100%;
    padding-left:0
  }
}
.blog-index__post-content h2 {
  margin:.5rem 0
}
.blog-index__post-content--small h2 {
  font-size:1.25rem
}
.blog-index__post-content p,
.blog-pagination {
  font-family:Lato,sans-serif
}
.blog-pagination {
  -webkit-box-align:center;
  -ms-flex-align:center;
  -webkit-box-pack:center;
  -ms-flex-pack:center;
  align-items:center;
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  justify-content:center;
  margin-bottom:3.3rem;
  text-align:center
}
.blog-pagination__link {
  border:1px solid transparent;
  border-radius:7px;
  color:#494a52;
  display:-webkit-inline-box;
  display:-ms-inline-flexbox;
  display:inline-flex;
  line-height:1;
  margin:0 .1rem;
  padding:.25rem .4rem;
  text-decoration:none
}
.blog-pagination__link--active {
  border:1px solid #b0c1d4
}
.blog-pagination__link:focus,
.blog-pagination__link:hover {
  text-decoration:none
}
.blog-pagination__next-link,
.blog-pagination__prev-link {
  -webkit-box-align:center;
  -ms-flex-align:center;
  align-items:center;
  display:-webkit-inline-box;
  display:-ms-inline-flexbox;
  display:inline-flex
}
.blog-pagination__prev-link {
  margin-right:.25rem;
  text-align:right
}
.blog-pagination__next-link {
  margin-left:.25rem;
  text-align:left
}
.blog-pagination__next-link--disabled,
.blog-pagination__prev-link--disabled {
  color:#b0c1d4;
  cursor:default;
  pointer-events:none
}
.blog-pagination__next-link svg,
.blog-pagination__prev-link svg {
  fill:#494a52;
  margin:0 5px
}
.blog-pagination__next-link--disabled svg,
.blog-pagination__prev-link--disabled svg {
  fill:#b0c1d4
}
.blog-pagination__number-link:focus,
.blog-pagination__number-link:hover {
  border:1px solid #b0c1d4
}
.body-container--blog-post {
  padding-top:133px
}
.blog-post-featured-image {
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  height:500px;
  width:100%
}
@media (max-width:768px) {
  .body-container--blog-post {
    padding-top:100px
  }
  .blog-post-featured-image {
    height:400px
  }
}
.blog-post {
  margin:0 auto;
  max-width:816px;
  padding:120px 0
}
.blog_post_author {
  padding-left:72px;
  padding-top:3px;
  position:relative
}
.blog_post_author_avatar {
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  border-radius:50%;
  content:"";
  height:56px;
  left:0;
  position:absolute;
  top:0;
  width:56px
}
.blog_post_author-name {
  color:#00213a!important;
  font-family:Lato;
  font-size:20px;
  font-style:normal;
  font-weight:600;
  line-height:160%
}
.blog_post_author-date {
  color:#6d6f78;
  font-family:Lato;
  font-size:16px;
  font-style:normal;
  font-weight:500;
  line-height:170%
}
.blog-post .blog-post__title {
  margin-bottom:40px;
  text-transform:uppercase
}
.blog-post__meta {
  align-items:center;
  display:flex;
  justify-content:space-between;
  margin-bottom:80px
}
.blog-post__tags {
  margin-bottom:16px
}
.blog-post__tag-link {
  color:#1fb3d9;
  font-family:Oswald;
  font-size:25px;
  font-style:normal;
  font-weight:400;
  line-height:37px;
  text-transform:uppercase
}
.blog-post__body a {
  color:#1fb3d9
}
.blog-post .social-links__icon {
  background:#0971bd!important
}
.blog-related-posts {
  background-color:#f8fafc;
  margin-top:3rem;
  padding:2rem 0
}
.blog-related-posts h2 {
  text-align:center
}
.blog-related-posts__list {
  display:-webkit-box;
  display:-ms-flexbox;
  display:flex;
  -ms-flex-wrap:wrap;
  flex-wrap:wrap
}
.blog-related-posts__post {
  -webkit-box-flex:0;
  color:#494a52;
  display:block;
  -ms-flex:0 0 100%;
  flex:0 0 100%;
  padding:1rem
}
@media screen and (min-width:768px) {
  .blog-related-posts__post {
    -webkit-box-flex:0;
    -ms-flex:0 0 50%;
    flex:0 0 50%
  }
}
@media screen and (min-width:1000px) {
  .blog-related-posts__post {
    -webkit-box-flex:0;
    -ms-flex:0 0 33.33333%;
    flex:0 0 33.33333%
  }
}
.blog-related-posts__image {
  height:auto;
  max-width:100%
}
.blog-related-posts__title {
  margin:1rem 0 .5rem
}
.blog-related-posts__title a {
  color:#494a52
}
.blog-comments {
  margin:0 auto;
  max-width:680px
}
.blog-comments form {
  max-width:100%
}
.blog-comments .hs-submit {
  text-align:center
}
.blog-comments .hs-button {
  background-color:transparent;
  border:1px solid #494a52;
  color:#494a52
}
.blog-comments .hs-button:hover {
  background-color:#494a52;
  color:#fff
}
.blog-comments .comment-reply-to {
  border:0
}
.blog-comments .comment-reply-to:hover {
  background-color:transparent;
  color:#494a52;
  text-decoration:underline
}
.blog-post__social_icons {
  align-items:center;
  color:#595959;
  display:flex;
  font-family:Lato;
  font-size:16px;
  font-style:normal;
  font-weight:600;
  line-height:170%
}
.blog-post__social_icons .social-links__icon {
  display:block
}
@media (max-width:768px) {
  .blog-post__meta {
    display:block
  }
  .blog-post__social_icons {
    padding:20px 0
  }
}
.blog-footer {
  background:#1fb3d9;
  padding:200px 20px
}
.blog-footer__inner {
  align-items:center;
  display:flex;
  justify-content:space-between
}
.blog-footer__inner>div {
  width:50%
}
.blog-footer__inner .hs_cos_wrapper_type_rich_text {
  display:block;
  margin:0 auto;
  max-width:412px;
  padding:0 20px
}
.blog-footer__cta {
  background:#fff;
  color:#1fb3d9;
  display:inline-block;
  font-family:Oswald;
  font-size:16px;
  font-style:normal;
  font-weight:500;
  line-height:100%;
  margin-top:32px;
  padding:28px 60px;
  text-align:center;
  text-transform:uppercase
}
@media (max-width:768px) {
  .blog-footer {
    padding:80px 20px
  }
  .blog-footer__inner {
    display:block
  }
  .blog-footer__inner>div {
    width:100%
  }
  .blog-footer__inner .hs_cos_wrapper_type_rich_text {
    margin-bottom:40px
  }
}
.blog-index__post-content--large {
  background:#f4f5f7;
  display:flex;
  flex-direction:column;
  justify-content:center;
  padding:32px
}
.blog-index__post-content h6 {
  margin:0
}
.blog-index__post {
  position:relative
}
.blog-index__post-content--small {
  background:#f4f5f7;
  margin-left:32px;
  margin-top:-50px;
  padding:20px;
  right:0
}
.blog-index__post-image-wrapper {
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  display:block;
  height:334px;
  width:100%
}
.blog-index__post-image-wrapper--large {
  min-height:374px
}
.blog-index__wrapper {
  padding:128px 0
}
.blog-index__inner {
  display:flex
}
.post_filter {
  margin:32px;
  width:286px
}
.post_filter h4 {
  margin-bottom:36px
}
.blog-index-topic-listing ul {
  list-style:none;
  margin:0;
  padding:0
}
.blog-index-topic-listing ul li {
  margin-bottom:24px
}
.blog-index-topic-listing ul li a {
  color:#00213a;
  display:block;
  font-family:Lato;
  font-size:20px;
  font-style:normal;
  font-weight:400;
  line-height:160%
}
.blog-index-topic-listing ul li a.active {
  font-weight:700
}
.blog-index {
  flex:1
}
.blog-index-load-more_wrapper {
  margin-top:32px;
  text-align:center;
  width:100%
}
.blog-index-load-more {
  background:#1fb3d9;
  border:1px solid #1fb3d9;
  color:#fff;
  display:inline-block;
  font-family:Oswald;
  font-size:16px;
  font-style:normal;
  font-weight:500;
  line-height:100%;
  padding:28px 50px;
  text-align:center;
  text-decoration:none;
  text-transform:uppercase
}
.blog-index-load-more:active,
.blog-index-load-more:focus,
.blog-index-load-more:hover {
  background:#fff!important;
  color:#1fb3d9!important;
  text-decoration:none
}
.blog-header__form__wrapper {
  align-items:center;
  background:#1fb3d9;
  display:flex;
  justify-content:space-between;
  margin:32px 0;
  padding:37px 0;
  width:100%
}
.blog-header__form {
  padding:19px 0 19px 56px;
  width:50%
}
.blog-header__form h4 {
  color:#fff;
  margin-bottom:32px;
  text-transform:uppercase
}
.blog-header__form form {
  align-items:center;
  display:flex
}
.blog-header__form form label {
  display:none
}
.blog-header__form form .hs_email {
  flex:1;
  margin-bottom:0
}
.blog-header__form form .hs-input {
  background:transparent;
  border:1px solid #fff;
  color:#fff;
  font-family:Lato;
  font-size:20px;
  font-style:normal;
  font-weight:500;
  line-height:160%;
  padding:20px 16px
}
.blog-header__form form .hs-input:active,
.blog-header__form form .hs-input:focus,
.blog-header__form form .hs-input:hover {
  border:1px solid #fff
}
.blog-header__form form .hs-input::placeholder {
  color:#fff;
  opacity:1
}
.blog-header__form form .hs-input:-ms-input-placeholder {
  color:#fff
}
.blog-header__form form .hs-input::-ms-input-placeholder {
  color:#fff
}
.blog-header__form form .hs-button {
  background:#fff;
  border:1px solid #fff;
  color:#1fb3d9;
  font-family:Oswald;
  font-size:16px;
  font-style:normal;
  font-weight:500;
  line-height:100%;
  padding:28px;
  text-align:center;
  text-transform:uppercase
}
.blog-header__form form .hs-error-msgs {
  display:none!important
}
.blog-header__form__image {
  width:30%
}
.blog-index__post {
  display:none
}
@media (max-width:768px) {
  .blog-index__inner {
    display:block
  }
  .blog-index-topic-listing ul {
    display:flex;
    overflow-x:scroll;
    white-space:nowrap
  }
  .blog-index-topic-listing ul li {
    padding-right:16px
  }
  .blog-index-topic-listing ul li a {
    text-decoration:none
  }
  .blog-index__wrapper {
    padding:64px 0
  }
  .blog-header__form__wrapper {
    display:block;
    padding:80px 20px
  }
  .blog-header__form {
    margin-bottom:24px;
    padding:0;
    width:100%
  }
  .blog-header__form__image {
    width:100%
  }
  .blog-index__post--large {
    display:block!important
  }
  .blog-index__post-content--large {
    background:#f4f5f7;
    margin-left:32px;
    margin-top:-50px;
    padding:20px;
    right:0
  }
}
.blog-banner {
  align-items:flex-end;
  background-image:url(https://8725880.fs1.hubspotusercontent-na1.net/hubfs/8725880/BRiX/client-results-banner.png);
  background-position:50%;
  background-repeat:no-repeat;
  background-size:cover;
  display:flex;
  height:760px;
  justify-content:space-between;
  width:100%
}
.blog-banner-image {
  margin-left:auto;
  margin-right:154px;
  max-width:611px;
  padding-left:20px;
  width:50%
}
.blog-banner-text {
  background:#1fb3d9;
  color:#fff;
  font-family:Lato;
  font-size:25px;
  font-style:normal;
  font-weight:400;
  line-height:150%;
  padding:80px 24px 24px 68px;
  width:50%
}
.blog-case-studies-heading {
  background:#1fb3d9;
  margin-bottom:-330px;
  padding:120px 20px 247px;
  text-align:center
}
.blog-case-studies-heading h2 {
  color:#fff
}
@media (max-width:768px) {
  .blog-banner {
    display:block;
    height:auto;
    margin-top:60px
  }
  .blog-banner-image {
    margin:0 auto;
    max-width:611px;
    padding:40px 20px;
    width:100%
  }
  .blog-banner-text {
    font-size:20px;
    padding:40px 20px;
    width:100%
  }
  .blog-case-studies-heading {
    padding:60px 20px 247px
  }
}
.error-page {
  padding:10rem 0;
  position:relative;
  text-align:center
}
.error-page:before {
  color:#f3f6f9;
  content:attr(data-error);
  font-family:Lato,sans-serif;
  font-size:40vw;
  font-weight:700;
  left:50%;
  position:absolute;
  top:50%;
  -webkit-transform:translate(-50%,-50%);
  -ms-transform:translate(-50%,-50%);
  transform:translate(-50%,-50%);
  width:100%;
  z-index:-1
}
@media screen and (min-width:1100px) {
  .error-page:before {
    font-size:20rem
  }
}
.error-page__heading {
  margin-bottom:1rem
}
.systems-page__wrapper {
  align-items:stretch;
  background-image:url(https://www.hubshop.co/hubfs/hero.jpg);
  box-sizing:border-box;
  display:flex;
  flex-direction:column;
  justify-content:center;
  margin:0;
  min-height:100vh;
  padding:90px 0
}
.systems-page__wrapper .content-wrapper {
  background:#fff
}
.systems-page__wrapper .form-container {
  margin-bottom:1em
}
.systems-page {
  margin:0 auto;
  max-width:700px;
  padding:8rem 1.4rem 3rem
}
.systems-page .success {
  background-color:#cde3cc;
  border:1.5px solid #4f7d24;
  border-radius:6px;
  color:#4f7d24;
  display:inline-block;
  margin:1.4rem 0;
  padding:.1rem .7rem;
  width:100%
}
.systems-page form input {
  max-width:100%
}
#email-prefs-form .header {
  background-color:transparent
}
.hs-search-field__bar {
  position:relative
}
.hs-search-field__suggestions {
  background-color:#fff;
  max-width:360px;
  position:absolute;
  right:0;
  top:100%;
  width:100%
}
.hs-search-results {
  margin-top:1.4rem
}
ul.hs-search-results__listing li {
  margin-bottom:1.4rem
}
.hs-search-results__title {
  color:#494a52;
  font-family:Merriweather,serif;
  font-size:1.25rem;
  margin-bottom:.35rem;
  text-decoration:underline
}
.hs-search-results__title:hover {
  text-decoration:none
}
.hs-search-results__description {
  padding-top:.7rem
}
.hs-search-highlight {
  font-weight:700
}
.hs-search-results__pagination a {
  color:#0270e0
}
.password-prompt input[type=password] {
  display:block;
  height:auto!important;
  margin-bottom:1.4rem
}
.backup-unsubscribe #email-prefs-form div {
  padding-bottom:0!important
}
.backup-unsubscribe input[type=email] {
  margin-bottom:1.4rem;
  padding:.7rem!important
}
.product-info {
  align-items:center;
  display:grid;
  grid-template-areas:"image details";
  grid-template-columns:1fr 1fr;
  overflow:hidden;
  width:100%
}
.product-info--swap {
  grid-template-areas:"details image"
}
.product-info h1 {
  margin-bottom:50px
}
.product-info__image {
  align-self:stretch;
  grid-area:image;
  height:100%
}
.product-info__image div {
  max-height:100vh;
  overflow:hidden;
  width:100%
}
.product-info__image div:before {
  box-sizing:border-box;
  content:"";
  display:block;
  padding-bottom:100%
}
.product-info__details {
  box-sizing:border-box;
  grid-area:details;
  max-width:620px;
  padding:50px 20px;
  width:100%
}
@media (min-width:1240px) {
  .product-info__details {
    padding:70px 20px 70px 65px
  }
  .product-info--swap .product-info__details {
    margin:0 0 0 auto;
    padding:70px 65px 70px 20px
  }
}
@media (max-width:767px) {
  .product-info {
    display:block
  }
  .product-info__image {
    height:33vh;
    max-height:300px
  }
  .product-info__details {
    max-width:100%;
    text-align:center
  }
}
.product-info__price-row {
  align-items:center;
  border-bottom:1px solid rgba(0,0,0,.2);
  display:flex;
  justify-content:space-between;
  margin-bottom:65px;
  padding-bottom:65px
}
.product-info__price span {
  font-size:30px
}
.product-info__price-row button {
  padding:14px 20px
}
.product-info__description-container {
  height:4.81em;
  line-height:160%;
  overflow:hidden
}
.product-info__description {
  white-space:pre-line
}
.product-info.clamped .product-info__description {
  -webkit-line-clamp:3;
  -webkit-box-orient:vertical;
  display:-webkit-box;
  max-height:4.8em;
  overflow:hidden;
  text-overflow:ellipsis
}
.product-info.expanded .product-info__description-container {
  height:auto
}
.product-info.expanded .product-info__description {
  display:block;
  max-height:none
}
.product-info.clamped .read-more-link {
  display:block
}
.product-info .read-more-link,
.product-info.expanded .read-more-link {
  display:none
}
.product-info__buy-now {
  border:1px solid;
  border-radius:4px;
  padding:10px 24px;
  text-decoration:none
}
.product-info__buy-now:focus,
.product-info__buy-now:hover {
  text-decoration:none
}
.page--checkout .header__shopping-cart-trigger {
  display:none
}
.page--checkout {
  --checkout__container-padding-top:0px
}
.checkout__wrapper {
  grid-gap:var(--container-h-padding);
  align-items:top;
  backdrop-filter:blur(25px);
  background:hsla(0,0%,100%,.8);
  display:grid;
  grid-template-columns:1fr auto
}
@media (max-width:1500px) {
  .checkout__container {
    --checkout__container-padding-top:80px;
    padding-top:var(--checkout__container-padding-top)
  }
}
.checkout__wrapper .shopping-cart--inline {
  align-self:stretch;
  display:grid;
  grid-template-rows:1fr auto;
  margin-right:calc(var(--container-h-padding)*-1);
  max-width:400px;
  width:45vw
}
.checkout__body {
  align-items:stretch;
  box-sizing:border-box;
  display:flex;
  flex-direction:column;
  justify-content:center;
  min-height:calc(100vh - var(--checkout__container-padding-top));
  padding:var(--container-h-padding)
}
@media (max-width:767px) {
  .checkout__wrapper {
    display:block
  }
  .checkout__wrapper .shopping-cart--inline {
    margin:0 calc(var(--container-h-padding)*-1);
    max-width:none;
    width:auto
  }
  .checkout__body {
    min-height:0;
    padding-left:0;
    padding-right:0
  }
  .checkout__body .hs-form fieldset.form-columns-1 .input,
  .checkout__body .hs-form fieldset.form-columns-2 .input {
    margin-right:0
  }
}
.checkout__body>*,
.checkout__signin__note,
.checkout__wrapper .shopping-cart__checkout {
  display:none
}
.checkout__total {
  color:#fff;
  font-size:150%;
  font-weight:700;
  margin:0;
  text-align:center
}
.StripeElement {
  background:#fff;
  border:2px solid #6d6f78;
  border-radius:3px;
  margin-bottom:1.4rem;
  padding:.7rem
}
.StripeElement--focus {
  border-color:#000
}
.checkout__error {
  color:red
}
.order-history {
  margin-top:120px
}
.order-history ul {
  padding:0
}
.order-history__order {
  display:grid;
  grid-template-areas:"id    date" "items items" "total total";
  margin-bottom:20px
}
.order-history__order__date,
.order-history__order__id {
  background:#ddd;
  margin-bottom:10px;
  padding:10px
}
.order-history__order__items {
  display:grid;
  grid-area:items;
  grid-template-columns:1fr auto auto
}
.order-history__order__item {
  display:contents;
  padding:0
}
.order-history__order__item>* {
  padding:10px
}
.order-history__order__date,
.order-history__order__item span,
.order-history__order__total {
  text-align:right
}
.order-history__order__total {
  font-weight:700;
  grid-area:total;
  padding:10px
}
.body-container {
  margin:0 auto;
  max-width:1920px
}

    .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#23428B 0%,#1D094C 100%);
}
.logo:hover img {
  -webkit-transition: all 200ms ease-in;
  -webkit-transform: scale(1.1);
  -ms-transition: all 200ms ease-in;
  -ms-transform: scale(1.1);
  -moz-transition: all 200ms ease-in;
  -moz-transform: scale(1.1);
  transition: all 200ms ease-in;
  transform: scale(1.1);
}
.logos img:hover, .logo img:hover {
  -webkit-filter: none;
  filter: none;
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0" /></filter></svg>#filter');
  -webkit-filter: grayscale(0);
  filter: grayscale(0);
  opacity: 1;
}
.logos img{
  -webkit-filter: gray;
  filter: gray;
  -webkit-filter: grayscale(50);
  filter: grayscale(50);
  -webkit-transition: .4s ease-in-out;
  transition: .4s ease-in-out;
  opacity: .7;}

  .logos {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(8em, 1fr));
  list-style: none;
  margin: 0;
  overflow: hidden;
  padding: 0;
  margin-bottom:40px;
}

.logos > li {
  aspect-ratio: 1;
  background-color: #fff;
  box-shadow: 0 0 0 1px #ededed;
}

.logo {
  display: grid;
  padding: 0.4em;
  place-items: center;
}

.logo:hover img{
-webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.1);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}

.logo figcaption {
  margin-top: 1em;
}

input[type="search"] {
  border: 0;
  font-size: 2rem;
  padding: 0.5em 2em;
  text-align: center;
  width: 100%;
}
h4.hpaline {
    display: none;
}
.hero-banner h1 {
    font-size: 80px;
}

.animated-text {
    animation-fill-mode: both;
    animation-duration: 1s;
}
.fadeInDown {
    animation-name: fadeInDown;
    visibility:visible;
}
		</style>
	</head>
<body>
<?php include("header.php"); ?>
    <!-- Hero Section Begin -->
    <div class="row-fluid-wrapper row-depth-1 row-number-3 dnd-row">
<div class="row-fluid ">
<div class="span12 widget-span widget-type-custom_widget dnd-module" style="" data-widget-type="custom_widget" data-x="0" data-w="12">
<div id="hs_cos_wrapper_widget_1702378937262" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_module" style="" data-hs-cos-general-type="widget" data-hs-cos-type="module">
<div id="digital-transformation" class="widget_1702378937262 header-content-animated-img" style="background: #FFFFFF;">
  
  <div class="content-wrapper">

    <div class="cm-hero-animated__animation">
      <div class="mosaic-container" data-inview="true">
        <div class="mosaic-element" data-index="1"></div>
        <div class="mosaic-element" data-index="2" style="background-image: url(https://www.aditiconsulting.com/hubfs/homepage-feat-image.jpg);">
          <div class="mosaic-element-child" data-index="1"></div>
          <div class="mosaic-element-child" data-index="2"></div>
        </div>
        <div class="mosaic-element" data-index="3" style="background-image: url(https://www.aditiconsulting.com/hubfs/group-meeting-feat-image.jpg);"></div>
        <div class="mosaic-element" data-index="4"></div>
        <div class="mosaic-element" data-index="5" style="background-image: url(https://www.aditiconsulting.com/hubfs/lady-office-feat-image.png);"></div>
        <div class="mosaic-element" data-index="6"></div>
        <div class="mosaic-element" data-index="7"></div>
      </div>
    </div>
    
    <div class="header-banner-content-wrap" style="max-width: 444px; margin: 0;">
      <h1>UNLEASH SUCCESS WITH FULL-CYCLE</h1>
<h1><span class="color-blue">TECHNOLOGY SOLUTIONS</span></h1>
<p>&nbsp;</p>
<div class="btn-group-wrap"><a href="/cs/c/?cta_guid=4374f515-7bdc-4329-a670-297252a9fb45&amp;signature=AAH58kHpexYTWIx_3Cxs4Z3QgwA_fNBftA&amp;pageId=43420778641&amp;placement_guid=2a2488c5-208b-4381-8f0a-2b84c231e327&amp;click=5be3faf3-a706-4219-91da-b7f4d455527f&amp;hsutk=8f5b90bd2cd2fcf5a3c380e08b4bb346&amp;canon=https%3A%2F%2Fwww.aditiconsulting.com%2F&amp;utm_referrer=https%3A%2F%2Fwww.aditiconsulting.com%2Fmanagedservices&amp;portal_id=8725880&amp;redirect_url=APefjpG_iPHZ4DioDbvPJs_e-svH47myPv9-LqHZ5oxem7Op16H5SGTdYnlDDE3VGKCqFOJfn-v37iWQNfnvc0llh9A49zXoA84NFaAmZeGMpFT3UbNeuxXK4Bc3d8yMeg4b_zjZZ9EOCpHfue9mi8ak-ip41ZSpUmmvAWrEHS6mc32YnAcONmzziZVD4zqnf6aTyRlFGCA26xBw9RMtApin7E4yeD6YUqFNm_lHph3tXGv8kwA-Lx1_ZR1IpcECV1ziwlcFGw66M0ILnJrAusCvenZ6Q5oSJA&amp;__hstc=234109249.8f5b90bd2cd2fcf5a3c380e08b4bb346.1699385369725.1702378591834.1702383124512.31&amp;__hssc=234109249.6.1702383124512&amp;__hsfp=2669229988&amp;contentType=standard-page&amp;hsLang=en" class="btn-primary" rel="noopener">START A PROJECT</a><a href="/talent-on-demand?hsLang=en" class="btn-secondary" rel="noopener">FIND TALENT NOW</a></div>
    </div>
    
  </div>
  
  
</div></div>

</div><!--end widget-span -->
</div><!--end row-->
</div>
	<div class="solutions">
        <div class="sub-solution">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-6">
                        <h2>OUR COMPANY</h2>
                        <h3>SNWN PVT. LTD is your trusted source in Managed IT services and support</h3>
                    </div>
                    <div class="col-md-6">
                        <p>SNWN PVT. LTD a company with Team, Agility & Skills is a privately owned 24×7 Technological IT Solutions and Services Company formed in 2007. Today we’re proud to boast a strong team of Cloud, DevOps engineers who thrive on rolling up their sleeves and solving your technological problems and meeting your business needs.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="img-solutions">
                        <img src="img/working-day-in-office-PQPMBS4-800x450-1.jpg" class="img-fluid">
                        <div class="how-we-can">
                            <h6>Our Services</h6>
                            <h3>How We Can Help</h3>
                            
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-solutions">
                        <img src="img/285-jir-60871-nam-eye-id-392451-jpeg1-800x450-1.jpg" class="img-fluid"> 
                        <div class="how-we-can">
                            <h6>Our Expertise</h6>
                            <h3>Why partner with us</h3>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-solutions">
                        <img src="img/portrait-of-a-cute-young-woman-smiling-P9G5TM81-800x450-1.jpg" class="img-fluid">
                        <div class="how-we-can">
                            <h6>Our Customers</h6>
                            <h3>Client Success Stories</h3>
                        </div>
                        </div> 
						
                    </div>
						
                </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="award">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-5">
                            <div class="img-award-1">
                            <img src="img/award/Experience.png"> 
                            </div>
                            </div>
                            <div class="col-md-7 reword-pad">
                            <div class="cnt">
                            <h5>10+ YEARS</h5>
                            <span>Experience</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-5">
                            <div class="img-award">
                            <img src="img/award/Assurance.png"> 
                            </div>
                            </div>
                            <div class="col-md-7 reword-pad">
                            <div class="cnt">
                            <h5>100% QUALITY</h5>
                            <span>Assurance</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-5">
                            <div class="img-award">
                            <img src="img/award/Satisfied.png"> 
                            </div>
                            </div>
                            <div class="col-md-7 reword-pad">
                                <div class="cnt">
                            <h5>100% CLIENTS</h5>
                            <span>Satisfaction</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="img-award">
                            <img src="img/award/Completed.png">
                                </div> 
                            </div>
                            <div class="col-md-7 reword-pad">
                            <div class="cnt">
                            <h5>600+ PROJECTS</h5>
                            <span>Completed</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	 <div class="support-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Stay Up, Stay Running, Stay Protected</h2>
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
								  <div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
  <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"/>
    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"/>
    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"/>
  </g>
</svg>
									  </div>

                                <div class="freatures-project">
                                <h5>Manage IT Solution</h5>
                                <p>24/7 maintenance and
monitoring that keeps
your computers, servers,
and systems up and running.</p>
                                <a href="#">Stay up and running</a>
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
								 <div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
  <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"/>
    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"/>
  </g>
</svg>
								 </div>
                                <div class="freatures-project">
                                <h5>Solution</h5>
                                <p>Realize the benefits of
building, migrating, and
running applications in Cloud.
We design your cloud
architecture</p>
                                <a href="#">Cloud First - Strategy</a>
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
								 <div class="img">
								 <svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
  <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"/>
    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"/>
  </g>
</svg>

  </g>
</svg>
								 </div>
                                <div class="freatures-project">
                                <h5>Cloud Services</h5>
                                <p>Protect your business
from malware, hackers,
viruses, ddos attacks and the most
commonly security threats</p>
                                <a href="#">Protect your business</a>
                                </div>
                             </div>      
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>



			<div class="technologies">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="technologies-1">
				<h2 style="">Technologies and Platforms We Use</h2>
						
						<div id="tab">
                <div class="j-tab-nav tab-nav-1 nav nav-pills justify-content-center p-4">
                    <a href="javascript:void(0);" class="os ml-1 p-3 current">Frontend</a>
                    <a href="javascript:void(0);" class="controlpanels ml-5 p-3">Backend</a>
                    <a href="javascript:void(0);" class="linkhelp ml-5 p-3">Mobile</a>
                    <a href="javascript:void(0);" class="cloud ml-5 p-3">Database</a>
                    <a href="javascript:void(0);" class="crm ml-5 p-3">Frameworks</a>
                    <a href="javascript:void(0);" class="other-tech ml-5 p-3">Cloud</a>
					<a href="javascript:void(0);" class="other-tech-1 ml-5 p-3">DevOps</a>
					
                </div>
                <div class="tab-con">
                    <div class="j-tab-con">
                        <div class="tab-con-item" style="display: block;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-Logo_windows d-block mb-md-2 mt-md-2 img-responsive" alt="Windows" title="Windows">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Window 2012,2008</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-Logo_centos d-block mb-md-2 mt-md-2 img-responsive" alt="Centos" title="Centos">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Centos</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-Logo_unix d-block mb-md-2 mt-md-2 img-responsive" alt="Unix" title="Unix">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Unix</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-Logo_ubuntu d-block mb-md-2 mt-md-2 img-responsive" alt="Ubuntu" title="Ubuntu">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Ubuntu</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-Logo_debin d-block mb-md-2 mt-md-2 img-responsive" alt="Debin" title="Debin">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Debin</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-Logo_freeBSD d-block mb-md-2 mt-md-2 img-responsive" alt="FreeBSD" title="FreeBSD">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">FreeBSD</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="tab-con-item" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_citrix d-block mb-md-2 mt-md-2 img-responsive" alt="Citrix" title="Citrix">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Citrix</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_vmware d-block mb-md-2 mt-md-2 img-responsive" alt="VmWare" title="VmWare">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">VmWare</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_onapp d-block mb-md-2 mt-md-2 img-responsive" alt="OnApp" title="OnApp">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">SolusVM</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_proxmox d-block mb-md-2 mt-md-2 img-responsive" alt="ProxMox" title="ProxMox">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">ProxMox</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_kvm d-block mb-md-2 mt-md-2 img-responsive" alt="KVM" title="KVM">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">KVM</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_xen d-block mb-md-2 mt-md-2 img-responsive" alt="XEN" title="XEN">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">XEN</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_hyper_v d-block mb-md-2 mt-md-2 img-responsive" alt="Hyper-V" title="Hyper-V">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Hyper-V</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_openvz d-block mb-md-2 mt-md-2 img-responsive" alt="OpenVZ" title="OpenVZ">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">OpenVZ</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_oracle_vm_server d-block mb-md-2 mt-md-2 img-responsive" alt="Oracle VM Server" title="Oracle VM Server">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Oracle VM Server</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class=" d-block mb-md-2 mt-md-2 img-responsive" alt="RedHat Virtulization" title="RedHat Virtulization">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">RedHat Virtulization</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_nutanix_acropolis d-block mb-md-2 mt-md-2 img-responsive" alt="Nutanix Acropolis" title="Nutanix Acropolis">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Nutanix Acropolis</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_virtuozzo d-block mb-md-2 mt-md-2 img-responsive" alt="Virtuozzo" title="Virtuozzo">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Virtuozzo</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-virtualization_openstack d-block mb-md-2 mt-md-2 img-responsive" alt="OpenStack" title="OpenStack">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">OpenStack</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-con-item" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-monitoring_system_zabbix d-block mb-md-2 mt-md-2 img-responsive" alt="Zabbix" title="Zabbix">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Zabbix</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-monitoring_system_nagios d-block mb-md-2 mt-md-2 img-responsive" alt="Nagios" title="Nagios">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Nagios</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-monitoring_system_solarwinds d-block mb-md-2 mt-md-2 img-responsive" alt="SolarWinds" title="SolarWinds">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">SolarWinds</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-monitoring_system_incinga d-block mb-md-2 mt-md-2 img-responsive" alt="Incinga" title="Incinga">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Incinga</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-monitoring_system_cacti d-block mb-md-2 mt-md-2 img-responsive" alt="Cacti" title="Cacti">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Cacti</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-monitoring_system_nixstats d-block mb-md-2 mt-md-2 img-responsive" alt="Nixstats" title="Nixstats">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Nixstats</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-con-item" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_saltstack d-block mb-md-2 mt-md-2 img-responsive" alt="Saltstack" title="Saltstack">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Saltstack</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_ansible d-block mb-md-2 mt-md-2 img-responsive" alt="Ansible" title="Ansible">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Ansible</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_puppet d-block mb-md-2 mt-md-2 img-responsive" alt="Puppet" title="Puppet">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Puppet</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_chef_devops d-block mb-md-2 mt-md-2 img-responsive" alt="Chef DevOps" title="Chef DevOps">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Chef DevOps</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-devops_tool_jenkins d-block mb-md-2 mt-md-2 img-responsive" alt="Jenkins" title="Jenkins">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Jenkins</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_git d-block mb-md-2 mt-md-2 img-responsive" alt="Git" title="Git">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Git</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_bamboo d-block mb-md-2 mt-md-2 img-responsive" alt="Bamboo" title="Bamboo">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Bamboo</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_docker d-block mb-md-2 mt-md-2 img-responsive" alt="Docker" title="Docker">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Docker</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-devops_tool_kubernetes d-block mb-md-2 mt-md-2 img-responsive" alt="Kubernetes" title="Kubernetes">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Kubernetes</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-con-item-6" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_pfsense d-block mb-md-2 mt-md-2 img-responsive" alt="pfSense" title="pfSense">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">pfSense</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_R1soft d-block mb-md-2 mt-md-2 img-responsive" alt="R1soft" title="R1soft">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">R1soft</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_WHMCS d-block mb-md-2 mt-md-2 img-responsive" alt="WHMCS" title="WHMCS">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">WHMCS</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_bitninja d-block mb-md-2 mt-md-2 img-responsive" alt="BitNinja" title="BitNinja">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">BitNinja</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_anycast d-block mb-md-2 mt-md-2 img-responsive" alt="AnyCast" title="AnyCast">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">AnyCast</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_halon d-block mb-md-2 mt-md-2 img-responsive" alt="Halon" title="Halon">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Halon</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_zimbra d-block mb-md-2 mt-md-2 img-responsive" alt="Zimbra" title="Zimbra">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Zimbra</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_baroes d-block mb-md-2 mt-md-2 img-responsive" alt="Baroes" title="Baroes">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Baroes</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						
						
						<div class="tab-con-item-2" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_pfsense d-block mb-md-2 mt-md-2 img-responsive" alt="pfSense222" title="pfSense">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">pfSense</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_R1soft d-block mb-md-2 mt-md-2 img-responsive" alt="R1soft222" title="R1soft">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">R1soft</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_WHMCS d-block mb-md-2 mt-md-2 img-responsive" alt="WHMCS222" title="WHMCS">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">WHMCS</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_bitninja d-block mb-md-2 mt-md-2 img-responsive" alt="BitNinja222" title="BitNinja">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">BitNinja</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_anycast d-block mb-md-2 mt-md-2 img-responsive" alt="AnyCast222" title="AnyCast">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">AnyCast</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_halon d-block mb-md-2 mt-md-2 img-responsive" alt="Halon222" title="Halon">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Halon</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_zimbra d-block mb-md-2 mt-md-2 img-responsive" alt="Zimbra222" title="Zimbra">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Zimbra</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_baroes d-block mb-md-2 mt-md-2 img-responsive" alt="Baroes222" title="Baroes">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Baroes</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						
						
						<div class="tab-con-item-3" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_pfsense d-block mb-md-2 mt-md-2 img-responsive" alt="pfSense333" title="pfSense">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">pfSense</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_R1soft d-block mb-md-2 mt-md-2 img-responsive" alt="R1soft" title="R1soft">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">R1soft</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_WHMCS d-block mb-md-2 mt-md-2 img-responsive" alt="WHMCS333" title="WHMCS">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">WHMCS</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_bitninja d-block mb-md-2 mt-md-2 img-responsive" alt="BitNinja" title="BitNinja">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">BitNinja</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_anycast d-block mb-md-2 mt-md-2 img-responsive" alt="AnyCast333" title="AnyCast">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">AnyCast</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_halon d-block mb-md-2 mt-md-2 img-responsive" alt="Halon333" title="Halon">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Halon</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_zimbra d-block mb-md-2 mt-md-2 img-responsive" alt="Zimbra333" title="Zimbra">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Zimbra</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png" class="bg-other_technology_baroes d-block mb-md-2 mt-md-2 img-responsive" alt="Baroes333" title="Baroes">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Baroes</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						
						
						<div class="tab-con-item-4" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_pfsense d-block mb-md-2 mt-md-2 img-responsive" alt="pfSense444" title="pfSense">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">pfSense</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_R1soft d-block mb-md-2 mt-md-2 img-responsive" alt="R1soft444" title="R1soft">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">R1soft</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="img/image-1/alamal-linux.png"  class="bg-other_technology_WHMCS d-block mb-md-2 mt-md-2 img-responsive" alt="WHMCS444" title="WHMCS">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">WHMCS</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp"  class="bg-other_technology_bitninja d-block mb-md-2 mt-md-2 img-responsive" alt="BitNinja444" title="BitNinja">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">BitNinja</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_anycast d-block mb-md-2 mt-md-2 img-responsive" alt="AnyCast444" title="AnyCast">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">AnyCast</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_halon d-block mb-md-2 mt-md-2 img-responsive" alt="Halon444" title="Halon">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Halon</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_zimbra d-block mb-md-2 mt-md-2 img-responsive" alt="Zimbra444" title="Zimbra">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Zimbra</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" class="bg-other_technology_baroes d-block mb-md-2 mt-md-2 img-responsive" alt="Baroes444" title="Baroes">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Baroes</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						
						
						<div class="tab-con-item" style="display: none;">
                            <div class="bglogoshome pt-5 pb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_pfsense d-block mb-md-2 mt-md-2 img-responsive" alt="pfSense555" title="pfSense">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">pfSense</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_R1soft d-block mb-md-2 mt-md-2 img-responsive" alt="R1soft555" title="R1soft">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">R1soft</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_WHMCS d-block mb-md-2 mt-md-2 img-responsive" alt="WHMCS555" title="WHMCS">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">WHMCS</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_bitninja d-block mb-md-2 mt-md-2 img-responsive" alt="BitNinja555" title="BitNinja">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">BitNinja</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images/bg-support-snwn.png" width="80" height="80" class="bg-other_technology_anycast d-block mb-md-2 mt-md-2 img-responsive" alt="AnyCast555" title="AnyCast">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">AnyCast</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_halon d-block mb-md-2 mt-md-2 img-responsive" alt="Halon555" title="Halon">
                                                </a>
                                            </div>
                                             <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Halon</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" width="80" height="80" class="bg-other_technology_zimbra d-block mb-md-2 mt-md-2 img-responsive" alt="Zimbra555" title="Zimbra">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Zimbra</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="box-logo">
                                                <a class="text-center text-dark">
                                                    <img src="images-1/logo-1933884_640.webp" class="bg-other_technology_baroes d-block mb-md-2 mt-md-2 img-responsive" alt="Baroes555" title="Baroes">
                                                </a>
                                            </div>
                                            <b class="text-uppercase d-block mb-md-2 p-2 text-white logotitle">Baroes</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
						</div>
				</div>
			</div>
			<p>Don’t see your platform here?<a href="#"> See our extended list of technologies</a>, or <a href="#">chat with us</a>.</p>
			</div>
            
		
		</div>



<div class="our-services">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    <div class="col-md-6">
                        <h2>OUR SERVICES</h2>
                        <h3>Industries We Serve</h3>
                    </div>
                    <div class="col-md-6">
                        <p>Leveraging next-gen technologies to develop
industry leading scalable digital solutions for
transformational customer experiences.</p>
                    </div>
				<div class="col-md-3">
					<div class="box-services">
						
						<img class="img-1" src="img/invest-logo.png">
						<img class="img-2" src="img/invest-logo-hover.png">
						<p>Banking & Finance</p>
					
					</div>
				</div>
				<div class="col-md-3">
					<div class="box-services">
						<img class="img-1" src="img/plug-logo.png">
						<img class="img-2" src="img/plus-logo-hover.png">
						<p>Healthcare</p>
						
					
					</div>
				</div>
				<div class="col-md-3">
					<div class="box-services">
						<img class="img-1" src="img/shopping-logo.png">
						<img class="img-2" src="img/shopping-logo-hover.png">
						<p>eCommerce</p>
						
					
					</div>
				</div>
				<div class="col-md-3">
					<div class="box-services">
						<img class="img-1" src="img/education-logo.png">
						<img class="img-2" src="img/education-logo-hover.png">
						<p>eCommerce</p>
						
					
					</div>
				</div>
		</div>
		<a href="#" class="clr-2">All Services <i class="fa fa-arrow-right mg-right"></i></a>
	</div>
</div>
</div>
</div>

<div class="our-clients">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Improve. Invent. Revamp.</h3>
				<h2>Our Clients</h2>
				<p>our expertise to deliver technology-led business breakthroughs across startups, global brands</p>
				
				<ol class="logos">
<li id="e8a71621-669d-4c43-b85b-539aad4b07ff">
<figure class="logo">
    <picture><img src="img\client\C.png"  style="aspect-ratio: 1 / 1;" alt="C | Cloud Host World" title="C | Cloud Host World">
</picture>
</figure>
</li>
<li id="e14ca454-9520-467c-9ed2-97ea7fb361de">
<figure class="logo">
    <picture><img src="img\client\Go (Golang).png"  style="aspect-ratio: 1 / 1;" alt="Golang | Cloud Host World" title="Golang | Cloud Host World"> 
</picture>
</figure></li><li id="b0abdd84-3294-4d60-95e4-23e8a542553d">
<figure class="logo">
<picture>
    <img src="img\client\HTML CSS.png"  style="aspect-ratio: 1 / 1;" alt="HTML CSS | Cloud Host World" title="HTML CSS | Cloud Host World"></picture></figure></li>
    <li id="9b55ce01-5d9e-4950-84b2-5b72a3094a02">
        <figure class="logo">
            <picture><img src="img\client\Java.png"  style="aspect-ratio: 1 / 1;" alt="Java | Cloud Host World" title="Java | Cloud Host World"></picture></figure>
        </li>
        <li id="ecf3c754-e6ef-4f8e-b6a4-3970b994b637">
            <figure class="logo"><picture><img src="img\client\JavaScript.png"  style="aspect-ratio: 1 / 1;" alt="JavaScript | Cloud Host World" title="JavaScript | Cloud Host World">
        </picture>
        </figure>
    </li>
    <li id="b6c5fbf7-6469-45ce-8f44-72ae16257477">
        <figure class="logo"><picture>
            <img src="img\client\Kotlin.png"  style="aspect-ratio: 1 / 1;" alt="Kotlin | Cloud Host World" title="Kotlin | Cloud Host World"></picture>
            </figure>
        </li>
        <li id="f389b11d-48b3-48f0-982d-6e08614492ee">
            <figure class="logo">
                <picture>
                    <img src="img\client\Lua.png"  style="aspect-ratio: 1 / 1;" alt="Lua | Cloud Host World" title="Lua | Cloud Host World"></picture></figure>
                </li>
                <!--<li id="98ad81d5-472a-48b5-8c2c-2ec0e69210b0">
                    <figure class="logo">
                        <picture>
                            <img src="img\chw-image\new-icon-social\Perl.png" alt="Adobe Illustrator" width="320" height="320" style="aspect-ratio: 1 / 1;"></picture></figure>
                        </li>-->
                        <li id="677fbbc2-9c9c-4ec4-9625-082d727bcf1c">
                            <figure class="logo">
                                <picture>
                                    <img src="img\client\SQL.png"  style="aspect-ratio: 1 / 1;" alt="SQL | Cloud Host World" title="SQL | Cloud Host World">
                                </picture>
                                </figure></li>
                                <li id="5877ec9e-f01d-40e4-b76c-9f95387e94e3">
                                    <figure class="logo">
                                        <picture>
                                            <img src="img\client\Swift.png" alt="Adobe InDesign"  style="aspect-ratio: 1 / 1;" title="Swift | Cloud Host World">
                                        </picture>
                                        
                                    </figure>
                                </li>
                                <li id="060425a8-8847-42d9-9cd4-751dcd875f2a">
                                    <figure class="logo">
                                        <picture>
                                        <img src="img\client\TypeScript.png"  style="aspect-ratio: 1 / 1;" alt="TypeScript | Cloud Host World" title="TypeScript | Cloud Host World">
                                        </picture>
                                        
                                    </figure>
                                </li>
                               <!-- <li id="4ad79a0d-4fa1-4989-b8cb-07c101efc9dc">
                                    <figure class="logo">
                                        <picture
                                        >
                                        <img src="img\chw-image\new-icon-social\Objective-C.png" alt="Adobe InDesign" width="320" height="320" style="aspect-ratio: 1 / 1;"></picture>
                                        </figure>
                                    </li>-->
                                    <li id="89e13613-4bf2-4d25-a82e-ed374bac1477">
                                        <figure class="logo">
                                            <picture>
                                            <img src="img\client\PHP.png"  style="aspect-ratio: 1 / 1;" alt="PHP | Cloud Host World" title="PHP | Cloud Host World">
                                            </picture>
                                           
                                        </figure>
                                    </li>
                                    <li id="162b05c1-9195-4ae1-bb0a-1c4c26f608c0">
                                        <figure class="logo"><picture>
                                        <img src="img\client\Python.png"  style="aspect-ratio: 1 / 1;" alt="Python | Cloud Host World" title="Python | Cloud Host World">
                                    </picture>
                                    
                                </figure>
                            </li>
                            <li id="192e240f-36c2-49bd-aa30-918a128dae4d">
                                <figure class="logo">
                                    <picture>
                                    <img src="img\client\R.png"  style="aspect-ratio: 1 / 1;" alt="R | Cloud Host World" title="R | Cloud Host World">
                                    </picture>
                                   
                                </figure>
                            </li>
                            <li id="5148e4e2-cdbb-4edc-92b0-a6569e9f9fac">
                                <figure class="logo"><picture>
                                <img src="img\client\ruby.png"  style="aspect-ratio: 1 / 1;" alt="Ruby | Cloud Host World" title="Ruby | Cloud Host World">
                                </picture>
                               
                            </figure>
                        </li>
                        <li id="c0023c7e-272c-4547-a8f7-8aff93f79333">
                            <figure class="logo">
                                <picture>
                                <img src="img\client\Rust.png"  style="aspect-ratio: 1 / 1;" alt="Rust | Cloud Host World" title="Rust | Cloud Host World"></picture>
                                </figure>
                            </li>
                            <li id="ea2733c4-24ed-4ace-84a7-83875410a71e">
                                <figure class="logo">
                                    <picture>
                                    <img src="img\client\Scala.png"  style="aspect-ratio: 1 / 1;" alt="Scala | Cloud Host World" title="Scala | Cloud Host World">
                                    </picture>
                                     
                                    </figure>
                                </li> 
                            </ol>
				</div>
		</div>
	</div>
</div>
<div class="why-choose-us">
<div class="container">
<div class="row">
	<div class="col-md-12">
			<div class="row">
                    <div class="col-md-6">
                        <h2>WHY CHOOSE US.</h2>
                        <h3>Our Core Values</h3>
						<p>Our teams of experts are the driving force behind all of our
successes. Together, we believe that anything is possible and
work relentlessly to meet or exceed our client’s expectations
while still staying true to our core values.
</p>
						<p>SNWN is committed to hiring the most talented individuals
in Information Technology and providing them with ongoing
professional growth and career development opportunities.
<br>Our staff members are not only passionate about technology
but also equally dedicated to their work.</p>
                    </div>
				<div class="col-md-3">
					<div class="box-services">
						<H2>1.</H2>
                        <div class="row">
                        <div class="col-md-8">
						<H6>Ensure impactful innovations</H6>
                        </div>
                        <div class="col-md-4">
                        <img src="img/client/clock.svg" class="im-why-choose" width="60px">
                        </div>
                        </div>
						<p>An unwavering commitment to drive
maximum customer satisfaction,
infused with integrity and passion.</p>
					
					</div>
					<div class="box-services">
						<H2>2.</H2>
                        <div class="row">
                        <div class="col-md-8">
						<H6>Ensure impactful innovations</H6>
                        </div>
                        <div class="col-md-4">
                        <img src="img/client/clock.svg" class="im-why-choose" width="60px">
                        </div>
                        </div>
						<p>Training and mentorship programs,
interactive sessions and community
meetups to drive personal growth.</p>
					
					</div>
				</div>
				<div class="col-md-3">
					<div class="box-services">
						
						<h2>3.</h2>
                        <div class="row">
                        <div class="col-md-8">
						<h6>Focus on self-development</h6>
                        </div>
                        <div class="col-md-4">
                        <img src="img/client/clock.svg" class="im-why-choose" width="60px">
                        </div>
                        </div>
						<p>Latest infrastructure and flexible
working hours along with many
other privileges that inculcate
healthy lifestyle choices. </p>
						
					
					</div>
					
					<div class="box-services">
                    
						<h2>4.</h2>
                        <div class="row">
                        <div class="col-md-8">
						<h6>Focus on self-development</h6>
                        </div>
                        <div class="col-md-4">
                        <img src="img/client/clock.svg" class="im-why-choose" width="60px">
                        </div>
                        </div>
						<p>Build, deploy, and release
code to cloud runtimes.
Connect and run services
at scale. inculcate
healthy lifestyle choices.</p>
					</div>
				</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="case-studies">
<div class="container">
<div class="row">
	<div class="col-md-12">
		<h3>Case studies</h3>
		<p>Enterprises from around the globe trust SNWN. These are some of the most innovative stories from
our customers about how they are using SNWN solutions to make the transition to multi-cloud infrastructure.</p>
		<div class="row" style="border:1px solid #eeebeb; border-radius:8px;">
			<div class="col-md-4" style="padding: 0;">
				<div class="case-st-img" >
			<img src="img/giant-building-with-sun.png">
					</div>
			</div>
			<div class="col-md-8" style="padding: 0;">
				<div class="global-leading">
			<h3>A bank, the cloud, and a paradigm shift</h3>
				<p>Leading global bank uses SNWN Terraform to
reduce risk and quickly deliver new services during
their shift to the cloud.</p>
				<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
					</div>
			</div>
		</div>
	
	</div>
	</div>
	</div>
</div>



<div class="bank">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/smart-discussing-meeting-report-phone.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/engineers-looking-bridge-construction.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
			
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<a href="#" class="clr-1">Read our case studies <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>





<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
		  <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
		  <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>

      <div class="col-md-7">
		  <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
			  </div>
      </div>
    </div>

    

  </div>
</section>



<?php include("footer.php"); ?>









    

			
	
	
		
	
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
		
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
        
 <script src="/hs/hsstatic/jquery-libs/static-1.1/jquery/jquery-1.7.1.js"></script>
<script>hsjQuery = window['jQuery'];</script>
<!-- HubSpot performance collection script -->
<script defer src="https://static.hsappstatic.net/content-cwv-embed/static-1.971/embed.js"></script>
<script src="https://www.aditiconsulting.com/hs-fs/hub/8725880/hub_generated/template_assets/42720016693/1617808259022/BRiX/js/jQuery.min.min.js"></script>
<script src="https://www.aditiconsulting.com/hs-fs/hub/8725880/hub_generated/template_assets/42501103490/1635544882827/BRiX/js/main.min.js"></script>
<script>
var hsVars = hsVars || {}; hsVars['language'] = 'en';
</script>

<script src="/hs/hsstatic/cos-i18n/static-1.53/bundles/project.js"></script>
<script src="https://www.aditiconsulting.com/hs-fs/hub/8725880/hub_generated/module_assets/42501013591/1691618596114/module_42501013591_menu-section.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script src="https://www.aditiconsulting.com/hs-fs/hub/8725880/hub_generated/module_assets/146336612457/1711575268899/module_146336612457_2_Column_-_Animated_Image_and_Content.min.js"></script>
<script src="https://www.aditiconsulting.com/hs-fs/hub/8725880/hub_generated/module_assets/146340810836/1713475837308/module_146340810836_2_Column_-_Full_Width_Image_and_Content.min.js"></script>


<!-- Start of HubSpot Analytics Code -->
<script type="text/javascript">
var _hsq = _hsq || [];
_hsq.push(["setContentType", "standard-page"]);
_hsq.push(["setCanonicalUrl", "https:\/\/www.aditiconsulting.com"]);
_hsq.push(["setPageId", "43420778641"]);
_hsq.push(["setContentMetadata", {
    "contentPageId": 43420778641,
    "legacyPageId": "43420778641",
    "contentFolderId": null,
    "contentGroupId": null,
    "abTestId": null,
    "languageVariantId": 43420778641,
    "languageCode": "en",
    
    
}]);
</script>

<script type="text/javascript" id="hs-script-loader" async defer src="/hs/scriptloader/8725880.js"></script>
<!-- End of HubSpot Analytics Code -->


<script type="text/javascript">
var hsVars = {
    render_id: "1ba5a3b9-041d-4a5b-b7c8-140c7d11ab8a",
    ticks: 1718648120465,
    page_id: 43420778641,
    
    content_group_id: 0,
    portal_id: 8725880,
    app_hs_base_url: "https://app.hubspot.com",
    cp_hs_base_url: "https://cp.hubspot.com",
    language: "en",
    analytics_page_type: "standard-page",
    scp_content_type: "",
    analytics_page_id: "43420778641",
    category_id: 1,
    folder_id: 0,
    is_hubspot_user: false
}
</script>


<script defer src="/hs/hsstatic/HubspotToolsMenu/static-1.321/js/index.js"></script>




    <script src="https://www.aditiconsulting.com/hs-fs/hub/8725880/hub_generated/template_assets/43615149392/1635544883687/BRiX/js/inview.min.js"></script>
    <script>
      $(document).ready(function(){
          if( window.hsInEditor ) {
              $('[data-inview="false"]').attr('data-inview', 'true');
              $('[data-inview-out="false"]').attr('data-inview-out', 'true');
              $('div.dnd-section[class*="-background-image"]').attr('data-inview', 'true');
              return;
          } else {
              if(typeof inView !== 'undefined' && !!inView){        
                inView('[data-inview="false"]')
                  .on('enter', el => {
                    el.setAttribute('data-inview', 'true');
                  })
                inView('div.dnd-section[class*="-background-image"]')
                  .on('enter', el => {
                    el.setAttribute('data-inview', 'true');
                  })
                inView('[data-inview-out="false"]')
                  .on('enter', el => {
                    el.setAttribute('data-inview-out', 'true');
                  })
                  .on('exit', el => {
                    el.setAttribute('data-inview-out', 'false');
                  })
                //inView.offset(100);
              }
          }
      });
      
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var text = ['DevOps', 'Cloud Services', 'Site Services', 'Support']; //Text to be shown
            var index = 0; //Starting index for rotating text
            var changeText = setInterval(function() {
                if (index == text.length - 1)
                    index = 0;
                else
                    index++;
                $('.rotate-text').animate({
                    "opacity": "0",
                    "margin-left": "10px"
                }, 500, function() {
                    $('.rotate-text').text(text[index]);
                    $('.rotate-text').animate({
                        "opacity": "1",
                        "margin-left": "0"
                    }, 500);
                });

            }, 2000);
        });
    </script>
<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>