<!DOCTYPE html>
<html lang="zxx">
	<head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="icon" href="img/fevicon.png" sizes="192x192">
		<style>
      .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);
}
			.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
  color: #fff;
  background: linear-gradient(to left,#23428b 0,#1d094c 100%);
  border-color: #dee2e6 #dee2e6 #fff;
}
.bdr-ld{
border: 1px solid #dee2e6; border-radius: 0 0px 15px 15px ;
}
.nav-item a{ color:#000; background-color:rgb(216,206,255,0.6); }
.nav-tabs .nav-item {
  margin-bottom: -1px;
  margin: 0 1px;
}
.appllication-service ul li a:hover{ color:#000; background: tranparant; }
.appllication-service ul li{ display:inline-block; width: 24.8%; text-align:center; margin-left:20px; }
.appllication-service ul li {
  display: inline-block;
  width: 24.8%;
  text-align: center;
  margin-left: 0px;
}
.bdr-ld {
  border: 1px solid #dee2e6;
  border-radius: 0 0px 15px 15px;
}
		</style>
	</head>
<body>
  <?php include("header.php"); ?>
	
    <!-- Header End -->
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="" style=" background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);">
            <div class="hero__item set-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1 class="ttl-heading" style="color:#fff; font-weight: 900; font-size: 56px;">Auto Scaling for  <span class="rotate-text">Speak Performance</span></h1>
                                    <!--<h4 class="service-cont">Service And Solutions</h4>-->
								<p class="automate">Keep costs low and resouces lean with complimentary cloud scaling technology</p>
								
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
							<div class="row">
					<div class="col-md-4">
					<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Get Started</a>			
					</div>
					<div class="col-md-4">
								<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Contact Us</a>
				</div>
        <div class="col-md-4">
								&nbsp;
				</div>
				</div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
								<img src="img/autoscaling-hero-logo.png" >
                                
                            </div>
                        </div>
                    </div>
                </div>
				
					<a href="#" class="Btn"><img class="mus" src="img/mouse-accest-1.png"><i class="fa fa-arrow-down chev-dn"></i>
				
				</a>
								
            </div>
        </div>
    </section>
		
		<div class="overview" style="">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    
				<div class="col-md-6">
					<div class="">
						
						<h3>Overview</h3>
						<p>Auto Scaling (AS) provides you with a highly
efficient management policy for computing
resources. You can set the time to execute the
management policies regularly or create a real
time monitoring policy to manage the number
of CVM instances and deploy the environment
for the instances, ensuring that your business
runs smoothly.</p>
<!--<a href="#" class="clr-3">Get a free qoutes <i class="fa fa-arrow-right mg-right"></i></a>-->
					
					</div>
				</div>
				<div class="col-md-6">
                    <div class="img-overview">
					<img src="img/online-business-illustration.png">
                </div>

					
</div>
						
					
					</div>
				</div>
				
		</div>
		
	</div>
</div>


<div class="container">
    <div class="row">

<div class="all-features" style="">
                                <div class="row">
                                 <div class="col-md-4">
									 <div class="autoscaling">
                                     <h3 class="heading-feature">What is autoscaling?</h3>
                                    <p>Autoscaling is a feature of cloud
computing that automatically
allocates resources based on
real-time demands and workloads.
By automatically scaling resources
up or down, performance is
consistently responsive, resilient,
and budget-friendly.</p>
                                 </div>
									</div>
                                 <div class="col-md-4">
									 <div class="autoscaling">
                                     <h3>How does it work?</h3>
                                    <p>Autoscaling monitors your website
and triggers a scaling event whenever
traffic begins to exceed concurrent
user capacity. The scaling event
dynamically adjusts cloud resources
to meet the increased demand.</p>
                                 </div>
									</div>
                                 <div class="col-md-4">
									 <div class="autoscaling-1">
                                     <h3>What are the benefits?</h3>
                                     <p>Autoscaling helps you save money on 
resource costs and maintain top
performance hassle-free. It works when
you need it to work, allowing you to drive
more traffic, gather more data, and focus
on creating a successful website. Know more.</p>
                                 </div>
									</div>
                                
                                    </div>
                                </div>
                            </div>
                        </div>




                         <div class="snwn-benefits">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Benefits</h2>
                            <p>Automatically ensure availability, performance & efficiency</p>
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
                                  <div class="img">
                                <img src="img/automatic.png">
                                      </div>

                                <div class="freatures-project">
                                <h5>Automatic</h5>
                                <p>AS automatically creates and removes
CVM instances in real time based on
the business load, ensuring that you
are running the optimal number of
instances and eliminating the need
for manual deployment.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/cost-saving.png">
 
                                 </div>
                                <div class="freatures-project">
                                <h5>Cost-saving</h5>
                                <p>AS helps you maintain an optimal
number of instances for variable
business demand. When the demand
increases, AS will automatically and
quickly add new CVM instances.
Conversely, when the demand decreases,
AS will automatically remove</p>
                                
                                </div>
                             </div>  
                            </div>
                                
                                <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Schedulable</h5>
                                <p>AS allows you to schedule scaling to
respond to regular changes in business
load (e.g. scaling up at 9 p.m. every day).</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                 <img src="img/fault-tolerant.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Fault-tolerant</h5>
                                <p>AS automatically creates and removes
CVM instances in real time based on
the business load, ensuring that you
are running the optimal number of
instances and eliminating the need
for manual deployment.</p>
                                </div>
                             </div> 
                        </div>


                        <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/easy-to-audit.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Easy to Audit</h5>
                                <p>AS helps you maintain an optimal
number of instances for variable
business demand. When the demand
increases, AS will automatically and
quickly add new CVM instances.
Conversely, when the demand decreases,
AS will automatically remove</p>
                                
                                </div>
                             </div>  
                            </div>


                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Schedulable</h5>
                                <p>AS allows you to schedule scaling to
respond to regular changes in business
load (e.g. scaling up at 9 p.m. every day).</p>
                                
                                </div>
                             </div>  
                            </div>
                                </div>
                            
                                </div>
                        </div>
                    </div>
                </div>
            </div>


	
	 


    



			










				<div class="scale-seamlessly">
<div class="container">
<div class="row">
	<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
                    <h3>FEATURES  .</h3>
                    <h2>Scale Seamlessly, Save Smart.</h2>


                </div>
				<section id="it-help-desk mb-3" class="mobile-responsive-scale">
        <div class="container">
             
            <div class="row pt-5 bg-clr-section">
                <div class="col-md-4 mb-3">
                    <div class="nav flex-column nav-pills text-left pl-4 border-right" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link  mb-2 p-1 show text-uppercase color-tab  active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="false" onmouseover="openCity(event, 'IT')">Alarm-triggered Scaling</a>
                        <a class="nav-link mb-2 p-1 text-uppercase color-tab" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false" onmouseover="openCity(event, 'Focus')">Scheduled Scaling</a>
                        <a class="nav-link mb-2 p-1 text-uppercase color-tab" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false" onmouseover="openCity(event, 'Expert')">Replacement of Unhealthy CVMs</a>
                        <a class="nav-link mb-2  p-1 text-uppercase color-tab" id="v-pills-settings-tabs" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="true" onmouseover="openCity(event, 'Safe')">Auto Load Balancing</a>
                        <a class="nav-link p-1 text-uppercase color-tab" id="v-pills-setting-tab" data-toggle="pill" href="#v-pills-setting" role="tab" aria-controls="v-pills-setting" aria-selected="true" onmouseover="openCity(event, 'Saf')">AS Activity Record</a>
                    </div>
                </div>
                <div class="col-md-8 mb-3">
                    <div class="tab-content text-center" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="IT" role="tabpanel" aria-labelledby="v-pills-home-tab" style="display: block;">
							<div class="container">
							<div class="row">
							
							<div class="col-md-12">
                                
								<div class="inner-padding">
                                    <div class="row" >
                                    <div class="col-md-9">
                                    <h5>Alarm-triggered Scaling</h5>
                                </div>
                                <div class="col-md-3"> <img src="img/client/clock.svg" width="60px"> </div>
                            </div>
                            <p class="text-justify cnt-scale">You can customize an alarm policy to adjust the business
deployment based on the CVM metrics. When a metric
(e.g. CPU utilization, memory utilization, outbound and
inbound bandwidth on the private network, outbound
and inbound bandwidth on the public network) reaches
the threshold due to the business load, the policy can
automatically increase</p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane fade show" id="Focus" role="tabpanel" aria-labelledby="v-pills-profile-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							
							<div class="col-md-12 padding-0">
								<div class="inner-padding">
                                    <div class="row" >
                                    <div class="col-md-9">
                                    <h5>Scheduled Scaling</h5>
                                    </div>
                                <div class="col-md-3"> <img src="img/client/clock.svg" width="60px"> </div>
                            </div>
                            <p class="text-justify cnt-scale">Installing IT infrastructure with networks, servers, security, storage, and a whole host of other components is extremely pricey. Outsourcing IT system support transforms fixed IT costs into variable costs and gives you room to budget accordingly. In short, you only pay for what you use, which can be a huge cost reduction means you can easily manage your annual operating costs.</p>
							</div>
								</div>
								</div>
								</div>
                        </div>
                        <div class="tab-pane fade show" id="Expert" role="tabpanel" aria-labelledby="v-pills-messages" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							
							<div class="col-md-12">
								<div class="inner-padding">
                                    <div class="row" >
                                    <div class="col-md-9">
                                    <h5>Replacement of Unhealthy CVMs</h5>
                                    </div>
                                <div class="col-md-3"> <img src="img/client/clock.svg" width="60px"> </div>
                            </div>
                            <p class="text-justify cnt-scale">By outsourcing IT support, you can free up its valuable resources and focus on what matters: making the wheels of the business turn. Our support experts will answer in least working time. We can also work with LIVE CHAT from your website. It is important for your technology to run 24/7 because it is the core of your business and we are focused on quality and fastness. </p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane fade show" id="Safe" role="tabpanel" aria-labelledby="v-pills-settings-tabs" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							
							<div class="col-md-12">
								<div class="inner-padding">
                                    <div class="row" >
                                    <div class="col-md-9">
                                    
                                    <h5>Auto Load Balancing</h5>
                                    </div>
                                <div class="col-md-3"> <img src="img/client/clock.svg" width="60px"> </div>
                            </div>
                            <p class="text-justify cnt-scale">Your company’s doesn’t likely have the budget to hire a panel of IT experts to guide you through the changing world of big data. When you outsource your IT support, you will gain access to top levels of technology and resources. This access will help you stay competitive in your industry. Certified team of experts can build that difference for you!</p>
							</div>
								</div>
								</div>
							</div>
                            </div>


                            <div class="tab-pane fade show" id="Saf" role="tabpanel" aria-labelledby="v-pills-setting-tab" style="display: none;">
                            <div class="container">
                            <div class="row bg-clr-section">
                            
                            <div class="col-md-12">
                                <div class="inner-padding">
                                    <div class="row" >
                                    <div class="col-md-9">
                                    <h5>AS Activity Record</h5>
                                    </div>
                                <div class="col-md-3"> <img src="img/client/clock.svg" width="60px"> </div>
                            </div>
                            <p class="text-justify cnt-scale">Your company’s doesn’t likely have the budget to hire a panel of IT experts to guide you through the changing world of big data. When you outsource your IT support, you will gain access to top levels of technology and resources. This access will help you stay competitive in your industry. Certified team of experts can build that difference for you!</p>
                            </div>
                                </div>
                                </div>
                            </div>
							
                    </div>
                </div>
            </div>
        </div>
    </section>
				
				
				
		</div>
		
	</div>
</div>
</div>
</div>	
					
					
					
					<div class="appllication-service">
					<div class="container">
					<div class="row">
						
                        <div class="col-md-12">
                            <h3>An Introduction to Modern Application Services</h3>
                            

<div id="tabs-group-0" class="tab-group tab-group--0">

  

  <div class="js-tabs" data-existing-hx="h3" data-tabs-prefix-class="hb">
    <ul class="js-tablist">
      <li class="js-tablist__item"><a class="js-tablist__link" href="#aliquam-0">Load Balancing</a></li>
      <li class="js-tablist__item"><a class="js-tablist__link" href="#debitis-0">Web Application Firewall</a></li>
      <li class="js-tablist__item"><a class="js-tablist__link" href="#soluta-0">Ingress Controller</a></li>
      <li class="js-tablist__item"><a class="js-tablist__link" href="#voluptates-0">Application Delivery Controller</a></li>
    </ul>
    <div class="js-tabcontent" id="aliquam-0">
    <img src="img/imag-1.png">
    <p>Load balancing has evolved from the hardware ADC network solutions popularized in the late 1990s to the software-defined application services platforms being adopted today.</p>
    </div>
    <div class="js-tabcontent" id="debitis-0">
    <img src="img/imag-1.png">
    <p>Load balancing has evolved from the hardware ADC network solutions popularized in the late 1990s to the software-defined application services platforms being adopted today1.</p>
    </div>
    <div class="js-tabcontent" id="soluta-0">
    <img src="img/imag-1.png">
    <p>Load balancing has evolved from the hardware ADC network solutions popularized in the late 1990s to the software-defined application services platforms being adopted today2.</p>
    </div>
    <div class="js-tabcontent" id="voluptates-0">
    <img src="img/imag-1.png">
    <p>Load balancing has evolved from the hardware ADC network solutions popularized in the late 1990s to the software-defined application services platforms being adopted today3.</p>
    </div>
    

  </div>

</div>       				
						</div>
						</div>
					</div>
					</div>
<div class="faq-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Want to know more?</h3>
				<h2>Frequently Asked Questions</h2>
				<div class="row">
				<div class="col-md-6">
				<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd3">
    <label for="rd3" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
	<div class="tab">
    <input type="radio" name="accordion-2" id="rd4">
    <label for="rd4" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
  
</section>
					</div>
<div class="col-md-6">
<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd1">
    <label for="rd1" class="tab__label">What are the benefits of DevOps?</label>
    <div class="tab__content">
      <p>If you want to have only one tab open, you can use .</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd2">
    <label for="rd2" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
	
  
</section>
	</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>

	
	<div class="know-more">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/smart-discussing-meeting-report-phone.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/engineers-looking-bridge-construction.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
			
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<a href="#" class="clr-1">Read our Blog <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>











<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
		  <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
		  <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>

      <div class="col-md-7">
		  <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
			  </div>
      </div>
    </div>
  </div>
</section>
<?php include("footer.php"); ?>

			
	
	
		
	
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script>
     /*
 * ES2015 accessible tabs panel system, using ARIA
 * Website: https://van11y.net/accessible-tab-panel/
 * License MIT: https://github.com/nico3333fr/van11y-accessible-tab-panel-aria/blob/master/LICENSE
 */
"use strict";

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

(function (doc) {
  "use strict";

  var TABS_JS = "js-tabs";
  var TABS_JS_LIST = "js-tablist";
  var TABS_JS_LISTITEM = "js-tablist__item";
  var TABS_JS_LISTLINK = "js-tablist__link";
  var TABS_JS_CONTENT = "js-tabcontent";
  var TABS_JS_LINK_TO_TAB = "js-link-to-tab";

  var TABS_DATA_PREFIX_CLASS = "data-tabs-prefix-class";
  var TABS_DATA_HX = "data-hx";
  var TABS_DATA_GENERATED_HX_CLASS = "data-tabs-generated-hx-class";
  var TABS_DATA_EXISTING_HX = "data-existing-hx";

  var TABS_DATA_SELECTED_TAB = "data-selected";

  var TABS_PREFIX_IDS = "label_";

  var TABS_STYLE = "tabs";
  var TABS_LIST_STYLE = "tabs__nav";
  var TABS_LISTITEM_STYLE = "tabs__nav-list-item";
  var TABS_LINK_STYLE = "tabs__nav-list-link";
  var TABS_CONTENT_STYLE = "tabs__panel";

  var TABS_HX_DEFAULT_CLASS = "hide";

  var TABS_ROLE_TABLIST = "tablist";
  var TABS_ROLE_TAB = "tab";
  var TABS_ROLE_TABPANEL = "tabpanel";
  var TABS_ROLE_PRESENTATION = "presentation";

  var ATTR_ROLE = "role";
  var ATTR_LABELLEDBY = "aria-labelledby";
  var ATTR_HIDDEN = "aria-hidden";
  var ATTR_CONTROLS = "aria-controls";
  var ATTR_SELECTED = "aria-selected";

  var DELAY_HASH_UPDATE = 1000;

  var hash = window.location.hash.replace("#", "");

  const IS_OPENED_CLASS = "is-active";

  var findById = function findById(id) {
    return doc.getElementById(id);
  };

  var addClass = function addClass(el, className) {
    if (el.classList) {
      el.classList.add(className); // IE 10+
    } else {
      el.className += " " + className; // IE 8+
    }
  };

  /*const removeClass = (el, className) => {
           if (el.classList) {
             el.classList.remove(className); // IE 10+
           }
           else {
                el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' '); // IE 8+
                }
           }*/

  var hasClass = function hasClass(el, className) {
    if (el.classList) {
      return el.classList.contains(className); // IE 10+
    } else {
      return new RegExp("(^| )" + className + "( |$)", "gi").test(el.className); // IE 8+ ?
    }
  };

  var setAttributes = function setAttributes(node, attrs) {
    Object.keys(attrs).forEach(function (attribute) {
      node.setAttribute(attribute, attrs[attribute]);
    });
  };
  var unSelectLinks = function unSelectLinks(elts) {
    elts.forEach(function (link_node) {
      var _setAttributes;

      setAttributes(
        link_node,
        ((_setAttributes = {}),
        _defineProperty(_setAttributes, ATTR_SELECTED, "false"),
        _defineProperty(_setAttributes, "tabindex", "-1"),
        _setAttributes)
      );
    });
  };
  var unSelectContents = function unSelectContents(elts) {
    elts.forEach(function (content_node) {
      content_node.setAttribute(ATTR_HIDDEN, true);
    });
  };

  var selectLink = function selectLink(el) {
    var _setAttributes2;

    var destination = findById(el.getAttribute(ATTR_CONTROLS));
    setAttributes(
      el,
      ((_setAttributes2 = {}),
      _defineProperty(_setAttributes2, ATTR_SELECTED, "true"),
      _defineProperty(_setAttributes2, "tabindex", "0"),
      _setAttributes2)
    );
    destination.removeAttribute(ATTR_HIDDEN);
    setTimeout(function () {
      el.focus();
    }, 0);
    setTimeout(function () {
      history.pushState(
        null,
        null,
        location.pathname +
          location.search +
          "#" +
          el.getAttribute(ATTR_CONTROLS)
      );
    }, DELAY_HASH_UPDATE);
  };

  var selectLinkInList = function selectLinkInList(
    itemsList,
    linkList,
    contentList,
    param
  ) {
    var indice_trouve = undefined;

    itemsList.forEach(function (itemNode, index) {
      if (
        itemNode
          .querySelector("." + TABS_JS_LISTLINK)
          .getAttribute(ATTR_SELECTED) === "true"
      ) {
        indice_trouve = index;
      }
    });
    unSelectLinks(linkList);
    unSelectContents(contentList);
    if (param === "next") {
      selectLink(linkList[indice_trouve + 1]);
      setTimeout(function () {
        linkList[indice_trouve + 1].focus();
      }, 0);
    }
    if (param === "prev") {
      selectLink(linkList[indice_trouve - 1]);
      setTimeout(function () {
        linkList[indice_trouve - 1].focus();
      }, 0);
    }
  };

  /* gets an element el, search if it is child of parent class, returns id of the parent */
  var searchParent = function searchParent(el, parentClass) {
    var found = false;
    var parentElement = el.parentNode;
    while (parentElement && found === false) {
      if (hasClass(parentElement, parentClass) === true) {
        found = true;
      } else {
        parentElement = parentElement.parentNode;
      }
    }
    if (found === true) {
      return parentElement.getAttribute("id");
    } else {
      return "";
    }
  };

  /** Find all tabs inside a container
   * @param  {Node} node Default document
   * @return {Array}
   */
  var $listTabs = function $listTabs() {
    var node =
      arguments.length <= 0 || arguments[0] === undefined ? doc : arguments[0];
    return [].slice.call(node.querySelectorAll("." + TABS_JS));
  };

  /**
   * Build tooltips for a container
   * @param  {Node} node
   */
  var attach = function attach(node) {
    $listTabs(node).forEach(function (tabs_node) {
      var iLisible = Math.random().toString(32).slice(2, 12);
      var prefixClassName =
        tabs_node.hasAttribute(TABS_DATA_PREFIX_CLASS) === true
          ? tabs_node.getAttribute(TABS_DATA_PREFIX_CLASS) + "-"
          : "";
      var hx =
        tabs_node.hasAttribute(TABS_DATA_HX) === true
          ? tabs_node.getAttribute(TABS_DATA_HX)
          : "";
      var hxGeneratedClass =
        tabs_node.hasAttribute(TABS_DATA_GENERATED_HX_CLASS) === true
          ? tabs_node.getAttribute(TABS_DATA_GENERATED_HX_CLASS)
          : TABS_HX_DEFAULT_CLASS;
      var existingHx =
        tabs_node.hasAttribute(TABS_DATA_EXISTING_HX) === true
          ? tabs_node.getAttribute(TABS_DATA_EXISTING_HX)
          : "";
      var $tabList = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_LIST)
      );
      var $tabListItems = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_LISTITEM)
      );
      var $tabListLinks = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_LISTLINK)
      );
      var $tabListPanels = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_CONTENT)
      );
      var noTabSelected = true;

      // container
      addClass(tabs_node, prefixClassName + TABS_STYLE);
      tabs_node.setAttribute("id", TABS_STYLE + iLisible);

      // ul
      $tabList.forEach(function (tabList) {
        var _setAttributes3;

        addClass(tabList, prefixClassName + TABS_LIST_STYLE);
        setAttributes(
          tabList,
          ((_setAttributes3 = {}),
          _defineProperty(_setAttributes3, ATTR_ROLE, TABS_ROLE_TABLIST),
          _defineProperty(_setAttributes3, "id", TABS_LIST_STYLE + iLisible),
          _setAttributes3)
        );
      });
      // li
      $tabListItems.forEach(function (tabListItem, index) {
        var _setAttributes4;

        addClass(tabListItem, prefixClassName + TABS_LISTITEM_STYLE);
        setAttributes(
          tabListItem,
          ((_setAttributes4 = {}),
          _defineProperty(_setAttributes4, ATTR_ROLE, TABS_ROLE_PRESENTATION),
          _defineProperty(
            _setAttributes4,
            "id",
            TABS_LISTITEM_STYLE + iLisible + "-" + (index + 1)
          ),
          _setAttributes4)
        );
      });
      // a
      $tabListLinks.forEach(function (tabListLink) {
        var _setAttributes5, _setAttributes6;

        var idHref = tabListLink.getAttribute("href").replace("#", "");
        var panelControlled = findById(idHref);
        var linkText = tabListLink.innerText;
        var panelSelected =
          tabListLink.hasAttribute(TABS_DATA_SELECTED_TAB) === true;

        addClass(tabListLink, prefixClassName + TABS_LINK_STYLE);
        setAttributes(
          tabListLink,
          ((_setAttributes5 = {
            id: TABS_PREFIX_IDS + idHref
          }),
          _defineProperty(_setAttributes5, ATTR_ROLE, TABS_ROLE_TAB),
          _defineProperty(_setAttributes5, ATTR_CONTROLS, idHref),
          _defineProperty(_setAttributes5, "tabindex", "-1"),
          _defineProperty(_setAttributes5, ATTR_SELECTED, "false"),
          _setAttributes5)
        );

        // panel controlled
        setAttributes(
          panelControlled,
          ((_setAttributes6 = {}),
          _defineProperty(_setAttributes6, ATTR_HIDDEN, "true"),
          _defineProperty(_setAttributes6, ATTR_ROLE, TABS_ROLE_TABPANEL),
          _defineProperty(
            _setAttributes6,
            ATTR_LABELLEDBY,
            TABS_PREFIX_IDS + idHref
          ),
          _setAttributes6)
        );
        addClass(panelControlled, prefixClassName + TABS_CONTENT_STYLE);

        // if already selected
        if (panelSelected && noTabSelected) {
          noTabSelected = false;
          setAttributes(
            tabListLink,
            _defineProperty(
              {
                tabindex: "0"
              },
              ATTR_SELECTED,
              "true"
            )
          );
          setAttributes(
            panelControlled,
            _defineProperty({}, ATTR_HIDDEN, "false")
          );
        }

        // hx
        if (hx !== "") {
          var hx_node = document.createElement(hx);
          hx_node.setAttribute("class", hxGeneratedClass);
          hx_node.setAttribute("tabindex", "0");
          hx_node.innerHTML = linkText;
          panelControlled.insertBefore(hx_node, panelControlled.firstChild);
        }
        // existingHx

        if (existingHx !== "") {
          var $hx_existing = [].slice.call(
            panelControlled.querySelectorAll(existingHx + ":first-child")
          );
          $hx_existing.forEach(function (hx_item) {
            hx_item.setAttribute("tabindex", "0");
          });
        }

        tabListLink.removeAttribute("href");
      });

      if (hash !== "") {
        var nodeHashed = findById(hash);
        if (nodeHashed !== null) {
          // just in case of an dumb error
          // search if hash is current tabs_node
          if (tabs_node.querySelector("#" + hash) !== null) {
            // search if hash is ON tabs
            if (hasClass(nodeHashed, TABS_JS_CONTENT) === true) {
              // unselect others
              unSelectLinks($tabListLinks);
              unSelectContents($tabListPanels);
              // select this one
              nodeHashed.removeAttribute(ATTR_HIDDEN);
              var linkHashed = findById(TABS_PREFIX_IDS + hash);
              setAttributes(
                linkHashed,
                _defineProperty(
                  {
                    tabindex: "0"
                  },
                  ATTR_SELECTED,
                  "true"
                )
              );
              noTabSelected = false;
            } else {
              // search if hash is IN tabs
              var panelParentId = searchParent(nodeHashed, TABS_JS_CONTENT);
              if (panelParentId !== "") {
                // unselect others
                unSelectLinks($tabListLinks);
                unSelectContents($tabListPanels);
                // select this one
                var panelParent = findById(panelParentId);
                panelParent.removeAttribute(ATTR_HIDDEN);
                var linkParent = findById(TABS_PREFIX_IDS + panelParentId);
                setAttributes(
                  linkParent,
                  _defineProperty(
                    {
                      tabindex: "0"
                    },
                    ATTR_SELECTED,
                    "true"
                  )
                );
                noTabSelected = false;
              }
            }
          }
        }
      }

      // if no selected => select first
      if (noTabSelected === true) {
        setAttributes(
          $tabListLinks[0],
          _defineProperty(
            {
              tabindex: "0"
            },
            ATTR_SELECTED,
            "true"
          )
        );
        var panelFirst = findById($tabListLinks[0].getAttribute(ATTR_CONTROLS));
        panelFirst.removeAttribute(ATTR_HIDDEN);
      }
    });
  };

  /* listeners */
  ["click", "keydown"].forEach(function (eventName) {
    //let isCtrl = false;

    doc.body.addEventListener(
      eventName,
      function (e) {
        // click on a tab link or on something IN a tab link
        var parentLink = searchParent(e.target, TABS_JS_LISTLINK);
        if (
          (hasClass(e.target, TABS_JS_LISTLINK) === true ||
            parentLink !== "") &&
          eventName === "click"
        ) {
          var linkSelected =
            hasClass(e.target, TABS_JS_LISTLINK) === true
              ? e.target
              : findById(parentLink);
          var parentTabId = searchParent(e.target, TABS_JS);
          var parentTab = findById(parentTabId);
          //let $parentListItems = [].slice.call(parentTab.querySelectorAll('.' + TABS_JS_LISTITEM));
          var $parentListLinks = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
          );
          var $parentListContents = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_CONTENT)
          );

          // aria selected false on all links
          unSelectLinks($parentListLinks);
          // add aria-hidden on all tabs contents
          unSelectContents($parentListContents);
          // add aria selected on selected link + show linked panel
          selectLink(linkSelected);

          e.preventDefault();
        }

        // Key down on tabs
        if (
          (hasClass(e.target, TABS_JS_LISTLINK) === true ||
            parentLink !== "") &&
          eventName === "keydown"
        ) {
          //let linkSelected = hasClass( e.target, TABS_JS_LISTLINK) === true ? e.target : findById( parentLink );
          var parentTabId = searchParent(e.target, TABS_JS);
          var parentTab = findById(parentTabId);
          var $parentListItems = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTITEM)
          );
          var $parentListLinks = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
          );
          var $parentListContents = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_CONTENT)
          );
          var firstLink = $parentListItems[0].querySelector(
            "." + TABS_JS_LISTLINK
          );
          var lastLink = $parentListItems[
            $parentListItems.length - 1
          ].querySelector("." + TABS_JS_LISTLINK);

          // strike home on a tab => 1st tab
          if (e.keyCode === 36) {
            unSelectLinks($parentListLinks);
            unSelectContents($parentListContents);
            selectLink(firstLink);

            e.preventDefault();
          }
          // strike end on a tab => last tab
          else if (e.keyCode === 35) {
            unSelectLinks($parentListLinks);
            unSelectContents($parentListContents);
            selectLink(lastLink);

            e.preventDefault();
          }
          // strike up or left on the tab => previous tab
          else if ((e.keyCode === 37 || e.keyCode === 38) && !e.ctrlKey) {
            if (firstLink.getAttribute(ATTR_SELECTED) === "true") {
              unSelectLinks($parentListLinks);
              unSelectContents($parentListContents);
              selectLink(lastLink);

              e.preventDefault();
            } else {
              selectLinkInList(
                $parentListItems,
                $parentListLinks,
                $parentListContents,
                "prev"
              );
              e.preventDefault();
            }
          }
          // strike down or right in the tab => next tab
          else if ((e.keyCode === 40 || e.keyCode === 39) && !e.ctrlKey) {
            if (lastLink.getAttribute(ATTR_SELECTED) === "true") {
              unSelectLinks($parentListLinks);
              unSelectContents($parentListContents);
              selectLink(firstLink);

              e.preventDefault();
            } else {
              selectLinkInList(
                $parentListItems,
                $parentListLinks,
                $parentListContents,
                "next"
              );
              e.preventDefault();
            }
          }
        }

        // Key down in tab panels
        var parentTabPanelId = searchParent(e.target, TABS_JS_CONTENT);
        if (parentTabPanelId !== "" && eventName === "keydown") {
          (function () {
            var linkSelected = findById(
              findById(parentTabPanelId).getAttribute(ATTR_LABELLEDBY)
            );
            var parentTabId = searchParent(e.target, TABS_JS);
            var parentTab = findById(parentTabId);
            var $parentListItems = [].slice.call(
              parentTab.querySelectorAll("." + TABS_JS_LISTITEM)
            );
            var $parentListLinks = [].slice.call(
              parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
            );
            var $parentListContents = [].slice.call(
              parentTab.querySelectorAll("." + TABS_JS_CONTENT)
            );
            var firstLink = $parentListItems[0].querySelector(
              "." + TABS_JS_LISTLINK
            );
            var lastLink = $parentListItems[
              $parentListItems.length - 1
            ].querySelector("." + TABS_JS_LISTLINK);

            // strike up + ctrl => go to header
            if (e.keyCode === 38 && e.ctrlKey) {
              setTimeout(function () {
                linkSelected.focus();
              }, 0);
              e.preventDefault();
            }
            // strike pageup + ctrl => go to prev header
            if (e.keyCode === 33 && e.ctrlKey) {
              // go to header
              linkSelected.focus();
              e.preventDefault();
              // then previous
              if (firstLink.getAttribute(ATTR_SELECTED) === "true") {
                unSelectLinks($parentListLinks);
                unSelectContents($parentListContents);
                selectLink(lastLink);
              } else {
                selectLinkInList(
                  $parentListItems,
                  $parentListLinks,
                  $parentListContents,
                  "prev"
                );
              }
            }
            // strike pagedown + ctrl => go to next header
            if (e.keyCode === 34 && e.ctrlKey) {
              // go to header
              linkSelected.focus();
              e.preventDefault();
              // then next
              if (lastLink.getAttribute(ATTR_SELECTED) === "true") {
                unSelectLinks($parentListLinks);
                unSelectContents($parentListContents);
                selectLink(firstLink);
              } else {
                selectLinkInList(
                  $parentListItems,
                  $parentListLinks,
                  $parentListContents,
                  "next"
                );
              }
            }
          })();
        }

        // click on a tab link
        var parentLinkToPanelId = searchParent(e.target, TABS_JS_LINK_TO_TAB);
        if (
          (hasClass(e.target, TABS_JS_LINK_TO_TAB) === true ||
            parentLinkToPanelId !== "") &&
          eventName === "click"
        ) {
          var panelSelectedId =
            hasClass(e.target, TABS_JS_LINK_TO_TAB) === true
              ? e.target.getAttribute("href").replace("#", "")
              : findById(parentLinkToPanelId).replace("#", "");
          var panelSelected = findById(panelSelectedId);
          var buttonPanelSelected = findById(
            panelSelected.getAttribute(ATTR_LABELLEDBY)
          );
          var parentTabId = searchParent(e.target, TABS_JS);
          var parentTab = findById(parentTabId);
          //let $parentListItems = [].slice.call(parentTab.querySelectorAll('.' + TABS_JS_LISTITEM));
          var $parentListLinks = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
          );
          var $parentListContents = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_CONTENT)
          );

          unSelectLinks($parentListLinks);
          unSelectContents($parentListContents);
          selectLink(buttonPanelSelected);

          e.preventDefault();
        }
      },
      true
    );
  });

  var onLoad = function onLoad() {
    attach();
    document.removeEventListener("DOMContentLoaded", onLoad);
  };

  document.addEventListener("DOMContentLoaded", onLoad);

  window.van11yAccessibleTabPanelAria = attach;
})(document);
</script>
  <script type="text/javascript">
        $(document).ready(function() {
            var text = ['DevOps', 'Cloud Services', 'Site Services', 'Support']; //Text to be shown
            var index = 0; //Starting index for rotating text
            var changeText = setInterval(function() {
                if (index == text.length - 1)
                    index = 0;
                else
                    index++;
                $('.rotate-text').animate({
                    "opacity": "0",
                    "margin-left": "10px"
                }, 500, function() {
                    $('.rotate-text').text(text[index]);
                    $('.rotate-text').animate({
                        "opacity": "1",
                        "margin-left": "0"
                    }, 500);
                });

            }, 2000);
        });
    </script>
						<script>
						jQuery(document).ready(function($) {
    var tabwrapWidth= $('.tabs-wrapper').outerWidth();
    var totalWidth=0;
    jQuery("ul li").each(function() { 
      totalWidth += jQuery(this).outerWidth(); 
    });
    if(totalWidth > tabwrapWidth){
      $('.scroller-btn').removeClass('inactive');
    }
    else{
      $('.scroller-btn').addClass('inactive');
    }

    if($("#scroller").scrollLeft() == 0 ){
      $('.scroller-btn.left').addClass('inactive');
    }
    else{
       $('.scroller-btn.left').removeClass('inactive');
    }
		var liWidth= $('#scroller li').outerWidth();
		var liCount= $('#scroller li').length;
		var scrollWidth = liWidth * liCount;

				$('.right').on('click', function(){
          $('.nav-tabs').animate({scrollLeft: '+=200px'}, 300);
          console.log($("#scroller").scrollLeft() + " px");
				});
				
				$('.left').on('click', function(){
					$('.nav-tabs').animate({scrollLeft: '-=200px'}, 300);
				});
      scrollerHide()
     
      function scrollerHide(){
        var scrollLeftPrev = 0;
        $('#scroller').scroll(function () {
            var $elem=$('#scroller');
            var newScrollLeft = $elem.scrollLeft(),
                width=$elem.outerWidth(),
                scrollWidth=$elem.get(0).scrollWidth;
            if (scrollWidth-newScrollLeft==width) {
                $('.right.scroller-btn').addClass('inactive');
            }
            else{

                 $('.right.scroller-btn').removeClass('inactive');
            }
            if (newScrollLeft === 0) {
              $('.left.scroller-btn').addClass('inactive');
            }
            else{

                 $('.left.scroller-btn').removeClass('inactive');
            }
            scrollLeftPrev = newScrollLeft;
        });
      }
	});
						</script>
		
    
	<script>
	$(document).ready(function () {
  $(".at-title").click(function () {
    $(this).toggleClass("active").next(".at-tab").slideToggle().parent().siblings().find(".at-tab").slideUp().prev().removeClass("active");
  });
});
	</script>
<script>
const accordionItemHeaders = document.querySelectorAll(
  ".accordion-item-header"
);

accordionItemHeaders.forEach((accordionItemHeader) => {
  accordionItemHeader.addEventListener("click", (event) => {
    // Uncomment in case you only want to allow for the display of only one collapsed item at a time!

    const currentlyActiveAccordionItemHeader = document.querySelector(
      ".accordion-item-header.active"
    );
    if (
      currentlyActiveAccordionItemHeader &&
      currentlyActiveAccordionItemHeader !== accordionItemHeader
    ) {
      currentlyActiveAccordionItemHeader.classList.toggle("active");
      currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
    }
    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if (accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    } else {
      accordionItemBody.style.maxHeight = 0;
    }
  });
});
</script>


<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>