<style>

.page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
}

.page-header nav {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.page-header button {
  display: flex;
  align-items: center;
  font-size: 16px;
  font-weight: bold;
  padding: 14px 20px;
  border-radius: 10px;
  color: white;
  background: #111;
  transition: background 0.3s;
}

.page-header button:hover {
  background: #0ab8f6;
}

.page-header.is-sticky {
  position: fixed;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.1);
  padding: 0;
  backdrop-filter: blur(10px);
  animation: slideDown 0.35s ease-out;
  z-index: 9999999999999999999999999999999;
}

.page-header.is-sticky img {
  max-width: 80%;
  margin-top: 0;
}

.page-header.is-sticky button {
  font-size: 14px;
  padding: 7px 10px;
}

@keyframes slideDown {
  from {
    transform: translateY(-100%);
  }
  to {
    transform: translateY(0);
  }
}
    .header-1 {
    position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
  width: 100%;
  height: auto;
  box-shadow: var(--shadow-medium);
  padding:15px 0;
}

.brand {
  font-family: inherit;
  font-size: 1.6rem;
  font-weight: 700;
  line-height: 1.5;
  text-transform: uppercase;
  color: var(--color-blue-300);
}
.dropdown-title-1 span.text-base-1
{color: #007bff;
  font-weight: 600 !important;}
.navbar-1 {
  width: 100%;
  height: 4.25rem;
  margin-inline: auto;
}
.navbar-inner-1 {
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: 100%;
  height: 100%;
}
.navbar-block-1 {
  position: absolute;
  left: 0;
  width: 100%;
  height: calc(100vh - 4rem);
  opacity: 0;
  overflow: auto;
  pointer-events: none;
  transition: opacity 0.4s ease;
}
.navbar-block-1.is-active {
  opacity: 1;
  pointer-events: initial;
}

@media screen and (min-width: 62rem) {
  .navbar-1 {
    display: flex;
    justify-content: space-between;
  }
  .navbar-block-1 {
    position: initial;
    height: initial;
    opacity: 1;
    overflow: auto;
    pointer-events: visible;
    background: none;
    transition: none;
  }
}
.menu-1 {
  padding-block: 1rem;
  padding-inline: auto;
  
}
.menu-link-1 {
  font-size: var(--text-base);
  font-weight: 500;
  line-height: inherit;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-block: 0.5rem;
  padding-inline: 1.5rem;
  color: var(--color-black-500);
  transition: color 0.3s ease;
}

@media screen and (min-width: 62rem) {
  .menu-1 {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    -moz-column-gap: 1rem;
         column-gap: 1rem;
    height: 100%;
    padding: unset;
  }
  .menu-item-1 {
    display: flex;
    cursor: pointer;
    padding-inline: 0.5rem;
  }
  .menu-link-1 {
    padding: unset;
    color:#fff;
  }
}
.dropdown-toggle-1 {
  cursor: pointer;
  outline: none;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}
.dropdown-toggle-1 i.bx-1 {
  font-size: 1.5rem;
  line-height: inherit;
  transition: rotate 0.4s ease;
}
.dropdown-content-1 {
  height: 0;
  overflow: hidden;
  background-color: var(--color-white-100);
  transition: height 0.5s ease;
}
.dropdown-column-1, .dropdown-group-1, .dropdown-items-1 {
  display: grid;
}
.dropdown-group-1 {
  padding-bottom: 1rem;
  padding-inline: 1.5rem;
}
.dropdown-title-1 {
  display: flex;
  -moz-column-gap: 1rem;
       column-gap: 1rem;
  align-items: center;
  padding-inline: 1rem;
}


.dropdown-icon-1 i.bx-1 {
  font-size: 1.5rem;
  line-height: 1.5;
  color: var(--color-blue-300);
}
.dropdown-items-1 {
  row-gap: 0.5rem;
  padding-inline: 2.0rem;
  padding-top: 0.35rem;
}
.dropdown-link-1 {
  font-size: var(--text-base);
  font-weight: 400;
  line-height: inherit;
  color: #000;
  transition: color 0.3s ease;
}

.dropdown-link-1:hover{ color:#5e5d5d;}
.dropdown-show-1 > .dropdown-toggle-1 i.bx-1 {
  rotate: 180deg;
}
.dropdown-block-1 {
  display: flex;
  align-items: flex-start;
  -moz-column-gap: 1rem;
       column-gap: 1rem;
  padding-top: 1rem;
  padding-inline: 1rem;
}

ul.dropdown-items-1 li{ list-style-type: none; }

@media screen and (min-width: 62rem) {
  .dropdown-toggle-1 {
    -moz-column-gap: 0.35rem;
         column-gap: 0.35rem;
    pointer-events: none;
  }
  .dropdown-content-1 {
    position: absolute;
    left: 0;
    right: 0;
    top: 6rem;
    opacity: 0;
    height: -webkit-max-content;
    height: -moz-max-content;
    height: max-content;
    pointer-events: none;
    background-color: var(--color-white-100);
    border-top: 2px solid var(--color-white-200);
    box-shadow: 1px 1px 1px 1px #ccc;
    transition: top 0.4s, opacity 0.3s ease;
    width: 60%;
    margin: 0 auto;
    border-radius:15px;
  }
  .dropdown-column-1 {
    grid-template-columns: repeat(3, minmax(0, 1fr));
    -moz-column-gap: 2rem;
         column-gap: 2rem;
    max-width: 75rem;
    margin-inline: auto;
  }
  .dropdown-group-1 {
    align-content: baseline;
    row-gap: 1.25rem;
    padding-block: 3rem;
  }
  .dropdown-group-1:first-child, .dropdown-group-1:last-child {
    margin: unset;
  }
  .dropdown-items-1 {
    padding-top: 10px;
  }
  .dropdown-block-1 {
    padding-top: 10px;
    padding-inline: 10px;
  }
  .dropdown-1:hover > .dropdown-content-1 {
    cursor: initial;
    top: 3.3rem;
    opacity: 1;
    pointer-events: initial;
    background-color:#fff;
  }
  .dropdown-1:hover > .dropdown-toggle-1 i.bx-1 {
    rotate: 180deg;
  }
}
.burger-1 {
  position: relative;
  display: block;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  width: 1.5rem;
  height: 1rem;
  border: none;
  outline: none;
  visibility: visible;
}
.burger-line-1 {
  position: absolute;
  display: block;
  right: 0;
  width: 100%;
  height: 2.15px;
  opacity: 1;
  rotate: 0deg;
  border-radius: 0.15rem;
  background-color: var(--color-black-500);
  transition: all 0.3s ease;
  background-color: #fff;
}
.burger-line-1:nth-child(1) {
  top: 0px;
}
.burger-line-1:nth-child(2) {
  top: 0.5rem;
  width: 70%;
}
.burger-line-1:nth-child(3) {
  top: 1rem;
}
.burger-1.is-active > .burger-line:nth-child(1) {
  top: 0.5rem;
  rotate: 135deg;
}
.burger-1.is-active > .burger-line:nth-child(2) {
  opacity: 0;
}
.burger-1.is-active > .burger-line:nth-child(3) {
  top: 0.5rem;
  rotate: -135deg;
}

@media screen and (min-width: 62rem) {
  .burger-1 {
    display: none;
    visibility: hidden;
  }
  
}
</style>

<div class="progress-wrap active-progress">
      <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
<path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear; stroke-dasharray: 307.919px, 307.919px; stroke-dashoffset: 133.777px; stroke: rgb(252, 2, 3);"></path>
</svg>
  </div>
<header class="header-1 page-header head" id="header-1">
  <nav class="navbar-1 container">
    <div class="navbar-inner-1">
    <a href="index.php"><img src="img/snwn-logo.png"></a>
      <div class="burger-1" id="burger-1">
        <span class="burger-line-1"></span>
        <span class="burger-line-1"></span>
        <span class="burger-line-1"></span>
      </div>
    </div>
    <div class="navbar-block-1" id="menu-1">
      <ul class="menu-1 new-menu-top">
        <li class="menu-item-1"><a href="#" class="menu-link-1">MANAGED IT SERVICES </a></li>
        <li class="menu-item-1 dropdown-1">
          <span class="dropdown-toggle-1 menu-link-1">
            SOLUTION <i class="fa fa-chevron-down"></i>
          </span>
          <div class="dropdown-content-1">
            <div class="dropdown-column-1">
              <div class="dropdown-group-1">
                <div class="dropdown-title-1">
                  <span class="dropdown-icon-1"><i class="bx-1 bx-code-1"></i></span>
                  <span class="text-base-1 font-medium"> Web Development</span>
                </div>
                <ul class="dropdown-items-1">
                  <li><a href="auto-scaling.php" class="dropdown-link-1">Auto Scaling</a></li>
                  <li><a href="azure-management.php" class="dropdown-link-1">Azure Management</a></li>
                  <li><a href="dediate-support.php" class="dropdown-link-1">Dedicate Support</a></li>
                  <li><a href="dev-ops.php" class="dropdown-link-1">Dev Ops</a></li>
                </ul>
              </div>
              <div class="dropdown-group-1">
                <div class="dropdown-title-1">
                  <span class="dropdown-icon-1"><i class="bx-1 device"></i></span>
                  <span class="text-base-1 font-medium">Web Design  </span>
                </div>
                <ul class="dropdown-items-1">
                  <li><a href="webhosting-support.php" class="dropdown-link-1">Webhosting Support</a></li>
                  <li><a href="seo-services.php" class="dropdown-link-1">SEO Services</a></li>
                  <li><a href="live-chat-support.php" class="dropdown-link-1">Live Chat Support</a></li>
                  <li><a href="#" class="dropdown-link-1">Webflow for Beginners</a></li>
                </ul>
              </div>
              <div class="dropdown-group-1">
                <div class="dropdown-title-1">
                  <span class="dropdown-icon-1"><i class="bx-1 bx-bookmark-1"></i></span>
                  <span class="text-base-1 font-medium">Certification</span>
                </div>
                <ul class="dropdown-items-1">
                  <li><a href="#" class="dropdown-link-1">Free Certificates</a></li>
                  <li><a href="#" class="dropdown-link-1">Courses Certificates</a></li>
                  <li><a href="#" class="dropdown-link-1">Premium Certificates</a></li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        <li class="menu-item-1 dropdown-1">
          <span class="dropdown-toggle-1 menu-link-1">
          CLOUD SERVISES  <i class="fa fa-chevron-down"></i></i>
          </span>
          <div class="dropdown-content-1">
            <div class="dropdown-column-1">
              <div class="dropdown-group-1">
                <div class="dropdown-block-1">
                  <span class="dropdown-icon-1"><i class="bx-1 bx-podcast-1"></i></span>
                  <div class="dropdown-inner-1">
                    <a href="#" class="text-base-1 font-medium">Podcasts</a>
                    <p class="text-base-1 font-normal">
                      Hear and enjoy our inspiration podcast together with us.
                    </p>
                  </div>
                </div>
                <div class="dropdown-block-1">
                  <span class="dropdown-icon-1"><i class="bx-1 bx-video-1"></i></span>
                  <div class="dropdown-inner-1">
                    <a href="#" class="text-base-1 font-medium">Tutorials</a>
                    <p class="text-base-1 font-normal">
                      Learn video tutorial with our professional instructors.
                    </p>
                  </div>
                </div>
              </div>
              <div class="dropdown-group-1">
                <div class="dropdown-block-1">
                  <span class="dropdown-icon-1"><i class="bx-1 bx-book-open-1"></i></span>
                  <div class="dropdown-inner-1">
                    <a href="#" class="text-base-1 font-medium">Help Center</a>
                    <p class="text-base-1 font-normal">
                      Discover how to register, install and use our products.
                    </p>
                  </div>
                </div>
                <div class="dropdown-block-1">
                  <span class="dropdown-icon-1"><i class="bx bx-bookmark"></i></span>
                  <div class="dropdown-inner-1">
                    <a href="#" class="text-base-1 font-medium">Community</a>
                    <p class="text-base-1 font-normal">
                      Share and connect with other user in our communities.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
        <li class="menu-item-1"><a href="#" class="menu-link-1">ABOUT US</a></li>
        <li class="menu-item-1"><a href="#" class="menu-link-1">CONTACT US</a></li>
      </ul>
    </div>
  </nav>
</header>
    <!-- Header End -->
     
    <script>
document.addEventListener("DOMContentLoaded", function() {
  const header = document.querySelector(".head");
  const toggleClass = "is-sticky";
  const scrollThreshold = 300; // Adjust this value as needed

  // Add a class to indicate the header is not sticky initially
  header.classList.add("not-sticky");

  window.addEventListener("scroll", () => {
    const currentScroll = window.pageYOffset;
    if (currentScroll > scrollThreshold && header.classList.contains("not-sticky")) {
      // Add sticky behavior when scrolled beyond the threshold
      header.classList.remove("not-sticky");
      header.classList.add(toggleClass);
    } else if (currentScroll <= scrollThreshold && header.classList.contains(toggleClass)) {
      // Remove sticky behavior when scrolled back up
      header.classList.remove(toggleClass);
      header.classList.add("not-sticky");
    }
  });
});
</script>
    <script>// Toggle to show and hide navbar menu
const navbarMenu = document.getElementById("menu-1");
const burgerMenu = document.getElementById("burger-1");

burgerMenu.addEventListener("click", () => {
  navbarMenu.classList.toggle("is-active");
  burgerMenu.classList.toggle("is-active");
});

// Toggle to show and hide dropdown menu
const dropdown = document.querySelectorAll(".dropdown-1");

dropdown.forEach((item) => {
  const dropdownToggle = item.querySelector(".dropdown-toggle-1");

  dropdownToggle.addEventListener("click", () => {
    const dropdownShow = document.querySelector(".dropdown-show-1");
    toggleDropdownItem(item);

    // Remove 'dropdown-show' class from other dropdown
    if (dropdownShow && dropdownShow != item) {
      toggleDropdownItem(dropdownShow);
    }
  });
});

// Function to display the dropdown menu
const toggleDropdownItem = (item) => {
  const dropdownContent = item.querySelector(".dropdown-content-1");

  // Remove other dropdown that have 'dropdown-show' class
  if (item.classList.contains("dropdown-show-1")) {
    dropdownContent.removeAttribute("style");
    item.classList.remove("dropdown-show-1");
  } else {
    // Added max-height on active 'dropdown-show' class
    dropdownContent.style.height = dropdownContent.scrollHeight + "px";
    item.classList.add("dropdown-show-1");
  }
};

// Fixed dropdown menu on window resizing
window.addEventListener("resize", () => {
  if (window.innerWidth > 992) {
    document.querySelectorAll(".dropdown-content-1").forEach((item) => {
      item.removeAttribute("style");
    });
    dropdown.forEach((item) => {
      item.classList.remove("dropdown-show-1");
    });
  }
});

// Fixed navbar menu on window resizing
window.addEventListener("resize", () => {
  if (window.innerWidth > 992) {
    if (navbarMenu.classList.contains("is-active")) {
      navbarMenu.classList.remove("is-active");
      burgerMenu.classList.remove("is-active");
    }
  }
});
</script>