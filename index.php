<!DOCTYPE html>
<html lang="zxx"><head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="icon" href="img/fevicon.png" sizes="192x192">	
<style>
    .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#23428B 0%,#1D094C 100%);
}
.logo:hover img {
  -webkit-transition: all 200ms ease-in;
  -webkit-transform: scale(1.1);
  -ms-transition: all 200ms ease-in;
  -ms-transform: scale(1.1);
  -moz-transition: all 200ms ease-in;
  -moz-transform: scale(1.1);
  transition: all 200ms ease-in;
  transform: scale(1.1);
}
.logos img:hover, .logo img:hover {
  -webkit-filter: none;
  filter: none;
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0" /></filter></svg>#filter');
  -webkit-filter: grayscale(1);
  filter: grayscale(1);
  opacity: 1;
}
.color-fill:hover{ background:#fff !important; transition:1s; border: 0.5px solid #eaeaea; }
.logos img{
  -webkit-filter: gray;
  filter: gray;
  -webkit-filter: grayscale(50);
  filter: grayscale(50);
  -webkit-transition: .4s ease-in-out;
  transition: .4s ease-in-out;
  opacity: .7;}

  .logos {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(9em, 1fr));
  list-style: none;
  margin: 0;
  overflow: hidden;
  padding: 0;
  margin-bottom:40px;
}

.logos > li {
  aspect-ratio: 1;
  /*background-color: rgba(255, 255, 255, 0.2);*/
  box-shadow: 0 0 0 1px #1a1a1a;
}

.logo {
  display: grid;
  padding: 0.4em;
  place-items: center;
}

.logo:hover img{
-webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.1);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}

.logo figcaption {
  margin-top: 1em;
}

input[type="search"] {
  border: 0;
  font-size: 2rem;
  padding: 0.5em 2em;
  text-align: center;
  width: 100%;
}
h4.hpaline {
    display: none;
}
.hero-banner h1 {
    font-size: 70px;
}

.animated-text {
    animation-fill-mode: both;
    animation-duration: 1s;
}
.fadeInDown {
    animation-name: fadeInDown;
    visibility:visible;
}
.tabs {
  width: 100%;
 /* background-color: #09F;*/
  border-radius: 5px 5px 5px 5px;
}
ul#tabs-nav {
  list-style: none;
  margin: 0;
  padding: 137px 0 100px 0;
  overflow: auto;
}
ul#tabs-nav li {
  /*float: left;*/
  font-weight: bold;
  margin-right: 2px;
  padding: 8px 10px;
  border-radius: 5px 5px 5px 5px;
  /*border: 1px solid #d5d5de;
  border-bottom: none;*/
  cursor: pointer;
  display: inline-block;
}
ul#tabs-nav li:hover,
ul#tabs-nav li.active {
 /* background-color: #08E;*/
}
#tabs-nav li a {
  text-decoration: none;
  color: #FFF;
  font-size: 24px;
  padding: 0px 20px 0px 56px;
}
.tab-content {
  padding: 10px;
  /*border: 5px solid #09F;
  background-color: #FFF;*/
  height:452px;
}
.slidersection{
    background-color: #000;
    text-align: center;
}
.slidersection h2{color:#fff; padding: 73px 0px 3px 0; font-size: 55px;}
.slidersection h5 {
  color: #fff;
  padding: 0px 0px 3px 0;
  font-size: 55px;
  z-index: -0;
  margin-top: 65px;
  text-transform: uppercase;
  position: relative;
  margin-bottom: 30px;
}
.slidersection p{color:#fff; padding: 0px 50px 50px 80px;  
    font-size: 18px; }


    

   

@keyframes scroll {
  0% {
    transform: translateX(0);
  }
  100% {
    transform: translateX(calc(-250px * 7));
  }
}
.slider {
  height: 100px;
  margin: auto;
  overflow: hidden;
  position: relative;
  width: auto;
}
.slider .slide-track {
  animation: scroll 40s linear infinite;
  display: flex;
  width: calc(250px * 14);
}
.slider .slide {
  height: 100px;
  width: 250px;
}



    .awardone{ position: relative; padding: 60px 0 0 0; background-color: transparant; }
.awardone .cmp-award{ position: relative;}
.awardone .cmp-award h5{ 
  color: #000;
  padding: 5px 0px 20px 0px;}
.awardone .cmp-award p{ color: #000; font-size: 18px;}
.awardone .cmp-award .cnt{     padding: 12px 20px 15px 0px; position: relative; }
.awardone .cmp-award .img-award{ position: relative; text-align: center;}
.awardone .cmp-award .img-award img{padding: 15px 0px 40px 0px;}
.awardone .cmp-award .img-award-1 img{padding: 15px 0px 40px 0px;}
.awardone .cmp-award .img-award-1{ position: relative; text-align: center;}
.awardone .cmp-award .img-award:after{ position: absolute; content: ""; left: -33px ; top: 0; border-right:2px solid #c6c6c6; height: 100%; }



.service-section h2{ padding: 0px 0 50px 0; margin-top:100px; text-align: center; font-size: 48px;}
.service-section p{ padding: 0px 0 0px 0;text-align: left; }
.service-section .project{ padding: 40px 10px 20px 10px;
  position: relative;
  border-radius: 8px;
  margin: 19px 0 60px 0;
  height: 360px;
}
.service-section .project:hover{  background-color: transparant !important;  position: relative;}

.service-section .project h5{ text-align:left;padding: 25px 0 10px 15px;    color: #000;
  font-size: 20px; z-index: 333; }
.service-section .project p{ text-align: left;padding: 5px 15px 0px 15px;color: grey; z-index: 333; font-size:14px;}

.service-section .img svg {
  margin: 0 auto;
    margin-left: auto;
  width: 21%;
  padding: 15px 0 0px 0;
  z-index: 333;
  margin-left: 15px;
}




.btn-page {
  text-align: center;
  color: #fff;
  display: block;
  font-weight: 700;
  padding: 12px 10px;
  width: 200px;
  margin: 0 auto;
  margin-top: 5px;
  margin-bottom: 50px;
  border-radius: 45px;
}

.bttn-read-page {
  color: #fff !important;
  font-weight: 600;
  display: block;
  position: relative;
  margin-top: 25px;
  margin-left: 9px;
  background-color: #540113;
  width: 231px;
  padding: 6px 14px;
  border-radius: 31px;
  font-size: 18px;
  margin: 0 auto;
}



    




.slide-container {
  padding: 30px 10px;
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  box-shadow: 2px 2px 2px 2px #0000002a;
}

.slide-container h5 {
  color: #fff;
}

.slide-container a {
  color: #fff !important;
}

.slide-nav {
  position: relative;
  height: 60px;
  width: 80%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 0px;
  margin-bottom: 50px;
}

.slide-nav::after {
  content: "";
  position: absolute;
  width: 100%;
  height: 1px;
  top: 58px;
}

.slide-container .slide-nav label {
  position: relative;
  display: block;
  height: 100%;
  width: 100%;
  text-align: center;
  line-height: 50px;
  cursor: pointer;
  font-size: 1.2rem;
  font-weight: 500;
  transition: all 0.3s;
  color: #fff;
}

.slide-container .slide-nav label::after {
  content: "";
  position: absolute;
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 0;
  height: 2px;
  background: rgb(179, 29, 63); /* Using the same color as the slider */
  transition: all 0.3s;
}

.slide-container .slide-nav label:hover::after {
  width: 100%;
}

input[type="radio"] {
  display: none;
}

.slide-container .slider {
  position: absolute;
  height: 4.5px;
  background-color: rgb(0, 211, 211);
  border-radius: 20px;
  width: 20%;
  z-index: 1;
  left: 0;
  bottom: 0;
  transition: all 0.3s;
  filter: blur(0.4px);
}

#home:checked ~ .slide-nav .home::after,
#features:checked ~ .slide-nav .features::after,
#blogs:checked ~ .slide-nav .blogs::after,
#about:checked ~ .slide-nav .about::after,
#contact:checked ~ .slide-nav .contact::after {
  width: 100%;
}

#home:checked ~ .slide-nav .slider {
  left: 0%;
}

#features:checked ~ .slide-nav .slider {
  left: 20%;
}

#blogs:checked ~ .slide-nav .slider {
  left: 40%;
}

#about:checked ~ .slide-nav .slider {
  left: 60%;
}

#contact:checked ~ .slide-nav .slider {
  left: 80%;
}

.slide-tab {
  height: auto;
}



.home-tab p {
  text-align: center;
}

.slide-tab a {
  color: #000;
  text-decoration: none;
}

.about-tab p {
  text-align: center;
}

.features-tab,
.blogs-tab,
.about-tab,
.contact-tab {
  display: none;
}

#features:checked ~ .tab-section .home-tab {
  display: none;
}

#features:checked ~ .tab-section .features-tab {
  display: block;
}

#blogs:checked ~ .tab-section .features-tab,
#blogs:checked ~ .tab-section .home-tab {
  display: none;
}

#blogs:checked ~ .tab-section .blogs-tab {
  display: block;
}

#about:checked ~ .tab-section .features-tab,
#about:checked ~ .tab-section .home-tab,
#about:checked ~ .tab-section .blogs-tab,
#about:checked ~ .tab-section .contact-tab {
  display: none;
}

#about:checked ~ .tab-section .about-tab {
  display: block;
}

#contact:checked ~ .tab-section .features-tab,
#contact:checked ~ .tab-section .home-tab,
#contact:checked ~ .tab-section .blogs-tab,
#contact:checked ~ .tab-section .about-tab {
  display: none;
}
.tab-section{ height:550px; }
#contact:checked ~ .tab-section .contact-tab {
  display: block;
}
/*.project.color-fill {
  background: linear-gradient(to bottom right, #fef2f4 25%, #FFF9FB 100%);
    transition: background-color 0.5s ease; /* Smooth transition */
    border: 0.5px solid #eaeaea;
}*/

/* Media for mobile */
@media only screen and (max-width: 576px) {
  html {
    font-size: 14px;
  }

  .slide-nav label {
    font-size: 1rem !important;
  }

  .slide-nav {
    width: 90%;
  }
}

@media only screen and (max-width: 360px) {
  html {
    font-size: 12px;
  }

  .slide-nav {
    width: 90%;
  }

  .slide-nav label {
    font-size: 1rem;
  }
}

.videoWrapper {
  display: block;
  float: left;
  margin-top: 30px;
}

#bg-animation {
  display: block;
  position: absolute;
  width: 96vw;
  height: 80vh;
  z-index: -1;
}
.wrapper {
  display: block;
  position: absolute;
  width: 100%;
  
  z-index: 0;
}



main {
    margin-top: 60px; /* Adjust based on header height */
    padding: 20px;
}

.sticky-section {
    position: sticky;
    top: 0;
    background-color: #f1f1f1;
    padding: 20px;
    z-index: 999;
}

.sticky-section .content {
    background-color: #fff;
    padding: 20px;
    border: 1px solid #ddd;
    border-radius: 5px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
}

.about-tab h2{ margin-bottom:40px; }

.card {
  height: 80vh;
  color: 000;
  display: flex;
  justify-content: center;
  align-items: center;
  position: sticky;
  border: none !important;
  top: 0;
}

.card-content {
 display: block;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 0;
}

.bw {
  background: url("#");
  background-size: cover;
  color: black;

  width: 100%;
  background-attachment: fixed;
  
}

.bgs-all{ background: url("img/snwnbcgrad.svg");
  background-size: cover;
  color: black;
  width: 100%;
  
  background-repeat: no-repeat; }

.tokyo {
  background: url("#");
  background-size: cover;
}

.mountains {
  background: url("#");
  background-size: cover;
}

.balloons {
  background: url("#");
  background-size: cover;
}











/* Boring Stuff */


.service-section .header-3{
	
	justify-content: center;
	align-items: center;
border:1px solid #e3e3e3;  }
.service-section .content{}



.header-3 {
  background: linear-gradient(-38deg, #FFF, #FFDDE2);
    background-size: auto;
  
  animation: verticalBreathing 10s ease infinite;
}

@keyframes verticalBreathing {
    0%   { background-position: 0% 0%;  }
	25%  { background-position: 40% 40%; }
	50%  { background-position: 80% 80%; }
	75%  { background-position: 20% 60%; }
    100% { background-position: 0% 0%;  }
}









/* Boring Stuff */
		</style>
	</head>
<body>
<?php include("header.php"); ?>
    <!-- Hero Section Begin -->
    <section class="hero-section">
    
	</video>
        <div class="bg-img">
            <div class="hero__item set-bg">
                <div class="container-fluid new-cnt-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                        <div class="hero__text hero-banner">
                            
                            <h1 class="ttl-heading animated-text fadeInDown" style="color:#000; font-weight: 700;">STREAMLINE<br> <span class="rotate-text">DEVOPS</span></h1>
                            <h2>STRATEGY.CONSULTING.SOLUTIONS.</h2>
                                <h4>Unleash Your Business's Potential to tackle your application and infrastructure challenges with Cutting-Edge Technology. Leverage Our Decades of Expertise and Certified Professionals to Optimise IT Investments for Maximum Returns.</h4>
                            <a href="#" class="bttn-read-m" style="font-weight: 400;">Reach Our Experts<i class="fa fa-arrow-right arrw-rgt"></i></a>
            <!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                        </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
                                <img src="../img/SNWN_hero.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
				
					<a href="#" class="Btn"><img class="mus" src="img/mouse accest.png"><i class="fa fa-arrow-down chev-dn"></i>
				</a>
					
            </div>
        </div>
    </section>
    <section class="slidersection" style="position:relative;">
    <div class="wrapper">
          
          
            <canvas id="bg-animation"></canvas>

            <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js?ver=1.9.1'></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>

            <script type='text/javascript' src='resources/scripts/bg-animation.js'></script>

        </div>
    <div class="container">
      <div class="slide-container">
        <input type="radio" name="slider" id="home" />
        <input type="radio" name="slider" id="features" />
        <input type="radio" name="slider" id="blogs" />
        <input type="radio" name="slider" id="about" />
        <!--<input type="radio" name="slider" id="contact" />-->
        <div class="tab-section">
          <div class="home-tab slide-tab">
            <h5>INNOVATE BEYOND. <strong>DELIVER MORE</strong></h5>
            <p>Established in 2017, SNWN stands as your trusted partner for managed IT services, enhancing business productivity with profound expertise. We excel in delivering solutions for complex IT projects and integrating new technologies in a rapidly evolving environment. Our team of certified DevOps engineers is dedicated to providing the best possible services, partnering with clients to achieve superior business outcomes together.</p>
          </div>
          <div class="features-tab slide-tab">
          <h5>MILESTONE OF <strong>UNTIEDING GROWTH</strong></h5>
    <div class="container-timel">
  <ul class="timeline">
    <li class="timeline__item" data-id="0">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label"><h3>2010</h3>Started</b>
    </li>
    <li class="timeline__item" data-id="1">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label-1">ISO Certification<h3>2012</h3></b>
    </li>
    <li class="timeline__item" data-id="2">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label"><h3>2014</h3>Support For Datacenter and web Hosting Companies Worldwide</b>
    </li>
    <li class="timeline__item" data-id="3">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
       
      <b class="label-2"><ul><li>+25 Team Members</li><li>Cloud Certifications</li></ul><h3>2018</h3></b>
    </li>
    <li class="timeline__item" data-id="4">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label"><h3>2020</h3><ul><li>+50 Project Completed</li><li>4+ Cloud Partners</li></ul></b>
    </li>
    <li class="timeline__item" data-id="5">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label-3"><ul><li>+150k Website Managment</li><li>+20 Web Hosting Company Support</li><li>New Office with +150 Staff Capacity</li></ul><h3>2024</h3></b>
    </li>
  </ul>
</div>
          </div>
          <div class="blogs-tab slide-tab">
            <h5>Choose Us. <strong>Grow Together</strong></h5>
            <p>We at SNWN make your IT headaches a thing of the past. Our 15-minute response time SLA speaks volumes for itself about why you should choose us. When issues arise, we act immediately, ensuring minimal disruption. With our 24x7 server management and outsource support services, you get round-the-clock monitoring, top-notch security, and easy scalability, all tailored to your business. We keep things running smoothly and efficiently without the stress– because your success is our priority!</p>
          </div>
          <div class="about-tab slide-tab">
          <div class="programe">
          <div class="col-md-12">
        <h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OUR <strong>PARTNER</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h5>
</div>
<div class="container">
    
<div class="col-md-12">
<ol class="logos" style="width:100%;">
<li id="e8a71621-669d-4c43-b85b-539aad4b07ff">
<figure class="logo">
    <picture><img src="img\about-icn\dom.webp"  style="aspect-ratio: 1 / 1;" alt="C | Cloud Host World" title="C | Cloud Host World">
</picture>
</figure>
</li>
<li id="e14ca454-9520-467c-9ed2-97ea7fb361de">
<figure class="logo">
    <picture><img src="img\about-icn\snwn4uk.webp" style="aspect-ratio: 1 / 1;" alt="Golang | Cloud Host World" title="Golang | Cloud Host World"> 
</picture>
</figure></li><li id="b0abdd84-3294-4d60-95e4-23e8a542553d">
<figure class="logo">
<picture>
    <img src="img\about-icn\snwnbm.webp"  style="aspect-ratio: 1 / 1;" alt="HTML CSS | Cloud Host World" title="HTML CSS | Cloud Host World"></picture></figure></li>
    <li id="9b55ce01-5d9e-4950-84b2-5b72a3094a02">
        <figure class="logo">
            <picture><img src="img\about-icn\snwnchw.webp"  style="aspect-ratio: 1 / 1;" alt="Java | Cloud Host World" title="Java | Cloud Host World"></picture></figure>
        </li>
        <li id="ecf3c754-e6ef-4f8e-b6a4-3970b994b637">
            <figure class="logo"><picture><img src="img\about-icn\snwnfsdata.webp"  style="aspect-ratio: 1 / 1;" alt="JavaScript | Cloud Host World" title="JavaScript | Cloud Host World">
        </picture>
        </figure>
    </li>
    <li id="b6c5fbf7-6469-45ce-8f44-72ae16257477">
        <figure class="logo"><picture>
            <img src="img\about-icn\snwnhem.webp"  style="aspect-ratio: 1 / 1;" alt="Kotlin | Cloud Host World" title="Kotlin | Cloud Host World"></picture>
            </figure>
        </li>
        <li id="f389b11d-48b3-48f0-982d-6e08614492ee">
            <figure class="logo">
                <picture>
                    <img src="img\about-icn\snwninter.webp" style="aspect-ratio: 1 / 1;" alt="Lua | Cloud Host World" title="Lua | Cloud Host World"></picture></figure>
                </li>
                <!--<li id="98ad81d5-472a-48b5-8c2c-2ec0e69210b0">
                    <figure class="logo">
                        <picture>
                            <img src="img\chw-image\new-icon-social\Perl.png" alt="Adobe Illustrator" width="320" height="320" style="aspect-ratio: 1 / 1;"></picture></figure>
                        </li>-->
                        <li id="677fbbc2-9c9c-4ec4-9625-082d727bcf1c">
                            <figure class="logo">
                                <picture>
                                    <img src="img\about-icn\snwnISPH.webp" style="aspect-ratio: 1 / 1;" alt="SQL | Cloud Host World" title="SQL | Cloud Host World">
                                </picture>
                                </figure></li>
                                <li id="5877ec9e-f01d-40e4-b76c-9f95387e94e3">
                                    <figure class="logo">
                                        <picture>
                                            <img src="img\about-icn\snwnser.webp" alt="Adobe InDesign"  style="aspect-ratio: 1 / 1;" alt="Swift | Cloud Host World" title="Swift | Cloud Host World">
                                        </picture>
                                        
                                    </figure>
                                </li>
                                <li id="060425a8-8847-42d9-9cd4-751dcd875f2a">
                                    <figure class="logo">
                                        <picture>
                                        <img src="img\about-icn\snwnword.webp"  style="aspect-ratio: 1 / 1;" alt="TypeScript | Cloud Host World" title="TypeScript | Cloud Host World">
                                        </picture>
                                        
                                    </figure>
                                </li>
                               <!-- <li id="4ad79a0d-4fa1-4989-b8cb-07c101efc9dc">
                                    <figure class="logo">
                                        <picture
                                        >
                                        <img src="img\chw-image\new-icon-social\Objective-C.png" alt="Adobe InDesign" width="320" height="320" style="aspect-ratio: 1 / 1;"></picture>
                                        </figure>
                                    </li>-->
                                    
                           
                                        </ol>
</div>
</div>
</div>
          </div>
          <!--<div class="contact-tab slide-tab">
            <h5>Contact Us</h5>
            <a href="">+222 22 222 22 22</a>
          </div>-->
        </div>
        <nav class="slide-nav">
          <label for="home" class="home">About </label>
          <label for="features" class="features">History </label>
          <label for="blogs" class="blogs">Why Us </label>
          <label for="about" class="about">Partners </label>
          <!--<label for="contact" class="contact">Contact</label>-->
        </nav>
      </div>
    </div>
  </section>
 <!--  <div class="bgs-all"> -->
 <div class="dev-sec-ops-new">
 
 <section class="card bw" >
   <div class="card-content">
   <div class="container">
   <div class="row">
     <div class="col-md-6">
     <h2 class="dv-sec">DevSecOps</h2>
     </div>
     <div class="col-md-6">
     <p class="dv-sec">From Strategy to Production, SNWN unites your team for Seamless Success.</p>
     </div>
     </div>
<div class="bg-color-sec">
<div class="row">
   <div class="col-md-6">
     <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Plan</h6>
       <h3>STRATEGY.</h3>
       <p>In the planning phase, we prioritise defining project scope, identifying requirements, and setting objectives. Our collaboration with clients ensures alignment on goals, while effective planning sets clear timelines, allocates resources strategically, and comes up with robust risk management strategies. This groundwork is essential for seamless development and successful deployment.
       </p>
       <ul>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Define project boundaries clearly.</li>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Identify essential requirements precisely. </li>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Set measurable objectives.</li>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Establish precise timelines and resource allocation strategies.</li>
       </ul>
   </div>
   <div class="col-md-6">
       <img src="img/complex-delivery-img-3.png">
</div>
</div>

</div>
   </div>
 </section>
 <section class="card tokyo" >
   <div class="card-content">
   <div class="container">
       
       <div class="row">
           <div class="col-md-6">
     <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
     
     <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>-->
</div>
</div>
<div class="bg-color-sec-1">
     <div class="row">
   <div class="col-md-6">
   <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Development</h6>
       <h3>CODE.</h3>
       <p>During the development phase, we engage in writing, testing, and integrating code, adhering to coding standards and leveraging version control. Continuous integration and collaborative efforts are pivotal in maintaining code quality and consistency, aiming to deliver a functional, scalable product that fulfils all requirements.
       </p>
       <ul>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Writing and testing code to coding standards.</li>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Integrating code using version control. </li>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Implementing continuous integration for quality assurance.</li>
           <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Collaborating for consistent, scalable development.</li>
       </ul>
   </div>
   <div class="col-md-6">
   <img src="img/complex-delivery-img-3.png">
</div>
</div>
</div>
</div>
  </section>
  <section class="card mountains" >
    <div class="card-content">
    <div class="container" style=" ">
    
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>-->
</div>
</div>
<div class="bg-color-sec-2">
<div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> DevOps</h6>
        <h3>INTEGRATION.</h3>
        <p>DevOps merges development and operations teams to optimise software delivery. This phase prioritises automation, continuous deployment, and meticulous monitoring. Employing tools like CI/CD pipelines, infrastructure as code, and containerization enhances operational efficiency and reliability significantly.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Seamless merging of development and operations teams.</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Emphasis on automated processes for continuous deployment. </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Rigorous monitoring throughout the integration phase.</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> CI/CD pipelines for efficient workflow management.</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
</div>
<ul>
  <li></li>
</ul>
    </div>
  </section>
  <div class="card balloons" >
    <div class="card-content">
    <div class="container" style=" ">
    
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>--->
</div>
</div>
<div class="bg-color-sec-3">
<div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Architect</h6>
        <h3>Design.</h3>
        <p>In the design phase, the architect crafts scalable and secure systems, with a focus on performance and fault tolerance and develops blueprints that steer both development and deployment, ensuring solutions are future-proof. This phase requires profound technical expertise and business awareness to deliver a solid foundation for smooth development, deployment, and future changes.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Design scalable and secure system architectures.</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Focus on performance optimization and fault tolerance. </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Develop blueprints to guide development and deployment.</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Ensure solutions are future-proof with technical and business insights.</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
    </div>
</div>
</div>
</div>




  <section class="card balloons" >
    <div class="card-content">
    <div class="container" style=" ">
    
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>--->
</div>
</div>
<div class="bg-color-sec-3">
<div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Security</h6>
        <h3>Lifecycle.</h3>
        <p>We have integrated security at every stage of the development lifecycle. This includes performing threat modelling and vulnerability assessments, and implementing robust security controls. Continuous testing and code review minimise risks and ensure a secure product. Additionally, our skilled staff undergo regular training to stay updated on the latest security practices.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Integrated security throughout the development lifecycle.</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Detailed threat modelling and vulnerability assessments. </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Implement comprehensive security controls.</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Continuous test and review code to enhance security.</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
    </div>
</div>
</div>
  </section>
</div>

<style>.sub-title {
   color: gray;
   font-size: 1em;
   margin-top: 0;
}
 
.fade-sol { 
   display: block; 
   opacity: 0;
   transition: opacity .1s linear;
}
.fade-sol[data-scroll="in"] {
   opacity: 1;
   transition-duration: 1s;
}



</style>


<div class="fade-sol">
<section class="service-section">
    
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <h2>
                  <strong>Solutions</strong> that Transform</h2>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
                                <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
                                    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"></path>
                                    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"></path>
                                    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"></path>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>DevOps</h5>
                                <p>Our experts are uniquely qualified to assist you in automating the process.</p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"></path>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>AWS Consulting</h5>
                                <p>Propel your business ahead with efficient resource utilisation, security, and superior ROI.                                </p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"></path>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>Azure Consulting</h5>
                                <p>Our Azure consultants deliver managed services, ensuring optimal efficiency & satisfaction.
                                </p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
                                <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
                                    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"></path>
                                    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"></path>
                                    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"></path>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>Web Hosting Support</h5>
                                <p>As a web hosting support provider, we ensure secure hosting with 24/7 assistance.</p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <div class="freatures-project">
                                <h5>Google Cloud Consulting</h5>
                                <p>Our experts will create a Google Cloud strategy, ensuring a smooth transition to the cloud.</p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>      
                        </div>
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"></path>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>Application Management</h5>
                                <p>Leveraging the latest technology, we provide 24x7 application support services. </p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"></path>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>WooCommerce Development</h5>
                                <p>Our WooCommerce experts ensure that  your online store runs at peak performance.</p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project header-3">
								<div class="img">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <div class="freatures-project">
                                <h5>Helpdesk Support Services</h5>
                                <p>Our helpdesk support ensures seamless operations for any query or technical challenges.</p>
                                <a href="#" class="explore-more-bts">Expore More <i class="fa fa-arrow-right arrw-rgt"></i></a>
                                </div>
                            </div>      
                        </div>
                    </div>
                    <div class="btn-page">
                    <a href="#" class="bttn-read-page">Expore All Solution</a> 
                    </div>
                </div>
            </div>
        </div>
    </div>  
    
   
</section>
</div>

<div class="fade-sol">
<section class="excellence-section">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <h2><strong>Industries</strong> We Serve</h2>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <div class="img-industries">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <h5>Finance</h5>
                                <p>We partner with the finance industry, providing robust security and trust in every transaction.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <div class="img-industries">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <h5>Ecommerce</h5>
                                <p>We revolutionise eCommerce with a user-friendly interface for a superior experience.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <div class="img-industries">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <h5>Healthcare</h5>
                                <p>We integrate advanced technology to deliver personalised care and patient well-being.</p>
                                
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <div class="img-industries">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <h5>Automative</h5>
                                <p>Pioneering advancements in technology, we lead the way in automotive design,& safety,</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <div class="img-industries">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <h5>Government</h5>
                                <p>We enhance the efficiency of government sectors through technology-driven solutions.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <div class="img-industries">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
                                </g>
                                </svg>
                                </div>
                                <h5>Entertainment</h5>
                                <p>We redefine entertainment with unmatched speed, 99.9% uptime, and digital platforms.</p>
                               
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</section>
</div>

<div class="fade-sol">
<div class="ptnrs">
<div class="container">
    <div class="row">
        <div class="col-md-3">
<h2>Our Partner :</h2>
</div>
<div class="col-md-9">
   <section class="logos-slider slider">
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
</div>
</div>
</div>
</div>
</div>
<div class="fade-sol">

    
	
<div class="ready-to-take">
            <div class=" set-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="hero__text hero-banner">
                            
                                <h1 class="ttl-heading animated-text fadeInDown" style="color:#000; font-weight: 700;"> Ready To Initiate Your Cloud Journey Now?</span></h1>
                                
                                    <h3>We're here to help you get the IT support you need at a price you can afford.</h3>
                                <a href="#" class="bttn-read-m-retake">Connect With Our Experts <!--<i class="fa fa-arrow-right arrw-rgt"></i>--></a>
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="hero__img">
                                <img src="../img/snwncall.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
				
					
					
            </div>
        </div>
    
</div>
<section class="awardone">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="img-award-1">
                            <img src="img/award/Experience.png"> 
                            <div class="cnt">
                            <h5>COST-EFECTIVE</h5>
                            <p>Cut costs effectively for seamless integration of modern solutions into your infrastructure.</p>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="img-award">
                            <img src="img/award/Assurance.png">
                            <div class="cnt"> 
                            <h5>LATEST TECHNOLOGY</h5>
                            <p>Leveraging latest technology and optimization techniques to maximise server efficiency.</p>
                            
                            </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="img-award">
                            <img src="img/award/Satisfied.png"> 
                            <div class="cnt">
                            <h5>INDUSTRY EXPERTISE</h5>
                            <p>Offering specialised knowledge tailored to specific sectors, addressing unique requirements.</p>
                            </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="img-award">
                                <img src="img/award/Completed.png">
                                <div class="cnt">
                                <h5>SCALABILE</h5>
                                <p>Providing flexible and reliable solutions that seamlessly adjust to varying business demands,</p>
                                </div> 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<div class="case-studies">
<div class="container">
<div class="row">
	<div class="col-md-12">
		<h3>Case Studies</h3>
		<p>Witness how our customers have successfully transformed their infrastructure to multi-cloud infrastructure, gaining insights into their remarkable journeys of growth and innovation employing our managed IT services.
</p>
		<div class="row" style="border:1px solid #eeebeb; border-radius:8px;">
			<div class="col-md-4" style="padding: 0;">
				<div class="case-st-img" >
			<img src="img/snwng4.png">
					</div>
			</div>
			<div class="col-md-8" style="padding: 0;">
				<div class="global-leading">
			<h3>A bank, the cloud, and a paradigm shift</h3>
				<p>Leading global bank uses SNWN Terraform to
reduce risk and quickly deliver new services during
their shift to the cloud.</p>
				<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
					</div>
			</div>
		</div>
	
	</div>
	</div>
	</div>
</div>



<div class="bank">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/snwng1.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/snwng3.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
			
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/snwng2.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<a href="#" class="clr-1">Read our case studies <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>




<?php include("footer.php"); ?>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
  <!-- Include jQuery library -->
<script src="https://unpkg.com/scroll-out@2.2.3/dist/scroll-out.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script>

$(document).ready(function() {
    // Function to check if an element is in view
    function isInView(element) {
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    }

    // Check if the target section is in view
    $(window).on('scroll', function() {
        $('.fade-sol').each(function() {
            if (isInView(this)) {
                setTimeout(function() {
                    $('.project').addClass('color-fill');
                }, 1000); // 2-second delay
            }
        });
    });
});
</script>
<script>
$(document).ready(function() {
    $(window).scroll(function() {
        $("").each(function() {
            var position = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > position - windowHeight + 900) {
                $(this).find('.card-content').css('opacity', '1');
            } else {
                $(this).find('.card-content').css('opacity', '0.0001');
            }
        });
    });
});
</script>
<script>
  console.clear();

ScrollOut({
   targets: '.fade-sol', 
   //once: true,
   onShown: function(el) {
      if (!el.src) { 
         el.src = el.dataset.src;
      }
   }
})
</script>
<script>
  // Smooth scrolling for anchor links
$(document).ready(function() {
    $('a[href^="#"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if( target.length ) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    });
});
</script>
<script>
  (function() {

var width, height, canvas, ctx, points, target, animateHeader = true;
var colorRGB = 128;

// Main
initHeader();
initAnimation();
addListeners();

function initHeader() {
    width = window.innerWidth;
    height = window.innerHeight;
    target = {x: width/2, y: height/2};


    canvas = document.getElementById('bg-animation');
    canvas.width = width;
    canvas.height = height;
    ctx = canvas.getContext('2d');

    // create points
    points = [];
    for(var x = 0; x < width; x = x + width/20) {
        for(var y = 0; y < height; y = y + height/20) {
            var px = x + Math.random()*width/20;
            var py = y + Math.random()*height/20;
            var p = {x: px, originX: px, y: py, originY: py };
            points.push(p);
        }
    }

    // for each point find the 5 closest points
    for(var i = 0; i < points.length; i++) {
        var closest = [];
        var p1 = points[i];
        for(var j = 0; j < points.length; j++) {
            var p2 = points[j]
            if(!(p1 == p2)) {
                var placed = false;
                for(var k = 0; k < 5; k++) {
                    if(!placed) {
                        if(closest[k] == undefined) {
                            closest[k] = p2;
                            placed = true;
                        }
                    }
                }

                for(var k = 0; k < 5; k++) {
                    if(!placed) {
                        if(getDistance(p1, p2) < getDistance(p1, closest[k])) {
                            closest[k] = p2;
                            placed = true;
                        }
                    }
                }
            }
        }
        p1.closest = closest;
    }

    // assign a circle to each point
    for(var i in points) {
        var c = new Circle(points[i], 2+Math.random()*2, 'rgba(255,255,255,0.3)');
        points[i].circle = c;
    }
}

// Event handling
function addListeners() {
    if(!('ontouchstart' in window)) {
        window.addEventListener('mousemove', mouseMove);
    }
    window.addEventListener('scroll', scrollCheck);
    window.addEventListener('resize', resize);
}

function mouseMove(e) {
    var posx = posy = 0;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY - $(window).scrollTop();
        absolutePosy = e.pageY;
    }
    else if (e.clientX || e.clientY)    {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
   
    target.x = posx;
    target.y = posy;
    
    if($('body').hasClass('single-iaa_programs')){
        if(absolutePosy > 800){
          colorRGB = 128;
        } else {
          colorRGB = 255;
        }
     } else if($('body').hasClass('page-template-default')){
       if(absolutePosy > 600){
         colorRGB = 128;
       } else {
         colorRGB = 255;
       }
     }
}

function scrollCheck() {
    
}

function resize() {

    width = window.innerWidth;
    height = window.innerHeight;
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    
}

// animation
function initAnimation() {
    animate();
    for(var i in points) {
        shiftPoint(points[i]);
    }
}

function animate() {
    if(animateHeader) {
        ctx.clearRect(0,0,width,height);
        for(var i in points) {
            // detect points in range
            if(Math.abs(getDistance(target, points[i])) < 4000) {
                points[i].active = 0.3;
                points[i].circle.active = 0.6;
            } else if(Math.abs(getDistance(target, points[i])) < 20000) {
                points[i].active = 0.1;
                points[i].circle.active = 0.3;
            } else if(Math.abs(getDistance(target, points[i])) < 40000) {
                points[i].active = 0.02;
                points[i].circle.active = 0.1;
            } else {
                points[i].active = 0;
                points[i].circle.active = 0;
            }

            drawLines(points[i]);
            points[i].circle.draw();
        }
    }
    requestAnimationFrame(animate);
}

function shiftPoint(p) {
    TweenLite.to(p, 1+1*Math.random(), {x:p.originX-50+Math.random()*100,
        y: p.originY-50+Math.random()*100, ease:Circ.easeInOut,
        onComplete: function() {
            shiftPoint(p);
        }});
}

// Canvas manipulation
function drawLines(p) {
    if(!p.active) return;
    for(var i in p.closest) {
        ctx.beginPath();
        ctx.moveTo(p.x, p.y);
        ctx.lineTo(p.closest[i].x, p.closest[i].y);
        ctx.strokeStyle = 'rgba('+colorRGB+','+colorRGB+','+colorRGB+','+ p.active+')';
//            ctx.strokeStyle = 'rgba(156,217,249,'+ p.active+')';
        ctx.stroke();
    }
}

function Circle(pos,rad,color) {
    var _this = this;

    // constructor
    (function() {
        _this.pos = pos || null;
        _this.radius = rad || null;
        _this.color = color || null;
    })();

    this.draw = function() {
        if(!_this.active) return;
        ctx.beginPath();
        ctx.arc(_this.pos.x, _this.pos.y, _this.radius, 0, 2 * Math.PI, false);
        ctx.fillStyle = 'rgba('+colorRGB+','+colorRGB+','+colorRGB+','+ _this.active+')';
        ctx.fill();
    };
}

// Util
function getDistance(p1, p2) {
    return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
}

})();
</script>

<script>
  <script>
  $(document).ready(function() {
    var sections = $(".card");
    var windowHeight = $(window).height();

    function activateSection(index) {
      sections.removeClass("active");
      sections.eq(index).addClass("active");
    }

    $(window).on("scroll", function() {
      var scrollTop = $(this).scrollTop();
      
      sections.each(function(index) {
        var sectionOffset = $(this).offset().top;

        if (scrollTop >= sectionOffset - windowHeight / 2 && scrollTop < sectionOffset + windowHeight / 2) {
          activateSection(index);
        }
      });
    });

    activateSection(0); // Activate the first section on page load
  });
</script>
</script>
  <script>
   $(document).ready(function() {
  let radios = $('input[type="radio"][name="slider"]');
  let currentIndex = 0;
  let interval;

  function autoScroll() {
    radios.eq(currentIndex).prop('checked', true);
    currentIndex = (currentIndex + 1) % radios.length;
  }

  function startAutoScroll() {
    interval = setInterval(autoScroll, 3000);
  }

  function stopAutoScroll() {
    clearInterval(interval);
  }

  // Start auto-scroll initially
  startAutoScroll();

  // Stop auto-scroll on hover
  $('.slide-container').hover(stopAutoScroll, startAutoScroll);
});
  </script>
		
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  
    <script>
    $(document).ready(function(){

     $("#mutliforms").on('slid.bs.carousel', function(e){
       setTimeout(function(){ 
        $('.carousel-item.active ul li ').find(".active").removeClass('active');
        e.preventDefault()
        var carousel_item =  $(".carousel-item.active").data('value');
        console.log($(".carousel-item.active ul").find('.'+carousel_item));
        console.log(carousel_item);
        $(".carousel-item.active ul li").find('.'+carousel_item).addClass('active');
      }, 10);
     });
   });

 </script>
    
    <script>//logo slider

$('.logos-slider').slick({
slidesToShow: 4,
slidesToScroll: 1,
autoplay: true,
autoplaySpeed: 1500,
arrows: false,
dots: false,
pauseOnHover: false,
responsive: [{
breakpoint: 768,
settings: {
slidesToShow: 3
}
}, {
breakpoint: 520,
settings: {
slidesToShow: 2
}
}]
});</script>
    
    
    
    <script>
        class Timeline {
  constructor() {
    this.activeItem = 0;
    this.timelineItems = document.querySelectorAll('.timeline__item');
    this.timer = null;
    this.currentEl = this.timelineItems[this.activeItem];
    
    this.registerEvents();
    this.initialize();
  }
  
  initialize() {
    this.currentEl.classList.add('timeline__item--active');    
  }
  
  start() {
    this.timer = window.setInterval(this.manageSteps.bind(this), 5000);
  }
  
  registerEvents() {
    this.timelineItems.forEach((item) => item.addEventListener('click', this.selectStep.bind(this)));
  }
  
  setActive() {
    this.currentEl.classList.add('timeline__item--active');
  }
  
  removeActive() {
    this.currentEl.classList.remove('timeline__item--active');
  }
  
  isActive() {
    return this.currentEl.classList.contains('timeline__item--active');
  }
  
  manageSteps() {    
    if(this.isActive()) {
      this.removeActive();
      this.activeItem = this.activeItem < this.timelineItems.length - 1 ? this.activeItem + 1 : 0;
    }
    
    this.setActive();
  }
  
  selectStep(evt) {
    let idSelectItem = evt.target.getAttribute('data-id');
    this.timelineItems[this.activeItem].classList.remove('timeline__item--active');
    
    window.clearInterval(this.timer);
    this.activeItem = parseInt(idSelectItem);
    this.timelineItems[idSelectItem].classList.add('timeline__item--active');
    this.start();
  }
}

const TimelineComponent = new Timeline();
TimelineComponent.start();
    </script>
        

        <script type="text/javascript">
        $(document).ready(function() {
            var text = ['DEVOPS', 'CLOUD', 'DEVELOPMENT', 'SUPPORT']; //Text to be shown
            var index = 0; //Starting index for rotating text
            var changeText = setInterval(function() {
                if (index == text.length - 1)
                    index = 0;
                else
                    index++;
                $('.rotate-text').animate({
                    "opacity": "0",
                    "margin-left": "10px"
                }, 500, function() {
                    $('.rotate-text').text(text[index]);
                    $('.rotate-text').animate({
                        "opacity": "1",
                        "margin-left": "0"
                    }, 500);
                });

            }, 2000);
        });
    </script>
     <script>
        // Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});
    </script>
<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>