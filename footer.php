<footer class="footer block-dark mt-n15 mt-lg-n30">
    <div class="h-150px h-lg-225px bgi-no-repeat bgi-position-x-center bgi-position-y-top bgi-size-cover dark-top-curved-bg"></div>
    <div class="container mt-n15">
        <div class="row">
            <div class="col-6 col-lg">
                <div class="pb-10 pt-5">
                    <h5 class="text-muted mb-2 mb-lg-7 font-size-4">Hosting</h5>

                    <div class="d-flex flex-column font-size-3 font-weight-bold">
                                                                            <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Shared Hosting</a>
                                                        <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Reseller Hosting</a>
                                                        <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">WordPress Hosting</a>
                                                        <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">E-Commerce Hosting</a>
                                                        <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Email Hosting</a>
                                                                </div>
                </div>
            </div>
            <div class="col-6 col-lg">
                <div class="pb-10 pt-5">
                    <h5 class="text-muted mb-2 mb-lg-7 font-size-4">Server</h5>
                    <div class="d-flex flex-column font-size-3 font-weight-bold">
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Dedicated Server</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Cloud VPS Server</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">SEO VPS Hosting</a>
                                        </div>
                </div>
            </div>
            <div class="col-6 col-lg">
                <div class="pb-10 pt-5">
                    <h5 class="text-muted mb-2 mb-lg-7 font-size-4">Addons</h5>

                    <div class="d-flex flex-column font-size-3 font-weight-bold">
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">SSL Certificates</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Cloud Backup</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Special Products</a>
                                        </div>
                </div>
            </div>
            <div class="col-6 col-lg">
                <div class="pb-10 pt-5">
                    <h5 class="text-muted mb-2 mb-lg-7 font-size-4">Company</h5>

                    <div class="d-flex flex-column font-size-3 font-weight-bold">
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">About Us</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Contact Us</a>
                                        </div>
                </div>
            </div>
            <div class="col-6 col-lg">
                <div class="pb-10 pt-5">
                    <h5 class="text-muted mb-2 mb-lg-7 font-size-4">Blog</h5>
                    <div class="d-flex flex-column font-size-3 font-weight-bold">
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Privacy Policy</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Terms of Service</a>
                                                <a href="#" class="text-dark-60 text-hover-primary py-2 py-lg-3">Knowledgebase</a>
                                        </div>
                </div>
            </div>
            <div class="col-6 col-lg">
                <div class="pb-10 pt-5">
                    <div class="d-flex flex-column font-size-3 font-weight-bold">
                        <a href="./index.html"><img src="img/snwn-logo.png"></a>
                </div>
            </div>
        </div>
    </div>
		</div>
<!--<div class="container">
    <div class="row">
        <div class="col-md-5">
           
                
                    <h5 class="text-white mb-3 mt-4">We Accepted</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#">
                            <div class="bg-l">
                                <img src="img/visa.png" class="img-fluid">
                            </div>
                        </a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">
                            <div class="bg-l">
                                <img src="img/mastercard.png" class="img-fluid">
                            </div>
                        </a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">
                            <div class="bg-l">
                                <img src="img/payoneer.png" class="img-fluid">
                            </div>
                        </a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">
                            <div class="bg-l">
                                <img src="img/paypal.png" class="img-fluid">
                            </div>
                            </a>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-2">&nbsp;</div>
        <div class="col-md-5">
                <h5 class="text-white mb-3 mt-4">Follow Us On</h5>
                <div class="row">
                <div class="col-md-2">
                    <a href="#">
                        <i  class="fa fa-brands fa-facebook bg-s"></i>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="#">
                        <i class="fa fa-brands fa-twitter fa-twitter bg-s"></i>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="#">
                        <i class="fa fa-brands fa-instagram bg-s"></i>
                    </a>
                </div>
                <div class="col-md-2">
                    <a href="#">
                        <i class="fa fa-brands fa-linkedin bg-s"></i>
                    </a>
                </div>
            </div>
    </div>

</div>-->
    

    <div class="container py-5 py-lg-10">
        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between">
            <div class="d-flex align-items-center mr-md-2 order-2 order-md-1">

                

                <p class="font-size-2 font-weight-bold text-dark-60 pt-1" style="text-align: center;" href="https://developer6669.com">
                    <a href="#"> 
                    </a> Copyright Ⓒ 2024 SNWN PVT. LTD. All rights reserved
                </p>

            </div>
        </div>
    </div>
</footer>