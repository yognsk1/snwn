<!DOCTYPE html>
<html lang="zxx">
	<head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="icon" href="img/fevicon.png" sizes="192x192">
		<style>
      .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);
}
			.appllication-service ul li {
  display: inline-block;
  width: 24.8%;
  text-align: center;
  margin-left: 20px;
}
.appllication-service ul li {
  display: inline-block;
  width: 24.8%;
  text-align: center;
  margin-left: 0px;
}
.bdr-ld {
  border: 1px solid #dee2e6;
  border-radius: 0 0px 15px 15px;
}
		</style>
	</head>
<body>
	<?php include("header.php"); ?>
    <!-- Header End -->
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="" style=" background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);">
            <div class="hero__item set-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1 class="ttl-heading" style="color:#fff; font-weight: 900; font-size: 56px;">Dedicate Support</h1>
                                    <!--<h4 class="service-cont">Service And Solutions</h4>-->
								<p class="automate">No one takes customer care and support more serious than SNWN Pvt. Ltd., Our around- the clock dedicated support technicians take full ownership of your issues, tackling the root cause and often resolving it within 30 minutes or less.</p>
								
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
							<div class="row">
					<div class="col-md-4">
					<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Reach Our Experts</a>			
					</div>
					<div class="col-md-4">
								<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Our Benefits</a>
				</div>
        <div class="col-md-4">
								&nbsp;
				</div>
				</div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
								<img src="img/autoscaling-hero-logo.png" >
                                
                            </div>
                        </div>
                    </div>
                </div>
				
					<a href="#" class="Btn"><img class="mus" src="img/mouse-accest-1.png"><i class="fa fa-arrow-down chev-dn"></i>
				
				</a>
								
            </div>
        </div>
    </section>
		
		<div class="support-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Expert Eyes on Your Servers</h2>
                            <p>Ensure seamless server operation and exceptional customer experiences with our expert team integrating seamlessly as a true extension of your team.</p>
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
								  <div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
  <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"></path>
    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"></path>
    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"></path>
  </g>
</svg>
									  </div>

                                <div class="freatures-project">
                                <h5>Monitor (Enhanced Pro-activity)</h5>
                                <p>We leverage real-time proactive monitoring services to ensure prompt identification, whenever any potential server issue arises our engineers quickly act swiftly to restore service without delay.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
								 <div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
  <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"></path>
    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"></path>
  </g>
</svg>
								 </div>
                                <div class="freatures-project">
                                <h5>Support (Seamless Resolution)</h5>
                                <p>Our admins seamlessly blend into your support system, ensuring efficient communication with your customers to offer speedy resolution their issues enhancing overall satisfaction.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
								 <div class="img">
								 <svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
  <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
  </g>
</svg>

  

								 </div>
                                <div class="freatures-project">
                                <h5>Audit (Minimized Disruptions)</h5>
                                <p>Regular server audits proactively identify and address performance, security, and other potential concerns, significantly reducing the chances of downtime and customer disruptions.</p>
                                
                                </div>
                             </div>      
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>







            <div class="make-difference">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>How We Make a Difference?</h2>
                            <p>At SNWN Pvt. Ltd. we differentiate ourselves by delivering unmatched support services tailored to exceed your expectations across various cloud platforms.</p>
							
                            <div class="row">
                            <div class="col-md-3">
                              <div class="project">
								  <div class="img">
                                <img src="img/cloud-infrastructure-strategy-design.png">
									  </div>

                                <div class="freatures-project">
                                <h5>Customized Assistance</h5>
                                <p>Whether you need support during business hours or have specific requirements outside office hours, our 24/7 dedicated technical support services are tailored to match your needs.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/continuos-integration.png">
 
								 </div>
                                <div class="freatures-project">
                                <h5>Server Monitoring & Quick Response</h5>
                                <p>Our experienced technicians excel in server monitoring, and rapid response, swiftly resolving service disruptions to maintain seamless experiences before it impacts your customers.</p>
                                
                                </div>
                             </div>  
                            </div>
								
								<div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/cloud-infrastructure-automation.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Server Setup and Provisioning</h5>
                                <p>Our experienced team provides custom-configured servers for seamless setup, along with regular tuning and updates for optimal performance and security.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
								 <img src="img/automated-monitoring.png">
								 </div>
                                <div class="freatures-project">
                                <h5>On-Demand Server Management</h5>
                                <p>Need help in critical hours with software installation or website troubleshooting? Our expert team provides smooth operations and quick issue resolution, giving you peace of mind.</p>
                                
                                </div>
								 
								
                             </div> 
								</div>
								 <div class="col-md-3">
                              <div class="project">
								  <div class="img">
                                <img src="img/docker.png">
									  </div>

                                <div class="freatures-project">
                                <h5>Cost-effective Management
</h5>
                                <p>Our certified experts help cut costs and manage server, software, and bandwidth expenses. If something isn't right for your business, we'll find better options with affordable charges.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/kubernetes.png">
 
								 </div>
                                <div class="freatures-project">
                                <h5>Server Optimization</h5>
                                <p>We optimize your operations by eliminating unnecessary software and resources, upgrading to superior configurations to ensure maximum performance and robust security.</p>
                                
                                </div>
                             </div>  
                            </div>
								
								<div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/spring-boot.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Operating System Updates</h5>
                                <p>We at SNWN Pvt. Ltd. employs zero-downtime upgrade techniques to deliver OS updates seamlessly, allowing you to focus on your business while we update and optimize your servers.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
								 <img src="img/jenkins.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Server Migration</h5>
                                <p>Whether moving to the cloud or migrating on-premises servers, we handle it all with near zero downtime, from resources allocation to data migration web have got you covered.</p>
                                
                                </div>
								 
								
                             </div> 
								</div>
							</div>
						</div>
					</div>
	</div>
	</div>

  <div class="future-it" style="">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    
				<div class="col-md-6">
					<div class="">
						
						<h3>Experience the Future of IT Management</h3>
						<p>Experience dedicated support tailored to your company, with a 24/7 customer support team that you can customize to meet your needs. Whether it's choosing Linux administration experts or setting rapid response times for ticket resolutions, we empower you to shape your support experience.</p>
            <p>Take charge of your customization needs – Drop us a line and consider it done!</p>
<a href="#" class="clr-15">Chat With Us <i class="fa fa-arrow-right mg-right"></i></a>
					
					</div>
				</div>
				<div class="col-md-6">
          <img src="img/9-2-graphic-design-download-png.png" class="img-fluid">
					
</div>
						
					
					</div>
				</div>
				
		</div>
		
	</div>
</div>
	
	 


<div class="compare-table">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Benefits</h2>
                            <h3>Find Your Perfect Fit - Shared Support vs. Dedicated Support</h3>
                            <div class="row">
                              <div class="col-md-12">
                              <table class="table table-bordered">
    <thead>
      <tr>
        <th><p>Basic</p></th>
        <th><p>Shared Support</p></th>
        <th><p>Dedicated Support</p></th>
        
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><p>Support Availability</p></td>
        <td><p>A Company handles multiple clients, thus the resources are shared across the client base.<p></td>
        <td><p>Dedicated Support- Every client is can take benefit of a dedicated support team available around the clock.</p></td>
      </tr>
      <tr>
        <td><p>Response Time</p></td>
        <td><p>Shared Support- Response times will vary across the factors such as the selected plan, workload, priority, and resource availability.</p> </td>
        <td><p>Dedicated Support-Experience lightning-fast response times through our dedicated support team.</p></td>
      </tr>
      <tr>
        <td><p>Customization</p></td>
        <td><p>Shared Support- Customization options might be limited and are dependent on available resources.</p></td>
        <td><p>Dedicated Support-Tailor-made support services are finely tuned to match your specific needs.</p></td>
      </tr>
      <tr>
        <td><p>Cost</p></td>
        <td><p>Shared Support- Shared plans are economical.</p></td>
        <td><p>Dedicated Support-Slightly higher in cost due to the provision of 24x7 dedicated support.</p></td>
      </tr>
      <tr>
        <td><p>Prioritization</p></td>
        <td><p>Shared Support- Issues are categorized based on priority and resource availability.</p></td>
        <td><p>Dedicated Support- All issues receive top priority and are resolved within the committed timeframe.</p></td>
      </tr>
      
    </tbody>
  </table>
                              </div>
                                </div>
                            
                                </div>
                        </div>
                    </div>
                </div>


                <div class="support-section new-top-mg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
								  <div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
  <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"></path>
    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"></path>
    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"></path>
  </g>
</svg>
									  </div>

                                <div class="freatures-project">
                                <h5>Certified Excellence in Support</h5>
                                <p>At the heart of our commitment to excellence lies our certified support quality. We allocate 5% of our revenue to an ISO-certified QA system, rigorously analysing multiple tickets monthly to elevate service standards continually.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
								 <div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
  <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"></path>
    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"></path>
  </g>
</svg>
								 </div>
                                <div class="freatures-project">
                                <h5>Streamlined Support Operations</h5>
                                <p>With over decade of expertise in server infrastructure management and supporting clients with distinct industries, we ensure optimal performance. Our services include OS patching, upgrades, migrations, virtualization, and much more.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
								 <div class="img">
								 <svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
  <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"></path>
    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"></path>
  </g>
</svg>

  

								 </div>
                                <div class="freatures-project">
                                <h5>Guaranteed Information Security</h5>
                                <p>We prioritize the security of your information. That’s why we stick to the most demanding security standard in the hosting industry, ISO certification, to safeguard your customer data, server logins, service agreements, and more.</p>
                                
                                </div>
                             </div>      
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="snwn-benefits-4">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>DIFFERENCE</h2>
                            <p>What Makes Us Unique?</p>
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
                                  <div class="img">
                                <img src="img/automatic.png">
                                      </div>

                                <div class="freatures-project">
                                <h5>100% White Label</h5>
                                <p>Our technicians operate through your support desk and use your company email signatures, making customers feel like they're interacting directly with your in-house team.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/cost-saving.png">
 
                                 </div>
                                <div class="freatures-project">
                                <h5>Multi-Channel Help</h5>
                                <p>Our 24/7 dedicated support specialists assist your customers via Email, Helpdesk, Live Chat, and social channels, ensuring seamless and comprehensive customer service around the clock.</p>
                                
                                </div>
                             </div>  
                            </div>
                                
                                <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Server Maintenance </h5>
                                <p>Our dedicated technical support specialists keep your servers optimized and secure with vulnerability patches, timely updates, performance optimizations, and reliable backups.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                 <img src="img/fault-tolerant.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Cloud and Virtual Server Management</h5>
                                <p>Mastering Cloud and Hypervisor tasks, from resource optimization to flawless VM migrations, leveraging our extensive expertise to propel your business forward with precision and reliability.</p>
                                </div>
                             </div> 
                        </div>


                        <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/easy-to-audit.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Billing and Pre-Sales Support</h5>
                                <p>Our dedicated support specialists manage everything from account renewals, new account setups, and pre-sales inquiries through live chat, to providing tailored support plans that are cost-effective.</p>
                                
                                </div>
                             </div>  
                            </div>


                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Reputation Management Services</h5>
                                <p>We keep a close eye on domain and IP blacklists to safeguard your servers from abuse reports. Incase an IP is listed, we swiftly delist it and secure your server to prevent future incidents.</p>
                                
                                </div>
                             </div>  
                            </div>
                                </div>
                            
                                </div>
                        </div>
                    </div>
                </div>



			










				
					
					
					


	
	
	
	
	

<div class="faq-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Want to know more?</h3>
				<h2>Frequently Asked Questions</h2>
				<div class="row">
				<div class="col-md-6">
				<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd3">
    <label for="rd3" class="tab__label">Why do you need dedicated support?</label>
    <div class="tab__content">
      <p>Dedicated support ensures your business gets specialized, focused assistance that's tailored exactly to your needs. This means quicker solutions to your challenges and happier customers. Having a team solely dedicated to you helps streamline operations, improve service quality, and build stronger customer relationships. It's all about boosting your business growth and ensuring your success.</p>
    </div>
  </div>
	<div class="tab">
    <input type="radio" name="accordion-2" id="rd4">
    <label for="rd4" class="tab__label">Do you provide 24x7 support in real?</label>
    <div class="tab__content">
      <p>Ofcourse! We offer dedicated 24/7 support to serve customers worldwide. Our live chat support guarantees real-time resolution of queries and issues, no matter the time zone. This round-the-clock availability not only boosts overall customer satisfaction but also demonstrates our commitment to exceptional service, ensuring your customers feel valued and supported at all times.</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd5">
    <label for="rd5" class="tab__label">Why are the perks of choosing SNWN Pvt. Ltd. dedicated support services?</label>
    <div class="tab__content">
      <p>Our dedicated support not only raises customer expectations but also significantly enhances their overall experience. It ensures swift and effective problem resolution, creating a more satisfying interaction. This in turn, drives increased sales, making it a win-win for both your business and your customers.</p>
    </div>
  </div>
	<div class="tab">
    <input type="radio" name="accordion-2" id="rd6">
    <label for="rd6" class="tab__label">Why should I offer live support to my customers?</label>
    <div class="tab__content">
      <p>Live support builds strong bonds with customers by providing immediate assistance and personalized interaction. This instant, tailored help not only resolves issues quickly but also cultivates loyalty and encourages repeat business. By showing customers they are valued and understood, live support drives long-term success.</p>
    </div>
  </div>
  
</section>
					</div>
<div class="col-md-6">
<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd1">
    <label for="rd1" class="tab__label">Can I get help with immediate attention to customer-specific issues through dedicated customer support?</label>
    <div class="tab__content">
      <p>Absolutely. Dedicated customer support is structured to deliver prompt assistance for various customer issues. Whether it's troubleshooting or resolving specific problems, our shared and dedicated 24/7 support team is equipped to address them swiftly.</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd2">
    <label for="rd2" class="tab__label">How does live support differ from dedicated customer support?</label>
    <div class="tab__content">
      <p>Live support delivers real-time customer service through channels such as live chat or phone support, ensuring immediate responses to customer queries. On the other hand, dedicated customer support goes a step further by assigning a specific agent or team to focus exclusively on your business or a select group of customers. This dedicated approach provides ongoing, personalized assistance, nurturing deeper relationships and a higher level of service tailored precisely to your needs.</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd7">
    <label for="rd7" class="tab__label">Why should I choose SNWN Pvt. Ltd. for dedicated 24/7 support?</label>
    <div class="tab__content">
      <p>Choosing SNWN Pvt. Ltd. means access to both live and dedicated 24/7 support, ensuring customers receive immediate help for any technical queries or issues. Our outsourced help desk support services are designed to swiftly resolve customer issues and enhance overall customer support satisfaction.</p>
    </div>
  </div>
	
  
</section>
	</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>

	
	<div class="know-more">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/smart-discussing-meeting-report-phone.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/engineers-looking-bridge-construction.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
			
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<a href="#" class="clr-1">Read our Blog <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>











<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
		  <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
		  <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>

      <div class="col-md-7">
		  <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
			  </div>
      </div>
    </div>

    

  </div>
</section>

<?php include("footer.php"); ?>

			
	
	
		
	
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
  <script>
     /*
 * ES2015 accessible tabs panel system, using ARIA
 * Website: https://van11y.net/accessible-tab-panel/
 * License MIT: https://github.com/nico3333fr/van11y-accessible-tab-panel-aria/blob/master/LICENSE
 */
"use strict";

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

(function (doc) {
  "use strict";

  var TABS_JS = "js-tabs";
  var TABS_JS_LIST = "js-tablist";
  var TABS_JS_LISTITEM = "js-tablist__item";
  var TABS_JS_LISTLINK = "js-tablist__link";
  var TABS_JS_CONTENT = "js-tabcontent";
  var TABS_JS_LINK_TO_TAB = "js-link-to-tab";

  var TABS_DATA_PREFIX_CLASS = "data-tabs-prefix-class";
  var TABS_DATA_HX = "data-hx";
  var TABS_DATA_GENERATED_HX_CLASS = "data-tabs-generated-hx-class";
  var TABS_DATA_EXISTING_HX = "data-existing-hx";

  var TABS_DATA_SELECTED_TAB = "data-selected";

  var TABS_PREFIX_IDS = "label_";

  var TABS_STYLE = "tabs";
  var TABS_LIST_STYLE = "tabs__nav";
  var TABS_LISTITEM_STYLE = "tabs__nav-list-item";
  var TABS_LINK_STYLE = "tabs__nav-list-link";
  var TABS_CONTENT_STYLE = "tabs__panel";

  var TABS_HX_DEFAULT_CLASS = "hide";

  var TABS_ROLE_TABLIST = "tablist";
  var TABS_ROLE_TAB = "tab";
  var TABS_ROLE_TABPANEL = "tabpanel";
  var TABS_ROLE_PRESENTATION = "presentation";

  var ATTR_ROLE = "role";
  var ATTR_LABELLEDBY = "aria-labelledby";
  var ATTR_HIDDEN = "aria-hidden";
  var ATTR_CONTROLS = "aria-controls";
  var ATTR_SELECTED = "aria-selected";

  var DELAY_HASH_UPDATE = 1000;

  var hash = window.location.hash.replace("#", "");

  const IS_OPENED_CLASS = "is-active";

  var findById = function findById(id) {
    return doc.getElementById(id);
  };

  var addClass = function addClass(el, className) {
    if (el.classList) {
      el.classList.add(className); // IE 10+
    } else {
      el.className += " " + className; // IE 8+
    }
  };

  /*const removeClass = (el, className) => {
           if (el.classList) {
             el.classList.remove(className); // IE 10+
           }
           else {
                el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' '); // IE 8+
                }
           }*/

  var hasClass = function hasClass(el, className) {
    if (el.classList) {
      return el.classList.contains(className); // IE 10+
    } else {
      return new RegExp("(^| )" + className + "( |$)", "gi").test(el.className); // IE 8+ ?
    }
  };

  var setAttributes = function setAttributes(node, attrs) {
    Object.keys(attrs).forEach(function (attribute) {
      node.setAttribute(attribute, attrs[attribute]);
    });
  };
  var unSelectLinks = function unSelectLinks(elts) {
    elts.forEach(function (link_node) {
      var _setAttributes;

      setAttributes(
        link_node,
        ((_setAttributes = {}),
        _defineProperty(_setAttributes, ATTR_SELECTED, "false"),
        _defineProperty(_setAttributes, "tabindex", "-1"),
        _setAttributes)
      );
    });
  };
  var unSelectContents = function unSelectContents(elts) {
    elts.forEach(function (content_node) {
      content_node.setAttribute(ATTR_HIDDEN, true);
    });
  };

  var selectLink = function selectLink(el) {
    var _setAttributes2;

    var destination = findById(el.getAttribute(ATTR_CONTROLS));
    setAttributes(
      el,
      ((_setAttributes2 = {}),
      _defineProperty(_setAttributes2, ATTR_SELECTED, "true"),
      _defineProperty(_setAttributes2, "tabindex", "0"),
      _setAttributes2)
    );
    destination.removeAttribute(ATTR_HIDDEN);
    setTimeout(function () {
      el.focus();
    }, 0);
    setTimeout(function () {
      history.pushState(
        null,
        null,
        location.pathname +
          location.search +
          "#" +
          el.getAttribute(ATTR_CONTROLS)
      );
    }, DELAY_HASH_UPDATE);
  };

  var selectLinkInList = function selectLinkInList(
    itemsList,
    linkList,
    contentList,
    param
  ) {
    var indice_trouve = undefined;

    itemsList.forEach(function (itemNode, index) {
      if (
        itemNode
          .querySelector("." + TABS_JS_LISTLINK)
          .getAttribute(ATTR_SELECTED) === "true"
      ) {
        indice_trouve = index;
      }
    });
    unSelectLinks(linkList);
    unSelectContents(contentList);
    if (param === "next") {
      selectLink(linkList[indice_trouve + 1]);
      setTimeout(function () {
        linkList[indice_trouve + 1].focus();
      }, 0);
    }
    if (param === "prev") {
      selectLink(linkList[indice_trouve - 1]);
      setTimeout(function () {
        linkList[indice_trouve - 1].focus();
      }, 0);
    }
  };

  /* gets an element el, search if it is child of parent class, returns id of the parent */
  var searchParent = function searchParent(el, parentClass) {
    var found = false;
    var parentElement = el.parentNode;
    while (parentElement && found === false) {
      if (hasClass(parentElement, parentClass) === true) {
        found = true;
      } else {
        parentElement = parentElement.parentNode;
      }
    }
    if (found === true) {
      return parentElement.getAttribute("id");
    } else {
      return "";
    }
  };

  /** Find all tabs inside a container
   * @param  {Node} node Default document
   * @return {Array}
   */
  var $listTabs = function $listTabs() {
    var node =
      arguments.length <= 0 || arguments[0] === undefined ? doc : arguments[0];
    return [].slice.call(node.querySelectorAll("." + TABS_JS));
  };

  /**
   * Build tooltips for a container
   * @param  {Node} node
   */
  var attach = function attach(node) {
    $listTabs(node).forEach(function (tabs_node) {
      var iLisible = Math.random().toString(32).slice(2, 12);
      var prefixClassName =
        tabs_node.hasAttribute(TABS_DATA_PREFIX_CLASS) === true
          ? tabs_node.getAttribute(TABS_DATA_PREFIX_CLASS) + "-"
          : "";
      var hx =
        tabs_node.hasAttribute(TABS_DATA_HX) === true
          ? tabs_node.getAttribute(TABS_DATA_HX)
          : "";
      var hxGeneratedClass =
        tabs_node.hasAttribute(TABS_DATA_GENERATED_HX_CLASS) === true
          ? tabs_node.getAttribute(TABS_DATA_GENERATED_HX_CLASS)
          : TABS_HX_DEFAULT_CLASS;
      var existingHx =
        tabs_node.hasAttribute(TABS_DATA_EXISTING_HX) === true
          ? tabs_node.getAttribute(TABS_DATA_EXISTING_HX)
          : "";
      var $tabList = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_LIST)
      );
      var $tabListItems = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_LISTITEM)
      );
      var $tabListLinks = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_LISTLINK)
      );
      var $tabListPanels = [].slice.call(
        tabs_node.querySelectorAll("." + TABS_JS_CONTENT)
      );
      var noTabSelected = true;

      // container
      addClass(tabs_node, prefixClassName + TABS_STYLE);
      tabs_node.setAttribute("id", TABS_STYLE + iLisible);

      // ul
      $tabList.forEach(function (tabList) {
        var _setAttributes3;

        addClass(tabList, prefixClassName + TABS_LIST_STYLE);
        setAttributes(
          tabList,
          ((_setAttributes3 = {}),
          _defineProperty(_setAttributes3, ATTR_ROLE, TABS_ROLE_TABLIST),
          _defineProperty(_setAttributes3, "id", TABS_LIST_STYLE + iLisible),
          _setAttributes3)
        );
      });
      // li
      $tabListItems.forEach(function (tabListItem, index) {
        var _setAttributes4;

        addClass(tabListItem, prefixClassName + TABS_LISTITEM_STYLE);
        setAttributes(
          tabListItem,
          ((_setAttributes4 = {}),
          _defineProperty(_setAttributes4, ATTR_ROLE, TABS_ROLE_PRESENTATION),
          _defineProperty(
            _setAttributes4,
            "id",
            TABS_LISTITEM_STYLE + iLisible + "-" + (index + 1)
          ),
          _setAttributes4)
        );
      });
      // a
      $tabListLinks.forEach(function (tabListLink) {
        var _setAttributes5, _setAttributes6;

        var idHref = tabListLink.getAttribute("href").replace("#", "");
        var panelControlled = findById(idHref);
        var linkText = tabListLink.innerText;
        var panelSelected =
          tabListLink.hasAttribute(TABS_DATA_SELECTED_TAB) === true;

        addClass(tabListLink, prefixClassName + TABS_LINK_STYLE);
        setAttributes(
          tabListLink,
          ((_setAttributes5 = {
            id: TABS_PREFIX_IDS + idHref
          }),
          _defineProperty(_setAttributes5, ATTR_ROLE, TABS_ROLE_TAB),
          _defineProperty(_setAttributes5, ATTR_CONTROLS, idHref),
          _defineProperty(_setAttributes5, "tabindex", "-1"),
          _defineProperty(_setAttributes5, ATTR_SELECTED, "false"),
          _setAttributes5)
        );

        // panel controlled
        setAttributes(
          panelControlled,
          ((_setAttributes6 = {}),
          _defineProperty(_setAttributes6, ATTR_HIDDEN, "true"),
          _defineProperty(_setAttributes6, ATTR_ROLE, TABS_ROLE_TABPANEL),
          _defineProperty(
            _setAttributes6,
            ATTR_LABELLEDBY,
            TABS_PREFIX_IDS + idHref
          ),
          _setAttributes6)
        );
        addClass(panelControlled, prefixClassName + TABS_CONTENT_STYLE);

        // if already selected
        if (panelSelected && noTabSelected) {
          noTabSelected = false;
          setAttributes(
            tabListLink,
            _defineProperty(
              {
                tabindex: "0"
              },
              ATTR_SELECTED,
              "true"
            )
          );
          setAttributes(
            panelControlled,
            _defineProperty({}, ATTR_HIDDEN, "false")
          );
        }

        // hx
        if (hx !== "") {
          var hx_node = document.createElement(hx);
          hx_node.setAttribute("class", hxGeneratedClass);
          hx_node.setAttribute("tabindex", "0");
          hx_node.innerHTML = linkText;
          panelControlled.insertBefore(hx_node, panelControlled.firstChild);
        }
        // existingHx

        if (existingHx !== "") {
          var $hx_existing = [].slice.call(
            panelControlled.querySelectorAll(existingHx + ":first-child")
          );
          $hx_existing.forEach(function (hx_item) {
            hx_item.setAttribute("tabindex", "0");
          });
        }

        tabListLink.removeAttribute("href");
      });

      if (hash !== "") {
        var nodeHashed = findById(hash);
        if (nodeHashed !== null) {
          // just in case of an dumb error
          // search if hash is current tabs_node
          if (tabs_node.querySelector("#" + hash) !== null) {
            // search if hash is ON tabs
            if (hasClass(nodeHashed, TABS_JS_CONTENT) === true) {
              // unselect others
              unSelectLinks($tabListLinks);
              unSelectContents($tabListPanels);
              // select this one
              nodeHashed.removeAttribute(ATTR_HIDDEN);
              var linkHashed = findById(TABS_PREFIX_IDS + hash);
              setAttributes(
                linkHashed,
                _defineProperty(
                  {
                    tabindex: "0"
                  },
                  ATTR_SELECTED,
                  "true"
                )
              );
              noTabSelected = false;
            } else {
              // search if hash is IN tabs
              var panelParentId = searchParent(nodeHashed, TABS_JS_CONTENT);
              if (panelParentId !== "") {
                // unselect others
                unSelectLinks($tabListLinks);
                unSelectContents($tabListPanels);
                // select this one
                var panelParent = findById(panelParentId);
                panelParent.removeAttribute(ATTR_HIDDEN);
                var linkParent = findById(TABS_PREFIX_IDS + panelParentId);
                setAttributes(
                  linkParent,
                  _defineProperty(
                    {
                      tabindex: "0"
                    },
                    ATTR_SELECTED,
                    "true"
                  )
                );
                noTabSelected = false;
              }
            }
          }
        }
      }

      // if no selected => select first
      if (noTabSelected === true) {
        setAttributes(
          $tabListLinks[0],
          _defineProperty(
            {
              tabindex: "0"
            },
            ATTR_SELECTED,
            "true"
          )
        );
        var panelFirst = findById($tabListLinks[0].getAttribute(ATTR_CONTROLS));
        panelFirst.removeAttribute(ATTR_HIDDEN);
      }
    });
  };

  /* listeners */
  ["click", "keydown"].forEach(function (eventName) {
    //let isCtrl = false;

    doc.body.addEventListener(
      eventName,
      function (e) {
        // click on a tab link or on something IN a tab link
        var parentLink = searchParent(e.target, TABS_JS_LISTLINK);
        if (
          (hasClass(e.target, TABS_JS_LISTLINK) === true ||
            parentLink !== "") &&
          eventName === "click"
        ) {
          var linkSelected =
            hasClass(e.target, TABS_JS_LISTLINK) === true
              ? e.target
              : findById(parentLink);
          var parentTabId = searchParent(e.target, TABS_JS);
          var parentTab = findById(parentTabId);
          //let $parentListItems = [].slice.call(parentTab.querySelectorAll('.' + TABS_JS_LISTITEM));
          var $parentListLinks = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
          );
          var $parentListContents = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_CONTENT)
          );

          // aria selected false on all links
          unSelectLinks($parentListLinks);
          // add aria-hidden on all tabs contents
          unSelectContents($parentListContents);
          // add aria selected on selected link + show linked panel
          selectLink(linkSelected);

          e.preventDefault();
        }

        // Key down on tabs
        if (
          (hasClass(e.target, TABS_JS_LISTLINK) === true ||
            parentLink !== "") &&
          eventName === "keydown"
        ) {
          //let linkSelected = hasClass( e.target, TABS_JS_LISTLINK) === true ? e.target : findById( parentLink );
          var parentTabId = searchParent(e.target, TABS_JS);
          var parentTab = findById(parentTabId);
          var $parentListItems = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTITEM)
          );
          var $parentListLinks = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
          );
          var $parentListContents = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_CONTENT)
          );
          var firstLink = $parentListItems[0].querySelector(
            "." + TABS_JS_LISTLINK
          );
          var lastLink = $parentListItems[
            $parentListItems.length - 1
          ].querySelector("." + TABS_JS_LISTLINK);

          // strike home on a tab => 1st tab
          if (e.keyCode === 36) {
            unSelectLinks($parentListLinks);
            unSelectContents($parentListContents);
            selectLink(firstLink);

            e.preventDefault();
          }
          // strike end on a tab => last tab
          else if (e.keyCode === 35) {
            unSelectLinks($parentListLinks);
            unSelectContents($parentListContents);
            selectLink(lastLink);

            e.preventDefault();
          }
          // strike up or left on the tab => previous tab
          else if ((e.keyCode === 37 || e.keyCode === 38) && !e.ctrlKey) {
            if (firstLink.getAttribute(ATTR_SELECTED) === "true") {
              unSelectLinks($parentListLinks);
              unSelectContents($parentListContents);
              selectLink(lastLink);

              e.preventDefault();
            } else {
              selectLinkInList(
                $parentListItems,
                $parentListLinks,
                $parentListContents,
                "prev"
              );
              e.preventDefault();
            }
          }
          // strike down or right in the tab => next tab
          else if ((e.keyCode === 40 || e.keyCode === 39) && !e.ctrlKey) {
            if (lastLink.getAttribute(ATTR_SELECTED) === "true") {
              unSelectLinks($parentListLinks);
              unSelectContents($parentListContents);
              selectLink(firstLink);

              e.preventDefault();
            } else {
              selectLinkInList(
                $parentListItems,
                $parentListLinks,
                $parentListContents,
                "next"
              );
              e.preventDefault();
            }
          }
        }

        // Key down in tab panels
        var parentTabPanelId = searchParent(e.target, TABS_JS_CONTENT);
        if (parentTabPanelId !== "" && eventName === "keydown") {
          (function () {
            var linkSelected = findById(
              findById(parentTabPanelId).getAttribute(ATTR_LABELLEDBY)
            );
            var parentTabId = searchParent(e.target, TABS_JS);
            var parentTab = findById(parentTabId);
            var $parentListItems = [].slice.call(
              parentTab.querySelectorAll("." + TABS_JS_LISTITEM)
            );
            var $parentListLinks = [].slice.call(
              parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
            );
            var $parentListContents = [].slice.call(
              parentTab.querySelectorAll("." + TABS_JS_CONTENT)
            );
            var firstLink = $parentListItems[0].querySelector(
              "." + TABS_JS_LISTLINK
            );
            var lastLink = $parentListItems[
              $parentListItems.length - 1
            ].querySelector("." + TABS_JS_LISTLINK);

            // strike up + ctrl => go to header
            if (e.keyCode === 38 && e.ctrlKey) {
              setTimeout(function () {
                linkSelected.focus();
              }, 0);
              e.preventDefault();
            }
            // strike pageup + ctrl => go to prev header
            if (e.keyCode === 33 && e.ctrlKey) {
              // go to header
              linkSelected.focus();
              e.preventDefault();
              // then previous
              if (firstLink.getAttribute(ATTR_SELECTED) === "true") {
                unSelectLinks($parentListLinks);
                unSelectContents($parentListContents);
                selectLink(lastLink);
              } else {
                selectLinkInList(
                  $parentListItems,
                  $parentListLinks,
                  $parentListContents,
                  "prev"
                );
              }
            }
            // strike pagedown + ctrl => go to next header
            if (e.keyCode === 34 && e.ctrlKey) {
              // go to header
              linkSelected.focus();
              e.preventDefault();
              // then next
              if (lastLink.getAttribute(ATTR_SELECTED) === "true") {
                unSelectLinks($parentListLinks);
                unSelectContents($parentListContents);
                selectLink(firstLink);
              } else {
                selectLinkInList(
                  $parentListItems,
                  $parentListLinks,
                  $parentListContents,
                  "next"
                );
              }
            }
          })();
        }

        // click on a tab link
        var parentLinkToPanelId = searchParent(e.target, TABS_JS_LINK_TO_TAB);
        if (
          (hasClass(e.target, TABS_JS_LINK_TO_TAB) === true ||
            parentLinkToPanelId !== "") &&
          eventName === "click"
        ) {
          var panelSelectedId =
            hasClass(e.target, TABS_JS_LINK_TO_TAB) === true
              ? e.target.getAttribute("href").replace("#", "")
              : findById(parentLinkToPanelId).replace("#", "");
          var panelSelected = findById(panelSelectedId);
          var buttonPanelSelected = findById(
            panelSelected.getAttribute(ATTR_LABELLEDBY)
          );
          var parentTabId = searchParent(e.target, TABS_JS);
          var parentTab = findById(parentTabId);
          //let $parentListItems = [].slice.call(parentTab.querySelectorAll('.' + TABS_JS_LISTITEM));
          var $parentListLinks = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_LISTLINK)
          );
          var $parentListContents = [].slice.call(
            parentTab.querySelectorAll("." + TABS_JS_CONTENT)
          );

          unSelectLinks($parentListLinks);
          unSelectContents($parentListContents);
          selectLink(buttonPanelSelected);

          e.preventDefault();
        }
      },
      true
    );
  });

  var onLoad = function onLoad() {
    attach();
    document.removeEventListener("DOMContentLoaded", onLoad);
  };

  document.addEventListener("DOMContentLoaded", onLoad);

  window.van11yAccessibleTabPanelAria = attach;
})(document);

       

    </script>
						<script>
						jQuery(document).ready(function($) {
    var tabwrapWidth= $('.tabs-wrapper').outerWidth();
    var totalWidth=0;
    jQuery("ul li").each(function() { 
      totalWidth += jQuery(this).outerWidth(); 
    });
    if(totalWidth > tabwrapWidth){
      $('.scroller-btn').removeClass('inactive');
    }
    else{
      $('.scroller-btn').addClass('inactive');
    }

    if($("#scroller").scrollLeft() == 0 ){
      $('.scroller-btn.left').addClass('inactive');
    }
    else{
       $('.scroller-btn.left').removeClass('inactive');
    }
		var liWidth= $('#scroller li').outerWidth();
		var liCount= $('#scroller li').length;
		var scrollWidth = liWidth * liCount;

				$('.right').on('click', function(){
          $('.nav-tabs').animate({scrollLeft: '+=200px'}, 300);
          console.log($("#scroller").scrollLeft() + " px");
				});
				
				$('.left').on('click', function(){
					$('.nav-tabs').animate({scrollLeft: '-=200px'}, 300);
				});
      scrollerHide()
     
      function scrollerHide(){
        var scrollLeftPrev = 0;
        $('#scroller').scroll(function () {
            var $elem=$('#scroller');
            var newScrollLeft = $elem.scrollLeft(),
                width=$elem.outerWidth(),
                scrollWidth=$elem.get(0).scrollWidth;
            if (scrollWidth-newScrollLeft==width) {
                $('.right.scroller-btn').addClass('inactive');
            }
            else{

                 $('.right.scroller-btn').removeClass('inactive');
            }
            if (newScrollLeft === 0) {
              $('.left.scroller-btn').addClass('inactive');
            }
            else{

                 $('.left.scroller-btn').removeClass('inactive');
            }
            scrollLeftPrev = newScrollLeft;
        });
      }
	});
						</script>
		<script type="text/javascript">
        $(document).ready(function() {
            var text = ['DevOps', 'Cloud Services', 'Site Services', 'Support']; //Text to be shown
            var index = 0; //Starting index for rotating text
            var changeText = setInterval(function() {
                if (index == text.length - 1)
                    index = 0;
                else
                    index++;
                $('.rotate-text').animate({
                    "opacity": "0",
                    "margin-left": "10px"
                }, 500, function() {
                    $('.rotate-text').text(text[index]);
                    $('.rotate-text').animate({
                        "opacity": "1",
                        "margin-left": "0"
                    }, 500);
                });

            }, 2000);
        });
    </script>
    
	<script>
	$(document).ready(function () {
  $(".at-title").click(function () {
    $(this).toggleClass("active").next(".at-tab").slideToggle().parent().siblings().find(".at-tab").slideUp().prev().removeClass("active");
  });
});
	</script>
<script>
const accordionItemHeaders = document.querySelectorAll(
  ".accordion-item-header"
);

accordionItemHeaders.forEach((accordionItemHeader) => {
  accordionItemHeader.addEventListener("click", (event) => {
    // Uncomment in case you only want to allow for the display of only one collapsed item at a time!

    const currentlyActiveAccordionItemHeader = document.querySelector(
      ".accordion-item-header.active"
    );
    if (
      currentlyActiveAccordionItemHeader &&
      currentlyActiveAccordionItemHeader !== accordionItemHeader
    ) {
      currentlyActiveAccordionItemHeader.classList.toggle("active");
      currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
    }
    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if (accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    } else {
      accordionItemBody.style.maxHeight = 0;
    }
  });
});
</script>


<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>