<!DOCTYPE html>
<html lang="zxx">
    
    <head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="icon" href="img/fevicon.png" sizes="192x192">
        <style>
            .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);
}
        </style>
    </head>
<body>
    <?php include("header.php"); ?>
    <!-- Header End -->
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="" style=" background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);">
            <div class="hero__item set-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1 class="ttl-heading" style="color:#fff; font-weight: 900; font-size: 56px;">Smart Automation for Speak Performance</h1>
                                    <!--<h4 class="service-cont">Service And Solutions</h4>-->
                                <p class="automate">Keep costs low and resouces lean with complimentary cloud scaling technology</p>
                                
                                <!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
                            <div class="row">
					<div class="col-md-4">
					<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Get Started</a>			
					</div>
					<div class="col-md-4">
								<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Contact Us</a>
				</div>
        <div class="col-md-4">
								&nbsp;
				</div>
				</div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
                                <img src="img/autoscaling-hero-logo.png" >
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                    <a href="#" class="Btn"><img class="mus" src="img/mouse-accest-1.png"><i class="fa fa-arrow-down chev-dn"></i>
                
                </a>
                                
            </div>
        </div>
    </section>
    <div class="menu-section">
        <div class="container">
        
          <div class="row">
          
            <div class="col-md-8">
              
              <div class="row">
            <div class="col-md-2">
              <a href="#overview" class="btn-href">Overview</a>
</div>
<div class="col-md-2">
<a href="#Benefits" class="btn-href">Solutions </a>
</div>
<div class="col-md-2">
<a href="#Offerings" class="btn-href">Offerings</a>
</div>
<div class="col-md-3">
<a href="#Successstory" class="btn-href">Successstory</a>
</div>
<div class="col-md-3">
<a href="#Resources" class="btn-href">Resources</a>
</div>
</div>
</div>
<div class="col-md-4">
<a href="#" class="clr-10">Get Started</a>
</div>
</div>
</div>
</div>
<div class="discover-whats" id="overview">
<div class="container">
    <div class="row">
    <h2>Overview</h2>
    <h3>Discover What’s Happening</h3>
    <p class="sub-h">Whether you’re looking to spur innovation and agility with Azure cloud, lower costs or build operational efficiencies, SNWN can help.
Our Microsoft® Azure® certified cloud experts put cutting-edge capabilities to work for your business. We apply deep expertise in cloud strategy, cloud-native
development, containers, application modernization, AI/ML Ops, IoT and workload management to help you accelerate innovation with Microsoft Azure.</p>
<div class="all-features" style="">

                                <div class="row">
                                  
                                 <div class="col-md-4">
                                     <div class="autoscaling">
                                     <h3 class="heading-feature">What is autoscaling?</h3>
                                    <p>Autoscaling is a feature of cloud computing that automatically allocates resources based on real-time demands and workloads. By automatically scaling resources up or down, performance is consistently responsive, resilient, and budget-friendly.</p>
                                 </div>
                                    </div>
                                 <div class="col-md-4">
                                     <div class="autoscaling">
                                     <h3>How does it work?</h3>
                                    <p>Autoscaling monitors your website and triggers a scaling event whenever traffic begins to exceed concurrent user capacity. The scaling event dynamically adjusts cloud resources to meet the increased demand.</p>
                                 </div>
                                    </div>
                                 <div class="col-md-4">
                                     <div class="autoscaling">
                                     <h3>What are the benefits?</h3>
                                     <p>Autoscaling helps you save money on resource costs and maintain top performance hassle-free. It works when you need it to work, allowing you to drive more traffic, gather more data, and focus on creating a successful website. Know more.</p>
                                 </div>
                                    </div>
                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              <div class="snwn-benefits" id="Benefits">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Benefits</h2>
                            <p>Common Enterprise Challenges vs Benefits:
                            Why Deploy Microsoft Azure for Your Enterprise?</p>
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
                                  <div class="img">
                                <img src="img/automatic.png">
                                      </div>
                                <div class="freatures-project">
                                <h5>Automatic</h5>
                                <p>AS automatically creates and removes CVM instances in real time based on the business load, ensuring that you are running the optimal number of instances and eliminating the need for manual deployment.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/cost-saving.png">
 
                                 </div>
                                <div class="freatures-project">
                                <h5>Cost-saving</h5>
                                <p>AS helps you maintain an optimal number of instances for variable business demand. When the demand increases, AS will automatically and quickly add new CVM instances. Conversely, when the demand decreases,AS will automatically remove</p>
                                
                                </div>
                             </div>  
                            </div>
                                
                                <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Schedulable</h5>
                                <p>AS allows you to schedule scaling to respond to regular changes in business load (e.g. scaling up at 9 p.m. every day).</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                 <img src="img/fault-tolerant.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Fault-tolerant</h5>
                                <p>AS automatically creates and removes CVM instances in real time based on the business load, ensuring that you are running the optimal number of instances and eliminating the need for manual deployment.</p>
                                </div>
                             </div> 
                        </div>


                        <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/easy-to-audit.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Easy to Audit</h5>
                                <p>AS helps you maintain an optimal number of instances for variable business demand. When the demand increases, AS will automatically and quickly add new CVM instances. Conversely, when the demand decreases, AS will automatically remove</p>
                                
                                </div>
                             </div>  
                            </div>


                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Schedulable</h5>
                                <p>AS allows you to schedule scaling to respond to regular changes in business load (e.g. scaling up at 9 p.m. every day).</p>
                                
                                </div>
                             </div>  
                            </div>
                                </div>
                            
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="snwn-benefits-1" id="Offerings">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Offerings</h2>
                            <p>Find solutions for putting your ideas into action</p>
                            <div class="row">
                            <div class="col-md-4">
                              <div class="project">
                                  <div class="img">
                                <img src="img/automatic.png">
                                      </div>

                                <div class="freatures-project">
                                <h5>Migrate to Azure</h5>
                                <p>AS automatically creates and removes CVM instances in real time based on the business load, ensuring that you are running the optimal number of instances and eliminating the need for manual deployment.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/cost-saving.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Data Analytics</h5>
                                <p>AS helps you maintain an optimal number of instances for variable business demand. When the demand increases, AS will automatically and quickly add new CVM instances. Conversely, when the demand decreases,AS will automatically remove</p>
                                </div>
                             </div>  
                            </div> 
                                <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Comprehensive expertise</h5>
                                <p>AS allows you to schedule scaling to respond to regular changes in business load (e.g. scaling up at 9 p.m. every day).</p>
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                 <img src="img/fault-tolerant.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Seamless Integration</h5>
                                <p>AS automatically creates and removes CVM instances in real time based on the business load, ensuring that you are running the optimal number of instances and eliminating the need for manual deployment.</p>
                                </div>
                             </div> 
                        </div>
                        <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/easy-to-audit.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Proactive Optimisation</h5>
                                <p>AS helps you maintain an optimal number of instances for variable business demand. When the demand increases, AS will automatically and quickly add new CVM instances. Conversely, when the demand decreases, AS will automatically remove</p>
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-4">
                             <div class="project">
                                 <div class="img">
                                <img src="img/schedulable.png">
                                 </div>
                                <div class="freatures-project">
                                <h5>Tailored Solutions</h5>
                                <p>AS allows you to schedule scaling to respond to regular changes in business load (e.g. scaling up at 9 p.m. every day).</p>
                                </div>
                             </div>  
                            </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>     
            <div class="approach-1" id="Successstory">
<div class="container">
<div class="row">
	<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<div class="">
						<h3>Discover your Azure
            Managed Services</h3>
						<p>Discover related technology platforms and
solutions to help you achieve smarter
business outcomes.</p>
<a href="#" class="clr-16">Get a free qoutes <i class="fa fa-arrow-right mg-right"></i></a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="box-services">
						<div class="accordion">
  <div class="at-item">
    <div class="at-title active">
      <h2>Assessment and Planning</h2>
    </div>
    <div class="at-tab" style="display: block;">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
  <div class="at-item">
    <div class="at-title">
      <h2>Continuous Integration</h2>
    </div>
    <div class="at-tab">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
	<div class="at-item">
    <div class="at-title">
      <h2>Continuous Deployment</h2>
    </div>
    <div class="at-tab">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
  <div class="at-item">
    <div class="at-title">
      <h2>Stringent Security Protocols</h2>
    </div>
    <div class="at-tab">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
</div>
					</div>
				</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="devops-services" id="Resources">
<div class="container">
<div class="row">
	<div class="col-md-12">
			<div class="row">
				<div class="col-md-6"><h3>Why Choose SNWN as Your
DevOps Services Company?</h3></div>
				<div class="col-md-6"><p>Clients choose us because of our ability to
improve business agility, increase efficiency, and
reduce costs with:</p></div>
				<section id="it-help-desk mb-3">
        <div class="container">
            <div class="row pt-5">
                <div class="col-md-4 mb-3">
                    <div class="nav flex-column nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link  mb-4 p-3 show text-uppercase color-tab  active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="false" onmouseover="openCity(event, 'IT')">Complex Delivery Experience</a>
                        <a class="nav-link mb-4 p-3 text-uppercase color-tab" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false" onmouseover="openCity(event, 'Focus')">Skilled DevOps Engineers</a>
                        <a class="nav-link mb-4 p-3 text-uppercase color-tab" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false" onmouseover="openCity(event, 'Expert')">Best Security Integration</a>
                        <a class="nav-link p-3 text-uppercase color-tab" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="true" onmouseover="openCity(event, 'Safe')">Dedicated DevOps Team</a>
                    </div>
                </div>
                <div class="col-md-8 mb-3">
                    <div class="tab-content text-center" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="IT" role="tabpanel" aria-labelledby="v-pills-home-tab" style="display: block;">
							<div class="container">
							<div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6">
								<div class="inner-padding">
                            <p class="text-justify">We have 8+ years of experience in
helping organizations streamline the
release cycles of different applications.
Our team of experts automate real-time
code correction throughout the develop-
pment process to eliminate security
breaches.</p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane fade show" id="Focus" role="tabpanel" aria-labelledby="v-pills-profile-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img-3.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6 padding-0">
								<div class="inner-padding">
                            <p class="text-justify">Installing IT infrastructure with networks, servers, security, storage, and a whole host of other components is extremely pricey. Outsourcing IT system support transforms fixed IT costs into variable costs and gives you room to budget accordingly. In short, you only pay for what you use, which can be a huge cost reduction means you can easily manage your annual operating costs.</p>
							</div>
								</div>
								</div>
								</div>
                        </div>
                        <div class="tab-pane fade show" id="Safe" role="tabpanel" aria-labelledby="v-pills-messages-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img-1.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6">
								<div class="inner-padding">
                            <p class="text-justify">By outsourcing IT support, you can free up its valuable resources and focus on what matters: making the wheels of the business turn. Our support experts will answer in least working time. We can also work with LIVE CHAT from your website. It is important for your technology to run 24/7 because it is the core of your business and we are focused on quality and fastness. </p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane fade show" id="Expert" role="tabpanel" aria-labelledby="v-pills-settings-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img-2.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6">
								<div class="inner-padding">
                            <p class="text-justify">Your company’s doesn’t likely have the budget to hire a panel of IT experts to guide you through the changing world of big data. When you outsource your IT support, you will gain access to top levels of technology and resources. This access will help you stay competitive in your industry. Certified team of experts can build that difference for you!</p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>		
		</div>
	</div>
</div>
</div>
</div>	
<div class="faq-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Want to know more?</h3>
                <h2>Frequently Asked Questions</h2>
                <div class="row">
                <div class="col-md-6">
                <section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd3">
    <label for="rd3" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
    <div class="tab">
    <input type="radio" name="accordion-2" id="rd4">
    <label for="rd4" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
</section>
</div>
<div class="col-md-6">
<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd1">
    <label for="rd1" class="tab__label">What are the benefits of DevOps?</label>
    <div class="tab__content">
      <p>If you want to have only one tab open, you can use .</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd2">
    <label for="rd2" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
</section>
    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
    <div class="know-more">
<div class="container">
<div class="row">
    <div class="col-md-4">
        <div class="bx-img">
            <img src="img/smart-discussing-meeting-report-phone.png">
            <h5>A bank, the cloud, and
a paradigm shift</h5>
            <a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="bx-img">
            <img src="img/engineers-looking-bridge-construction.png">
            <h5>A bank, the cloud, and
a paradigm shift</h5>
            <a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
            
        </div>
    </div>
    <div class="col-md-4">
        <div class="bx-img">
            <img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
            <h5>A bank, the cloud, and
a paradigm shift</h5>
            <a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
        
        </div>
    </div>
    <a href="#" class="clr-1">Read our Blog <i class="fa fa-arrow-right mg-right"></i></a>
</div>  
</div>
</div>
<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
          <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
          <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>
      <div class="col-md-7">
          <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
              </div>
      </div>
    </div>
  </div>
</section>
<?php include("footer.php"); ?>
    <!-- Footer Section End -->
    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/submail.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
                        <script>
                        jQuery(document).ready(function($) {
    var tabwrapWidth= $('.tabs-wrapper').outerWidth();
    var totalWidth=0;
    jQuery("ul li").each(function() { 
      totalWidth += jQuery(this).outerWidth(); 
    });
    if(totalWidth > tabwrapWidth){
      $('.scroller-btn').removeClass('inactive');
    }
    else{
      $('.scroller-btn').addClass('inactive');
    }

    if($("#scroller").scrollLeft() == 0 ){
      $('.scroller-btn.left').addClass('inactive');
    }
    else{
       $('.scroller-btn.left').removeClass('inactive');
    }
        var liWidth= $('#scroller li').outerWidth();
        var liCount= $('#scroller li').length;
        var scrollWidth = liWidth * liCount;

                $('.right').on('click', function(){
          $('.nav-tabs').animate({scrollLeft: '+=200px'}, 300);
          console.log($("#scroller").scrollLeft() + " px");
                });
                
                $('.left').on('click', function(){
                    $('.nav-tabs').animate({scrollLeft: '-=200px'}, 300);
                });
      scrollerHide()
     
      function scrollerHide(){
        var scrollLeftPrev = 0;
        $('#scroller').scroll(function () {
            var $elem=$('#scroller');
            var newScrollLeft = $elem.scrollLeft(),
                width=$elem.outerWidth(),
                scrollWidth=$elem.get(0).scrollWidth;
            if (scrollWidth-newScrollLeft==width) {
                $('.right.scroller-btn').addClass('inactive');
            }
            else{

                 $('.right.scroller-btn').removeClass('inactive');
            }
            if (newScrollLeft === 0) {
              $('.left.scroller-btn').addClass('inactive');
            }
            else{

                 $('.left.scroller-btn').removeClass('inactive');
            }
            scrollLeftPrev = newScrollLeft;
        });
      }
    });
                        </script>
        
    
    <script>
    $(document).ready(function () {
  $(".at-title").click(function () {
    $(this).toggleClass("active").next(".at-tab").slideToggle().parent().siblings().find(".at-tab").slideUp().prev().removeClass("active");
  });
});
    </script>
<script>
const accordionItemHeaders = document.querySelectorAll(
  ".accordion-item-header"
);

accordionItemHeaders.forEach((accordionItemHeader) => {
  accordionItemHeader.addEventListener("click", (event) => {
    // Uncomment in case you only want to allow for the display of only one collapsed item at a time!

    const currentlyActiveAccordionItemHeader = document.querySelector(
      ".accordion-item-header.active"
    );
    if (
      currentlyActiveAccordionItemHeader &&
      currentlyActiveAccordionItemHeader !== accordionItemHeader
    ) {
      currentlyActiveAccordionItemHeader.classList.toggle("active");
      currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
    }
    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if (accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    } else {
      accordionItemBody.style.maxHeight = 0;
    }
  });
});
</script>


<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

        //Scroll back to top

        var progressPath = document.querySelector('.progress-wrap path');
        var pathLength = progressPath.getTotalLength();
        progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
        progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
        progressPath.style.strokeDashoffset = pathLength;
        progressPath.getBoundingClientRect();
        progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
        var updateProgress = function () {
            var scroll = $(window).scrollTop();
            var height = $(document).height() - $(window).height();
            var progress = pathLength - (scroll * pathLength / height);
            progressPath.style.strokeDashoffset = progress;
        }
        updateProgress();
        $(window).scroll(updateProgress);
        var offset = 50;
        var duration = 550;
        jQuery(window).on('scroll', function() {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('.progress-wrap').addClass('active-progress');
            } else {
                jQuery('.progress-wrap').removeClass('active-progress');
            }
        });
        jQuery('.progress-wrap').on('click', function(event) {
            event.preventDefault();
            jQuery('html, body').animate({scrollTop: 0}, duration);
            return false;
        })


    });

})(jQuery);

</script>
        <script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
            
    
</body>

</html>