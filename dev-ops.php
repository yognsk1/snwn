<!DOCTYPE html>
<html lang="zxx">
	
	<head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="icon" href="img/fevicon.png" sizes="192x192">
		<style>
			.page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#0B84C6 0%,#221C81 100%);
}
		</style>
	</head>
<body>
	<?php include("header.php"); ?>
    <!-- Header End -->
    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="" style="background-repeat: no-repeat; background-size: cover; background-image: url('img/devops-banner-bg-1.png'); background-position: left;
  padding: 0px 0 101px 0;
  position: relative;">
            <div class="hero__item set-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1 class="ttl-heading" style="color:#fff; font-weight: 900;">DevOps</h1>
                                    <h4 class="service-cont">Service And Solutions</h4>
								<p class="automate">Automate, scale, and modernize product engineering</p>
								
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
                            <div class="row">
					<div class="col-md-4">
					<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Get Started</a>			
					</div>
					<div class="col-md-4">
								<a href="#" class="bttn-read-m-1" style="font-weight: 400;">Contact Us</a>
				</div>
        <div class="col-md-4">
								&nbsp;
				</div>
				</div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
                                
                            </div>
                        </div>
                    </div>
                </div>
				
					<a href="#" class="Btn"><img class="mus" src="img/mouse-accest-1.png"><i class="fa fa-arrow-down chev-dn"></i>
				
				</a>
								
            </div>
        </div>
    </section>
	<div class="we-align-devs">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    <div class="col-md-12">
						<div class="whychoose-algn">
                        <h2>We align Devs with Ops</h2>
                        
						<p>Our services span the application life cycle and can be used for any application from digital customer facing systems
to large-scale enterprise products. Our comprehensive DevOps services have a diverse portfolio of use-cases, from
consumer-facing systems to enterprise-level applications.
</p>
                    </div>
						</div>
                    
				<div class="row" style="border-radius: 8px;" >
			<div class="col-md-4" style="padding: 0;">
				<div class="case-st-img" >
			<img src="img/align-dev.png">
					</div>
			</div>
			<div class="col-md-8" style="padding: 0;">
				<div class="global-leading">
			<fieldset id="accordion">
    <label>
		<span>DevOps assessment <i class="fa fa-chevron-down right-icn"></i></span>
		<input type="radio" value="bar1" name="accordion" checked>
		<div><p>We audit infrastructures and software delivery processes in place to reveal
bottlenecks and offer a feasible optimization plan spanning your technological
stack, software workflows, and human resources’ workload.</p></div>
	</label>
	<label>
		<span>DevOps automation <i class="fa fa-chevron-down right-icn"></i></span>
		<input type="radio" value="bar2" name="accordion">
		<div><p>We audit infrastructures and software delivery processes in place to reveal
bottlenecks and offer a feasible optimization plan spanning your technological
stack, software workflows, and human resources’ workload.</p></div>
	</label>
	<label>
		<span>DevOps management <i class="fa fa-chevron-down right-icn"></i></span>
		<input type="radio" value="bar3" name="accordion">
		<div><p>We audit infrastructures and software delivery processes in place to reveal
bottlenecks and offer a feasible optimization plan spanning your technological
stack, software workflows, and human resources’ workload.</p></div>
	</label>
</fieldset>
  
					</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
</div>
	 <div class="devops-solution">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Benefits of Our DevOps Solutions</h2>
							<p>Our DevOps solutions help organizations deliver software faster, more reliably, and more efficiently. We offer
a range of services, including consulting, implementation, managed services and tools.</p>
                            <div class="row">
                            <div class="col-md-3">
                              <div class="project">
								  <div class="img">
                                <img src="img/aceess.png">
									  </div>

                                <div class="freatures-project">
                                <h5>Access to professional
DevOps engineers</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/cost-reduce.png">
 
								 </div>
                                <div class="freatures-project">
                                <h5>Cost reduction Cost reduction</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
                             </div>  
                            </div>
								
								<div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/operational-support.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Improved operational
support</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
								 <img src="img/devepment-team.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Increased development
team agility</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
								 
								
                             </div> 
								
                            
                        </div>
								
                            <div class="col-md-3">
                              <div class="project">
								  <div class="img">
                                <img src="img/aceess.png">
									  </div>

                                <div class="freatures-project">
                                <h5>Access to professional
DevOps engineers</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/cost-reduce.png">
 
								 </div>
                                <div class="freatures-project">
                                <h5>Cost reduction Cost reduction</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
                             </div>  
                            </div>
								
								<div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/operational-support.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Improved operational
support</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
								 <img src="img/devepment-team.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Increased development
team agility</h5>
                                <p>Hiring our DevOps team, you get quick
and easy access to experienced
DevOps engineers, who are able to
power your project with their
expertise right from the start.</p>
                                
                                </div>
								 
								
                             </div> 
								
                            
                        </div>
								</div>
							<div class="benefits-all">
								<div class="row">
								 <div class="col-md-3">
									 <h3>Technical benefits</h3>
									 <ul>
									 	 <li>Continuous software delivery</li>
										 <li>Facilitated release planning</li>
										 <li>Efficient issue resolution</li>
									 </ul>
								 </div>
								 <div class="col-md-3">
									 <h3>Business benefits</h3>
									 <ul>
									 	 <li>Continuous software delivery</li>
										 <li>Facilitated release planning</li>
										 <li>Efficient issue resolution</li>
									 </ul>
								 </div>
								 <div class="col-md-3">
									 <h3>Staff benefits</h3>
									 <ul>
									 	 <li>Continuous software delivery</li>
										 <li>Facilitated release planning</li>
										 <li>Efficient issue resolution</li>
									 </ul>
								 </div>
								<div class="col-md-3">
									 <h3>Staff benefits</h3>
									 <ul>
									 	 <li>Continuous software delivery</li>
										 <li>Facilitated release planning</li>
										 <li>Efficient issue resolution</li>
									 </ul>
								 </div>
									</div>
								</div>
                        </div>
                    </div>
                </div>
            </div>



			



<div class="approach">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    
				<div class="col-md-6">
					<div class="">
						
						<h3>Our DevOps Approach</h3>
						<p>Our DevOps approach uses the best CI/CD
processes, tools, and practices needed to
accelerate the software delivery process.
The key elements of the DevOps approach
we follow for all our projects include:</p>
<a href="#" class="clr-3">All Services <i class="fa fa-arrow-right mg-right"></i></a>
					
					</div>
				</div>
				<div class="col-md-6">
					<div class="box-services">
						<div class="accordion">
  <div class="at-item">
    <div class="at-title active">
      <h2>Assessment and Planning</h2>
    </div>
    <div class="at-tab" style="display: block;">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
  <div class="at-item">
    <div class="at-title">
      <h2>Continuous Integration</h2>
    </div>
    <div class="at-tab">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
	<div class="at-item">
    <div class="at-title">
      <h2>Continuous Deployment</h2>
    </div>
    <div class="at-tab">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
  <div class="at-item">
    <div class="at-title">
      <h2>Stringent Security Protocols</h2>
    </div>
    <div class="at-tab">
      We assess the current organizational processes
and IT infrastructure and create a comprehensive
roadmap for automating the infrastructure.
    </div>
  </div>
</div>
						
					
					</div>
				</div>
				
		</div>
		
	</div>
</div>
</div>
</div>
	
	
	
	
	
	
	
	
	<div class="timeline-devops" style="">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
                    
				<div class="col-md-6">
					<div class="">
						
						<h3>Our DevOps Services Include</h3>
						<p>Our pool of DevOps services enable on-demand scalability,
ensure optimum code efficiency, and eliminate security breaches</p>
<a href="#" class="clr-3">Get a free qoutes <i class="fa fa-arrow-right mg-right"></i></a>
					
					</div>
				</div>
				<div class="col-md-6">
					
</div>
						
					
					</div>
				</div>
				
		</div>
		
	</div>
</div>





<div class="our-devOps-solutions">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Benefits of Our DevOps Solutions</h2>
							
                            <div class="row">
                            <div class="col-md-3">
                              <div class="project">
								  <div class="img">
                                <img src="img/cloud-infrastructure-strategy-design.png">
									  </div>

                                <div class="freatures-project">
                                <h5>Cloud Infrastructure
Strategy and Design</h5>
                                <p>We configure, deploy, and maintain
your complete infrastructure under the
cloud throughout the application deliver
 lifecycle.</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/continuos-integration.png">
 
								 </div>
                                <div class="freatures-project">
                                <h5>Continuous Integration +
Continuous Delivery (CI/CD)</h5>
                                <p>We provide QA and testing for you
 code with a real-time online dashboard.
Test before deploying, follow our
CI/CD process to deploy automatically</p>
                                
                                </div>
                             </div>  
                            </div>
								
								<div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/cloud-infrastructure-automation.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Cloud Infrastructure
Automation Consulting</h5>
                                <p>We provide cloud infrastructure
automation consulting services as well.
We will assess your business requirement
 and start with a strategy</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
								 <img src="img/automated-monitoring.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Automated
Monitoring</h5>
                                <p>We provide cloud infrastructure monitoring
services that can not only detect an
 react to many potentially catastrophic
events</p>
                                
                                </div>
								 
								
                             </div> 
								</div>
								 <div class="col-md-3">
                              <div class="project">
								  <div class="img">
                                <img src="img/docker.png">
									  </div>

                                <div class="freatures-project">
                                <h5>Docker DevOps
Services
</h5>
                                <p>And this is a deliverable for an application
to have maximum functionality; hence
expect from Infoane that we dedicate
 our time</p>
                                
                                </div>
                              </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/kubernetes.png">
 
								 </div>
                                <div class="freatures-project">
                                <h5>Kubernetes
Consulting</h5>
                                <p>We provide Kubernetes consulting services
to manage your clusters and resources
better. You'll be able to visualize system
health,</p>
                                
                                </div>
                             </div>  
                            </div>
								
								<div class="col-md-3">
                             <div class="project">
								 <div class="img">
                                <img src="img/spring-boot.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Spring Boot
Consulting</h5>
                                <p>We provide Spring Boot consulting services
to manage your projects better,
develop and deploy applications.</p>
                                
                                </div>
                             </div>  
                            </div>
                            <div class="col-md-3">
                             <div class="project">
								 <div class="img">
								 <img src="img/jenkins.png">
								 </div>
                                <div class="freatures-project">
                                <h5>Jenkins
CI/CD</h5>
                                <p>We will help you implement CI/CD. We can
restructure your source code and integrate
it with continuous integration (CI) tools,
including Jenkins.</p>
                                
                                </div>
								 
								
                             </div> 
								</div>
							</div>
						</div>
					</div>
	</div>
	</div>
				<div class="devops-services">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
				<div class="col-md-6"><h3>Why Choose SNWN as Your
DevOps Services Company?</h3></div>
				<div class="col-md-6"><p>Clients choose us because of our ability to
improve business agility, increase efficiency, and
reduce costs with:</p></div>
				<section id="it-help-desk mb-3">
        <div class="container">
             
            <div class="row pt-5">
                <div class="col-md-4 mb-3">
                    <div class="nav flex-column nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link  mb-4 p-3 show text-uppercase color-tab  active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="false" onmouseover="openCity(event, 'IT')">Complex Delivery Experience</a>
                        <a class="nav-link mb-4 p-3 text-uppercase color-tab" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false" onmouseover="openCity(event, 'Focus')">Skilled DevOps Engineers</a>
                        <a class="nav-link mb-4 p-3 text-uppercase color-tab" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false" onmouseover="openCity(event, 'Expert')">Best Security Integration</a>
                        <a class="nav-link p-3 text-uppercase color-tab" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="true" onmouseover="openCity(event, 'Safe')">Dedicated DevOps Team</a>
                    </div>
                </div>
                <div class="col-md-8 mb-3">
                    <div class="tab-content text-center" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="IT" role="tabpanel" aria-labelledby="v-pills-home-tab" style="display: block;">
							<div class="container">
							<div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6">
								<div class="inner-padding">
                            <p class="text-justify">We have 8+ years of experience in
helping organizations streamline the
release cycles of different applications.
Our team of experts automate real-time
code correction throughout the develop-
pment process to eliminate security
breaches.</p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane fade show" id="Focus" role="tabpanel" aria-labelledby="v-pills-profile-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img-3.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6 padding-0">
								<div class="inner-padding">
                            <p class="text-justify">Installing IT infrastructure with networks, servers, security, storage, and a whole host of other components is extremely pricey. Outsourcing IT system support transforms fixed IT costs into variable costs and gives you room to budget accordingly. In short, you only pay for what you use, which can be a huge cost reduction means you can easily manage your annual operating costs.</p>
							</div>
								</div>
								</div>
								</div>
                        </div>
                        <div class="tab-pane fade show" id="Safe" role="tabpanel" aria-labelledby="v-pills-messages-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img-1.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6">
								<div class="inner-padding">
                            <p class="text-justify">By outsourcing IT support, you can free up its valuable resources and focus on what matters: making the wheels of the business turn. Our support experts will answer in least working time. We can also work with LIVE CHAT from your website. It is important for your technology to run 24/7 because it is the core of your business and we are focused on quality and fastness. </p>
							</div>
								</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane fade show" id="Expert" role="tabpanel" aria-labelledby="v-pills-settings-tab" style="display: none;">
							<div class="container">
                            <div class="row bg-clr-section">
							<div class="col-md-6 padding-0">
                            <img src="img/complex-delivery-img-2.png" alt="It Help Desk Cost" title="It Help Desk Cost" class="text-center">
								</div>
							<div class="col-md-6">
								<div class="inner-padding">
                            <p class="text-justify">Your company’s doesn’t likely have the budget to hire a panel of IT experts to guide you through the changing world of big data. When you outsource your IT support, you will gain access to top levels of technology and resources. This access will help you stay competitive in your industry. Certified team of experts can build that difference for you!</p>
							</div>
								</div>
								</div>
							</div>
                        </div>
							
                    </div>
                </div>
            </div>
        </div>
    </section>
				
				
				
		</div>
		
	</div>
</div>
</div>
</div>							
<div class="platform-we-use">
<div class="container">
<div class="row">
	<div class="col-md-12">
		
			<div class="row">
				<div class="col-md-6">
					<div class="box-services">
						
						<img src="img/devops-infographis-tools-2-1.png">
					
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="">
						
						<h3>DevOps Tools and Platforms
We Use</h3>
						<p>The key to success for a successful
DevOps transformation is to measure
your systems and digital business
impact. Here are a few essential
DevOps tools and platforms we use
for successful DevOps implementation</p>
					
					</div>
				</div>
				
		</div>
		
	</div>
</div>
</div>
</div>
	
	
	
	
	

<div class="faq-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Want to know more?</h3>
				<h2>Frequently Asked Questions</h2>
				<div class="row">
				<div class="col-md-6">
				<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd3">
    <label for="rd3" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
	<div class="tab">
    <input type="radio" name="accordion-2" id="rd4">
    <label for="rd4" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
  
</section>
					</div>
<div class="col-md-6">
<section class="accordion accordion--radio">
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd1">
    <label for="rd1" class="tab__label">What are the benefits of DevOps?</label>
    <div class="tab__content">
      <p>If you want to have only one tab open, you can use .</p>
    </div>
  </div>
  <div class="tab">
    <input type="radio" name="accordion-2" id="rd2">
    <label for="rd2" class="tab__label">What are the factors that decide DevOps success?</label>
    <div class="tab__content">
      <p>But if you wanna close the opened tab, you must add a "close" button somewhere, like the one below, that is just another styled radio input.</p>
    </div>
  </div>
	
  
</section>
	</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>
<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
		  <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
		  <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>

      <div class="col-md-7">
		  <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
			  </div>
      </div>
    </div>

    

  </div>
</section>
	
	<div class="know-more">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/smart-discussing-meeting-report-phone.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/engineers-looking-bridge-construction.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
			
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read Our Blog <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<a href="#" class="clr-1">Read our Blog <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>













<?php include("footer.php"); ?>

			
	
	
		
	
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
		
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
	<script>
	$(document).ready(function () {
  $(".at-title").click(function () {
    $(this).toggleClass("active").next(".at-tab").slideToggle().parent().siblings().find(".at-tab").slideUp().prev().removeClass("active");
  });
});
	</script>
<script>
const accordionItemHeaders = document.querySelectorAll(
  ".accordion-item-header"
);

accordionItemHeaders.forEach((accordionItemHeader) => {
  accordionItemHeader.addEventListener("click", (event) => {
    // Uncomment in case you only want to allow for the display of only one collapsed item at a time!

    const currentlyActiveAccordionItemHeader = document.querySelector(
      ".accordion-item-header.active"
    );
    if (
      currentlyActiveAccordionItemHeader &&
      currentlyActiveAccordionItemHeader !== accordionItemHeader
    ) {
      currentlyActiveAccordionItemHeader.classList.toggle("active");
      currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
    }
    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if (accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    } else {
      accordionItemBody.style.maxHeight = 0;
    }
  });
});
</script>


<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>