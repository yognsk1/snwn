<div class="progress-wrap">
      <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
<path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919px, 307.919px; stroke-dashoffset: 218.598px; stroke: rgb(252, 2, 3);"></path>
</svg>
  </div>
    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas__menu__overlay"></div>
    <div class="offcanvas__menu__wrapper">
        <div class="canvas__close">
            <span class="fa fa-times-circle-o"></span>
        </div>
        <div class="offcanvas__logo">
            <a href="#"><img src="img/snwn-logo.png"></a>
        </div>
        <nav class="offcanvas__menu mobile-menu">
            <ul>
                <li class="active"><a href="#">Managed IT Services</a>
                        <ul class="dropdown">
                                    <li><a href="#">Ecommerce Service</a></li>
                                    <li><a href="auto-scaling.html">Auto Scaling</a></li>
                                    <li><a href="#">CPanel</a></li>
                                    <li><a href="#">Applications</a></li>
                                    <li><a href="#">Disaster Recovery</a></li>
                                </ul>
                </li>
                <li><a href="#">Solution</a>
                    <ul class="dropdown">
                                    <li><a href="#">Cloud</a></li>
                                    <li><a href="dev-ops.html">DevOps</a></li>
                                    <li><a href="#">DevSecOps</a></li>
                                    <li><a href="#">Security</a></li>
                                    <li><a href="#">Load Balancing</a></li>
                                <li><a href="#">Cluster </a></li>
                                <li><a href="#">Migration</a></li>
                                <li><a href="#">Disaster Recovery</a></li>
                                
                                </ul>
                </li>
                <li><a href="#">Hosting</a></li>
                <li><a href="#">Dedicated Servers </a>
                    <ul class="dropdown">
                                    <li><a href="#">Cloud Setup [Private/Public] </a></li>
                                    
                                    <li><a href="#">Cloud Cunsultant</a></li>
                                    <li><a href="#">Cloud Migration</a></li>
                                    <li><a href="#">Cloud Optimization</a></li>
                                    <li><a href="#">Auto Scaling</a></li>
                                </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="offcanvas__auth">
            <ul>
                <li><a href="live-chat-support.html"> Support</a></li>
                <li><a href="#"> Contact Us</a></li>
            </ul>
            <!--<ul>
                <li ><a href="#">Login</a></li>
                <li><a href="#">Get Started</a></li>
            </ul>-->
        </div>
        <div class="offcanvas__info">
            <ul>
                <li><i class="fa fa-tags" aria-hidden="true"></i>
                    Save up to <span>50%</span>on Dedicated Servers – Chat Now!</li>
            </ul>
        </div>
    </div>
    <!-- Offcanvas Menu End -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="bg-head">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <div class="header__logo">
                        <a href="./index.html"><img src="img/snwn-logo.png"></a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10">
                    <nav class="header__menu">
                        <ul class="pull-right menu-middle">
                            <li class="active"><a href="#">Managed IT Services <i class="fa fa-chevron-down"></i></a>
							<ul class="dropdown">
                                    <li><a href="#">Ecommerce Service</a></li>
                                    <li><a href="auto-scaling.html">Auto Scaling</a></li>
                                    <li><a href="#">CPanel</a></li>
									<li><a href="#">Applications</a></li>
									<li><a href="#">Disaster Recovery</a></li>
                                </ul>

							</li>
                            <li><a href="#">Solution <i class="fa fa-chevron-down"></i></a>
							<ul class="dropdown">
                                    <li><a href="#">Cloud</a></li>
                                    <li><a href="dev-ops.html">DevOps</a></li>
                                    <li><a href="#">DevSecOps</a></li>
									<li><a href="#">Security</a></li>
									<li><a href="#">Load Balancing</a></li>
								<li><a href="#">Cluster </a></li>
								<li><a href="#">Migration</a></li>
								<li><a href="#">Disaster Recovery</a></li>
								
                                </ul>
							</li>
                            <li><a href="#">Cloud Services <i class="fa fa-chevron-down"></i></a>
							<ul class="dropdown">
                                    <li><a href="#">Cloud Setup [Private/Public] </a></li>
                                    
                                    <li><a href="#">Cloud Cunsultant</a></li>
									<li><a href="#">Cloud Migration</a></li>
									<li><a href="#">Cloud Optimization</a></li>
									<li><a href="#">Auto Scaling</a></li>
                                </ul>
							</li>
                            <li><a href="#">About Us</a></li>
							<li><a href="#">Contact Us</a></li>
                            
                            <!--<li><a href="#">Addons</a></li>-->
                        </ul>
                        <!--<ul class="login-n">
                            <li class="bg-clr"><a href="#">Login</a></li>
                           <li class="bg-clr"><a href="#">Get Started</a></li>
                        </ul>-->
                    </nav>
                </div>
            </div>
        </div>
            <div class="canvas__open">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </header>
    <!-- Header End -->