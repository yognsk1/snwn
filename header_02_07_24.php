<head>
  
<link rel="stylesheet" href="css/head.css" type="text/css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
<style>ul.new-radius a{ padding-left: 25px !important; }</style>
</head>
<div class="progress-wrap active-progress">
      <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
<path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear; stroke-dasharray: 307.919px, 307.919px; stroke-dashoffset: 133.777px; stroke: rgb(252, 2, 3);"></path>
</svg>
  </div>
<div class="container-fluid new-cnt-fluid">
<!-- START: RUBY DEMO HEADER -->
<div class="ruby-menu-demo-header">
  
      <!-- ########################### -->
      <!-- START: RUBY HORIZONTAL MENU -->
      <div class="ruby-wrapper">
        
        <button class="c-hamburger c-hamburger--htx visible-xs">
          <span>toggle menu</span>
        </button>
        <ul class="ruby-menu">
          <li class="ruby-active-menu-item"><div class="header__logo">
                        <a href="./index.php"><img src="img/SNWNLOGOW.png"></a>
                    </div></li>
          <li class="ruby-menu-mega-blog"><a class="menu-fnt" href="#">IT Infrastructure</a>
            <div style="height: 480px;" class="new-radius">
              <ul class="platfoms">
                <li><h6>Solving It challengess Across Indistries Every Day</h6></li>
                <hr>
                <li><h5>Cost Effective</h5>
                  <p>Affortable your Experts Solutions as Per your Needs</p>
                </li>
                <li><h5>Scalable plans</h5>
                <p>Your Server Grow Your Plan Adopts.</p>
                </li>
                <li><h5>White Labeled</h5>
                <p>Deliver Top Nouch SErvice under Your Brand</p>
                </li>
                <li><h5>Multi Channel Outsource</h5>
                <p>Seamless Support Across All Channels</p>
                </li>
              </ul>
              <ul class="ruby-menu-mega-blog-nav">

                <li class="ruby-active-menu-item "><a class="menu-fnt" href="#">Why Server Management</a>
                  <div class="ruby-grid ruby-grid-lined" style="height: 480px;">
                    <div class="ruby-row  sb-menu">
                      <div class="ruby-col-9">
                      <h3 class="hedng">MONITORING. MAINTENANCE. AUTOMATION</h3>
                      <h4 class="sub-hening mns">Why Server Managment?</h4>
                      <p>At the forefront of server management, we boast a cutting-edge infrastructure and a
dedicated technical support center. Our expertise extends to world-class server
management services and technical support for a wide array of server environments,
including Dedicated Servers, VPS (Virtual Private Servers)</p>

                      </div>
                    </div>
                    <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:75px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li class="hidden-md mns"><a class="menu-fnt" href="#">Support Management</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 480px; ">
                    <div class="ruby-row   sb-menu-7" style="border-bottom:1px solid #ccc; padding: 23px 0px 10px 0px; width:98%">
                      <div class="ruby-col-12">
                    <h3 class="hedng">SEAMLESS. RELIABLE. EFFECTIVE</h3>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                        
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">Web Hosting Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Helpdesk Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Application Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      &nbsp
                      </div>
                      <div class="ruby-col-11">
                      &nbsp;
                      </div>
                      </div>
</div>
<div class="ruby-row  sb-menu-6" style="padding: 23px 0px 10px 0px;">
                      
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">WordPress Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMJ.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Joomla Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Woocommerce Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                      
                    </div>
                    
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li class="mns"><a class="menu-fnt" href="#">Server Management</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 480px;">
                    <div class="ruby-row  sb-menu-7 ">
                    <div class="ruby-col-12">
                    <h3 class="hedng">SEAMLESS. RELIABLE. EFFECTIVE</h3>
                    </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1 ">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">cPanel Server Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Plesk and Other Web Panel Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Disaster Recovery Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Application Server Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                      
                    </div>
                    
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li class="mns"><a class="menu-fnt" href="#">Infrastructure Management</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 480px;">
                    <div class="ruby-row sb-menu sb-menu-7">
                    <div class="ruby-col-12">
                    <h3 class="hedng" style="">SEAMLESS. RELIABLE. EFFECTIVE</h3>
                    </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">DataCenter Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Infrastructure Setup & architect</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Server & Network Monitoring</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Storage Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMJ.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Virtualization Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Infrastructure Migration</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Infrastructure Optimization</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                    </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>
                <li><a class="menu-fnt" href="#">AI Management</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 480px;">
                    <div class="ruby-row   sb-menu-7">
                      <div class="ruby-col-12">
                    <h3 class="hedng">SEAMLESS. RELIABLE. EFFECTIVE</h3>
                      </div>
                      <div class="ruby-col-7 hov-bdr img-ai">
                        
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">AIOpps</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-7 hov-bdr img-ai">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">AI as a Service (AIaaS)</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-7 hov-bdr img-ai">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Automation Solution</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                    </div>
                      
</div>
              </li>
                <li class="award-im"><img src="img/navigation-icon/ISO-logo.png"></li>
              </ul>
              <ul>
                <li></li>
              </ul>
            </div>
          <span class="ruby-dropdown-toggle"></span></li>
          <li class="ruby-menu-mega-blog"><a href="#">Cloud Management</a>
          <div style="height: 500px;" class="new-radius">
            
              <ul class="platfoms">
                <li><h6>Platform Partenerships</h6></li>
                <li><img class="icon-navigate" src="img/navigation-icon/aws.png">AWS</li>
                <li><img class="icon-navigate" src="img/navigation-icon/g-cloud.png">Google Cloud</li>
                <li><img class="icon-navigate" src="img/navigation-icon/microsoft.png">Microsoft</li>
                <li><img class="icon-navigate" src="img/navigation-icon/kubernetes.png">Kuberbetes</li>
                <li><img class="icon-navigate" src="img/navigation-icon/OVHcloud.png">OVH Cloud</li>
              </ul>
              
              <ul class="ruby-menu-mega-blog-nav">

                <li class="ruby-active-menu-item"><img src="img/snwn-h1.jpg" class="img-fluid" width="100%" height="100%" >
                  <div class="ruby-grid ruby-grid-lined" style="height: 425.359px;">
                    <div class="ruby-row sb-menu">
                    <div class="ruby-col-12">
                    <h3 class="hedng" >OPTIMIZED CLOUD. MAXIMIZED EFFICIANCY.</h3>
                    </div>
                    <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">Cloud Infrastructure Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">Cloud Infrastructure Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Security & Firewall Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Cost and performance Optimization</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Cloud Migration</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr ">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMJ.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Cloud Monitoring</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Auto Scaling Setup & Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a href="#">AutoMation Services</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>

                    </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>
              </ul>
              <ul>
                <li></li>
              </ul>
            </div>
          <span class="ruby-dropdown-toggle"></span></li>
          <li class="ruby-menu-mega-blog"><a href="#">Industry</a>
          <div style="height: 500px;" class="new-radius">
          <ul class="platfoms">
                <li><h6>Solving It challengess Across Indistries Every Day</h6></li>
                <hr>
                <li><h5>Cost Effective</h5>
                  <p>Affortable your Experts Solutions as Per your Needs</p>
                </li>
                <li><h5>Scalable plans</h5>
                <p>Your Server Grow Your Plan Adopts.</p>
                </li>
                <li><h5>White Labeled</h5>
                <p>Deliver Top Nouch SErvice under Your Brand</p>
                </li>
                <li><h5>Multi Channel Outsource</h5>
                <p>Seamless Support Across All Channels</p>
                </li>
              </ul>
              <ul class="ruby-menu-mega-blog-nav">

                <li class="ruby-active-menu-item"><img src="img/snwn-h2.jpg" class="img-fluid" width="100%" height="100%" >
                  <div class="ruby-grid ruby-grid-lined" style="height: 425.359px;">
                    <div class="ruby-row sb-menu">
                    <div class="ruby-col-12">
                    <h3 class="hedng" style="margin-bottom:-15px !important;">YOUR INDUSTRY.OUR PASSION.</h3>
                    </div>
                    <div class="ruby-row">
                      <div class="ruby-col-6 ">
                      <div class="ruby-col-12 hov-bdr-2">
                      <a class="mns" href="#">Bank and Financial Sector</a>
                      </div>
                      <div class="ruby-col-12 hov-bdr-2">
                      <a class="mns" href="#">Healthcare</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-2">
                      <a class="mns" href="#">Ecommerce $ Shopping</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-2">
                      <a class="mns" href="#">Automotive</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-2" >
                      <a class="mns" href="#">Government</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-2">
                      <a class="mns" href="#">Media & Entertainment </a>
                      </div>
                     
                      <div class="ruby-col-12 hov-bdr-2">
                      <a class="mns" href="#">Other</a>
                      </div>
                    </div>
                    <div class="ruby-col-6">
                      <div class="main-bx">
                      <div class="ruby-row">
                      <h3>Case Studies :</h3>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                      </div>
                    </div>
                    </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                

                

                
                
              </ul>
              <ul>
                <li></li>
              </ul>
            </div></li>
          <li class="ruby-menu-mega-blog"><a href="#">Consulting</a>
          <div style="height: 500px;" class="new-radius">
          <ul class="platfoms">
                <li><h6>Solving It challengess Across Indistries Every Day</h6></li>
                <hr>
                <li><h5>Cost Effective</h5>
                  <p>Affortable your Experts Solutions as Per your Needs</p>
                </li>
                <li><h5>Scalable plans</h5>
                <p>Your Server Grow Your Plan Adopts.</p>
                </li>
                <li><h5>White Labeled</h5>
                <p>Deliver Top Nouch SErvice under Your Brand</p>
                </li>
                <li><h5>Multi Channel Outsource</h5>
                <p>Seamless Support Across All Channels</p>
                </li>
              </ul>
              <ul class="ruby-menu-mega-blog-nav">

                <li class="ruby-active-menu-item"><img src="img/snwn-h3.jpg" class="img-fluid" width="100%" height="100%" >
                  <div class="ruby-grid ruby-grid-lined" style="height: 425.359px;">
                    <div class="ruby-row sb-menu">

                    <div class="ruby-col-12">
                    <h3 class="hedng" style="margin-bottom:-15px !important;">YOUR SUCCESS.OUR EXPERTISE.</h3>
                    </div>
                    <div class="ruby-row">
                      <div class="ruby-col-6 ">
                      <div class="ruby-col-12 hov-bdr-4">
                      <a class="mns" href="#">DevSecOps</a>
                      </div>
                      <div class="ruby-col-12 hov-bdr-4">
                      <a class="mns" href="#">AWS Consulting</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-4">
                      <a class="mns" href="#">Azure Consulting</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-4">
                      <a class="mns" href="#">G-Cloud Consulting</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-4" >
                      <a class="mns" href="#">Docker Consulting</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-4">
                      <a class="mns" href="#">Kubernetes Consulting </a>
                      </div>
                     
                      
                    </div>
                    <div class="ruby-col-6">
                      <div class="main-bx">
                      <div class="ruby-row">
                        <h3>Case Studies :</h3>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                      </div>
                      
                      </div>
                    </div>











                      
                    </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li class="hidden-md"><a href="#">DevSecOps</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 500px;">
                    <div class="ruby-row sb-menu">
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">Web Hosting Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Helpdesk Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Application Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">WordPress Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMJ.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Joomla Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Woocommerce Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      
                    </div>
                    
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li><a href="#">AWS Consulting</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 500px;">
                    <div class="ruby-row sb-menu">
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">cPanel Server Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Plesk and Other Web Panel Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Disaster Recovery Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Application Server Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      
                      
                    </div>
                    
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li><a href="#">Azure Consulting</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 500px;">
                    <div class="ruby-row sb-menu sb-menu-1">
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">DataCenter Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Infrastructure Setup & architect</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Server & Network Monitoring</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div> 
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Storage Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMJ.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Virtualization Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Infrastructure Migration</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Infrastructure Optimization</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                    </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>
                <li><a href="#">G-Cloud Consulting</a></li>
                <li><a href="#">Docker Consulting</a></li>
                <li><a href="#">Kubernetes Consulting</a></li>
                <li><a href="#">Software Automation</a></li>
                <li><a href="#">Software Performance Optimization</a></li>
              </ul>
              <ul>
                <li></li>
              </ul>
            </div>
          <span class="ruby-dropdown-toggle"></span>
        </li>
          <li class="ruby-menu-mega-blog"><a href="#">Company</a>
          <div style="height: 500px;" class="new-radius">
          <ul class="platfoms">
                <li><h6>Solving It challengess Across Indistries Every Day</h6></li>
                <hr>
                <li><h5>Cost Effective</h5>
                  <p>Affortable your Experts Solutions as Per your Needs</p>
                </li>
                <li><h5>Scalable plans</h5>
                <p>Your Server Grow Your Plan Adopts.</p>
                </li>
                <li><h5>White Labeled</h5>
                <p>Deliver Top Nouch SErvice under Your Brand</p>
                </li>
                <li><h5>Multi Channel Outsource</h5>
                <p>Seamless Support Across All Channels</p>
                </li>
              </ul>
              <ul class="ruby-menu-mega-blog-nav" >

                <li class="ruby-active-menu-item "><img src="img/snwn-h1.jpg" class="img-fluid" width="100%" height="100%" >
                  <div class="ruby-grid ruby-grid-lined" style="height: 430.359px;">
                    <div class="ruby-row sb-menu">
                    <div class="ruby-col-12">
                    <h3 class="hedng" style="margin-bottom:-15px !important;">PASSION INTERGRITY EXCELLENCE</h3>
                    </div>
                    <div class="ruby-row">
                      <div class="ruby-col-6">
                      <div class="ruby-col-12 hov-bdr-5">
                      <a class="mns" href="#">Why SNWN</a>
                      </div>
                      <div class="ruby-col-12 hov-bdr-5">
                      <a class="mns" href="#">Company Profile</a>
                      </div>
                      
                      <div class="ruby-col-12 hov-bdr-5">
                      <a class="mns" href="#">Partnership And Certificate</a>
                      <p>At the forefront of server management,
dedicated technical support center. Our
management services and technical
including Dedicated Servers At the forefront
dedicated technical support center. Our
management services and technical
including Dedicated Servers</p>
                      </div>
                      
                      
                     
                      
                    </div>
                    <div class="ruby-col-6">
                      <div class="main-bx">
                      <div class="ruby-row">
                      <h3>Case Studies :</h3>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      <div class="ruby-col-6">
                          <div class="bxs"></div>
                      </div>
                      </div>
                      <div class="ruby-col-12">
                      <a href="#" style="float:right; margin-right:25px; margin-top:30px; color:#000; font-weight:500;">Know More <i class="fa fa-arrow-right"></i></a>
                      </div>
                    </div>
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li class="hidden-md"><a href="#">Company Profile</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 500px;">
                    <div class="ruby-row sb-menu">
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">Web Hosting Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Helpdesk Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Application Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">WordPress Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMJ.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Joomla Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWOO.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Woocommerce Development & Support</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      
                    </div>
                    
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>

                <li><a href="#">Partnership and Certificate</a>
                <div class="ruby-grid ruby-grid-lined" style="height: 500px;">
                    <div class="ruby-row sb-menu">
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMWH.svg">
                      </div>
                        <div class="ruby-col-11">
                      <a class="mns" href="#">cPanel Server Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                        </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMHS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Plesk and Other Web Panel Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr hov-bdr-1">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNMMAS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Disaster Recovery Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      <div class="ruby-col-6 hov-bdr">
                      <div class="ruby-col-1">
                      <img src="img/navigation-icon/SNWNWS.svg">
                      </div>
                      <div class="ruby-col-11">
                      <a class="mns" href="#">Application Server Management</a>
                      <p>Reduce and govern cloud spend without sacrificing flexibility</p>
                      </div>
                      </div>
                      
                      
                    </div>
                    
                  </div>
                  
                <span class="ruby-dropdown-toggle"></span></li>
              </ul>
              <ul>
                <li></li>
              </ul>
            </div>
          <span class="ruby-dropdown-toggle"></span>
        </li>
          

          

          

          

          <li class="ruby-menu-right"><a class="contact-btn" href="#">Contact Us</a></li>

          

          <!--<li class="ruby-menu-right ruby-menu-social ruby-menu-search"><a href="#"><i class="fa fa-search" aria-hidden="true"></i><span><input type="text" name="search" placeholder="Search.."></span></a></li>-->

        </ul>
      </div>
      <!-- END:   RUBY HORIZONTAL MENU -->
      <!-- ########################### -->
</div>
    </div>
<!-- END: RUBY DEMO HEADER -->

    <!-- Header End -->
    <script>
document.addEventListener("DOMContentLoaded", function() {
  const header = document.querySelector(".head");
  const toggleClass = "is-sticky";
  const scrollThreshold = 300; // Adjust this value as needed

  // Add a class to indicate the header is not sticky initially
  header.classList.add("not-sticky");

  window.addEventListener("scroll", () => {
    const currentScroll = window.pageYOffset;
    if (currentScroll > scrollThreshold && header.classList.contains("not-sticky")) {
      // Add sticky behavior when scrolled beyond the threshold
      header.classList.remove("not-sticky");
      header.classList.add(toggleClass);
    } else if (currentScroll <= scrollThreshold && header.classList.contains(toggleClass)) {
      // Remove sticky behavior when scrolled back up
      header.classList.remove(toggleClass);
      header.classList.add("not-sticky");
    }
  });
});
</script>
    <script>// Toggle to show and hide navbar menu
const navbarMenu = document.getElementById("menu-1");
const burgerMenu = document.getElementById("burger-1");

burgerMenu.addEventListener("click", () => {
  navbarMenu.classList.toggle("is-active");
  burgerMenu.classList.toggle("is-active");
});

// Toggle to show and hide dropdown menu
const dropdown = document.querySelectorAll(".dropdown-1");

dropdown.forEach((item) => {
  const dropdownToggle = item.querySelector(".dropdown-toggle-1");

  dropdownToggle.addEventListener("click", () => {
    const dropdownShow = document.querySelector(".dropdown-show-1");
    toggleDropdownItem(item);

    // Remove 'dropdown-show' class from other dropdown
    if (dropdownShow && dropdownShow != item) {
      toggleDropdownItem(dropdownShow);
    }
  });
});

// Function to display the dropdown menu
const toggleDropdownItem = (item) => {
  const dropdownContent = item.querySelector(".dropdown-content-1");

  // Remove other dropdown that have 'dropdown-show' class
  if (item.classList.contains("dropdown-show-1")) {
    dropdownContent.removeAttribute("style");
    item.classList.remove("dropdown-show-1");
  } else {
    // Added max-height on active 'dropdown-show' class
    dropdownContent.style.height = dropdownContent.scrollHeight + "px";
    item.classList.add("dropdown-show-1");
  }
};

// Fixed dropdown menu on window resizing
window.addEventListener("resize", () => {
  if (window.innerWidth > 992) {
    document.querySelectorAll(".dropdown-content-1").forEach((item) => {
      item.removeAttribute("style");
    });
    dropdown.forEach((item) => {
      item.classList.remove("dropdown-show-1");
    });
  }
});

// Fixed navbar menu on window resizing
window.addEventListener("resize", () => {
  if (window.innerWidth > 992) {
    if (navbarMenu.classList.contains("is-active")) {
      navbarMenu.classList.remove("is-active");
      burgerMenu.classList.remove("is-active");
    }
  }
});
</script>