<!DOCTYPE html>
<html lang="zxx"><head>
    <meta charset="UTF-8">
    <meta name="description" content="SNWN Tech Solution">
    <meta name="keywords" content="SNWN Tech Solution">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNWN Tech Solution</title>
    <!-- Google Font -->
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">!-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="icon" href="img/fevicon.png" sizes="192x192">	
<style>
    .page-header {
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    padding: 0px 0 0px 0 !important;
    z-index: 1;
    background: linear-gradient(to left,#23428B 0%,#1D094C 100%);
}
.logo:hover img {
  -webkit-transition: all 200ms ease-in;
  -webkit-transform: scale(1.1);
  -ms-transition: all 200ms ease-in;
  -ms-transform: scale(1.1);
  -moz-transition: all 200ms ease-in;
  -moz-transform: scale(1.1);
  transition: all 200ms ease-in;
  transform: scale(1.1);
}
.logos img:hover, .logo img:hover {
  -webkit-filter: none;
  filter: none;
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0" /></filter></svg>#filter');
  -webkit-filter: grayscale(0);
  filter: grayscale(0);
  opacity: 1;
}
.logos img{
  -webkit-filter: gray;
  filter: gray;
  -webkit-filter: grayscale(50);
  filter: grayscale(50);
  -webkit-transition: .4s ease-in-out;
  transition: .4s ease-in-out;
  opacity: .7;}

  .logos {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(8em, 1fr));
  list-style: none;
  margin: 0;
  overflow: hidden;
  padding: 0;
  margin-bottom:40px;
}

.logos > li {
  aspect-ratio: 1;
  background-color: #fff;
  box-shadow: 0 0 0 1px #ededed;
}

.logo {
  display: grid;
  padding: 0.4em;
  place-items: center;
}

.logo:hover img{
-webkit-transition: all 200ms ease-in;
    -webkit-transform: scale(1.1);
    -ms-transition: all 200ms ease-in;
    -ms-transform: scale(1.1);   
    -moz-transition: all 200ms ease-in;
    -moz-transform: scale(1.1);
    transition: all 200ms ease-in;
    transform: scale(1.1);
}

.logo figcaption {
  margin-top: 1em;
}

input[type="search"] {
  border: 0;
  font-size: 2rem;
  padding: 0.5em 2em;
  text-align: center;
  width: 100%;
}
h4.hpaline {
    display: none;
}
.hero-banner h1 {
    font-size: 70px;
}

.animated-text {
    animation-fill-mode: both;
    animation-duration: 1s;
}
.fadeInDown {
    animation-name: fadeInDown;
    visibility:visible;
}
.tabs {
  width: 100%;
 /* background-color: #09F;*/
  border-radius: 5px 5px 5px 5px;
}
ul#tabs-nav {
  list-style: none;
  margin: 0;
  padding: 137px 0 100px 0;
  overflow: auto;
}
ul#tabs-nav li {
  /*float: left;*/
  font-weight: bold;
  margin-right: 2px;
  padding: 8px 10px;
  border-radius: 5px 5px 5px 5px;
  /*border: 1px solid #d5d5de;
  border-bottom: none;*/
  cursor: pointer;
  display: inline-block;
}
ul#tabs-nav li:hover,
ul#tabs-nav li.active {
 /* background-color: #08E;*/
}
#tabs-nav li a {
  text-decoration: none;
  color: #FFF;
  font-size: 24px;
  padding: 0px 20px 0px 56px;
}
.tab-content {
  padding: 10px;
  /*border: 5px solid #09F;
  background-color: #FFF;*/
  height:452px;
}
.slidersection{
    background-color: #000;
    text-align: center;
}
.slidersection h2{color:#fff; padding: 73px 0px 3px 0; font-size: 55px;}
.slidersection p{color:#fff; padding: 50px 0 0 46px;     padding: 50px 80px;
    font-size: 24px; }


    

   

@keyframes scroll {
  0% {
    transform: translateX(0);
  }
  100% {
    transform: translateX(calc(-250px * 7));
  }
}
.slider {
  height: 100px;
  margin: auto;
  overflow: hidden;
  position: relative;
  width: auto;
}
.slider .slide-track {
  animation: scroll 40s linear infinite;
  display: flex;
  width: calc(250px * 14);
}
.slider .slide {
  height: 100px;
  width: 250px;
}



    





.btn-page {
  text-align: center;
  color: #fff;
  display: block;
  font-weight: 700;
  padding: 12px 10px;
  width: 200px;
  margin: 0 auto;
  margin-top: 5px;
  margin-bottom: 50px;
  border-radius: 45px;
}

.bttn-read-page {
  color: #fff !important;
  font-weight: 600;
  display: block;
  position: relative;
  margin-top: 25px;
  margin-left: 9px;
  background-color: #540113;
  width: 327px;
  padding: 15px 32px;
  border-radius: 31px;
  font-size: 25px;
  margin:0 auto;
}

.card {
  height: 77vh;
  color: 000;
  display: flex;
  justify-content: center;
  align-items: center;
  position: sticky;
  border: none !important;
  top: 0;
}

.card-content {
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 0;
}

.bw {
  background: url("#");
  background-size: cover;
  color: black;

  width: 100%;
  background-attachment: fixed;
  
}

.bgs-all{ background: url("img/snwnbcgrad.svg");
  background-size: cover;
  color: black;
  width: 100%;
  
  background-repeat: no-repeat; }

.tokyo {
  background: url("#");
  background-size: cover;
}

.mountains {
  background: url("#");
  background-size: cover;
}

.balloons {
  background: url("#");
  background-size: cover;
}


		</style>
	</head>
<body>
<?php include("header.php"); ?>
    <!-- Hero Section Begin -->
    <section class="hero-section">
    
	</video>
        <div class="bg-img">
            <div class="hero__item set-bg">
                <div class="container-fluid new-cnt-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text hero-banner">
                            
                                <h1 class="ttl-heading animated-text fadeInDown" style="color:#000; font-weight: 700;">FOCUS<br> <span class="rotate-text">DEVOPS</span></h1>
                                <h2>INNOVATIVE ADAPTIVE RELIABLE</h2>
                                    <h4>Lets us Provide the support you deserveLets us Provide the support you deserveLets us Provide the support you deserveLets us Provide the support you deserveLets us Provide the support you deserve</h4>
                                <a href="#" class="bttn-read-m" style="font-weight: 400;">Make it Stress Free <i class="fa fa-arrow-right arrw-rgt"></i></a>
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
                                <img src="../img/SNWN_hero.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
				
					<a href="#" class="Btn"><img class="mus" src="img/mouse accest.png"><i class="fa fa-arrow-down chev-dn"></i>
				</a>
					
            </div>
        </div>
    </section>
    <section class="slidersection">
    <div class="container"
><div class="tabs">
  
  <div id="tabs-content">
    <div id="tab1" class="tab-content">
      <h2>Beyond Expectations. Always Deliver.</h2>
      <p >"SNWN is a trusted IT development,DevOps, cloud architecture, and support partner. Our expert team
delivers unique solutions to reduce costs and enhance security for datacenters,
web hosting companies, and other enterprises. We provide comprehensive services from
consultation to implementation, ensuring robust, scalable, and secure infrastructure for your business success."</p>
    </div>
    <div id="tab2" class="tab-content">
        <h2>MILESTONE OF UNTIEDING GROWTH</h2>
    <div class="container-timel">
  <ul class="timeline">
    <li class="timeline__item" data-id="0">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label"><h3>2010</h3>Started</b>
    </li>
    <li class="timeline__item" data-id="1">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label-1">ISO Certification<h3>2012</h3></b>
    </li>
    <li class="timeline__item" data-id="2">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label"><h3>2014</h3>Support For Datacenter and web Hosting Companies Worldwide</b>
    </li>
    <li class="timeline__item" data-id="3">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
       
      <b class="label-2"><ul><li>+25 Team Members</li><li>Cloud Certifications</li></ul><h3>2018</h3></b>
    </li>
    <li class="timeline__item" data-id="4">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label"><h3>2020</h3><ul><li>+50 Project Completed</li><li>4+ Cloud Partners</li></ul></b>
    </li>
    <li class="timeline__item" data-id="5">
      <span class="bullet" aria-hidden="true"></span>
      <span class="line" aria-hidden="true"></span>
      
      <b class="label-3"><ul><li>+150k Website Managment</li><li>+20 Web Hosting Company Support</li><li>New Office with +150 Staff Capacity</li></ul><h3>2024</h3></b>
    </li>
  </ul>
</div>
    </div>
    <div id="tab3" class="tab-content">
      <h2 >Experience.  Expertise.  Excellence</h2>
      <p >"Our assured 24/7 active outsources support with the least response time as good as 30 Minutes
Support Tickets Response Time is the answer to your question of "Why choose us". We have a
team of certified engineers for quality assurance, hence providing you the best service. With a
wide range of packages we give you the privilege to choose the cost-effective package as per
your requirement."</p>
    </div>
    <div id="tab4" class="tab-content">
      <h2 >Jay</h2>
      <p >"I don't care if she's my cousin or not, I'm gonna knock those boots again tonight."</p>
    </div>
  </div> <!-- END tabs-content -->
  <ul id="tabs-nav">
    <li><a href="#tab1">About </a></li>
    <li><a href="#tab2">History</a></li>
    <li><a href="#tab3">Why us</a></li>
    <li><a href="#tab4">Partners</a></li>
  </ul> <!-- END tabs-nav -->
</div> <!-- END tabs -->
</div>
</section>
<div class="bgs-all">
<div>
  <div class="dev-sec-ops-new">
<div class="container">
      <div class="row">
      <div class="col-md-6">
      <h2 class="dv-sec">DevSecOps</h2>
      </div>
      <div class="col-md-6">
      <p class="dv-sec">From Plannning To Production SNWN Brings Your Team Together</p>
      </div>
      </div>
  <section class="card bw" >
    <div class="card-content">
       
<div class="bg-color-sec">
<div class="row">
    <div class="col-md-6">
      <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Plan</h6>
        <h3>STRATRGY</h3>
        <p>The Planning Phase Focuses On Defining Project Scope, Identifying Requirements, And Setting Objectives Collabration with Stakeholders Ensures Alignment on Goals. Effective Palnnings Established Timelines Resourses Allocations And Risk Managment Stratergies, Lying The Foundation For Succsefull devepment and Deployment.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Define Project Scope</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Idenfy key Requirement </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Set Clear Objectives</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Establish Timelines Resources</li>
        </ul>
    </div>
    <div class="col-md-6">
        <img src="img/complex-delivery-img-3.png">
</div>
</div>
<ul>
  <li>PLAN</li>
  <li>DEVELOPMENT</li>
  <li>DEVOPS</li>
  <li>ARCHITECT</li>
  <li>SECURITY</li>
</ul>
</div>
    </div>
  </section>
  <section class="card tokyo" >
    <div class="card-content">
    <div class="container">
        
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>-->
</div>
</div>
<div class="bg-color-sec-1">
      <div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Development</h6>
        <h3>CODE.</h3>
        <p>The Planning Phase Focuses On Defining Project Scope, Identifying Requirements, And Setting Objectives Collabration with Stakeholders Ensures Alignment on Goals. Effective Palnnings Established Timelines Resourses Allocations And Risk Managment Stratergies, Lying The Foundation For Succsefull devepment and Deployment.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Define Project Scope</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Idenfy key Requirement </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Set Clear Objectives</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Establish Timelines Resources</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
</div>


    </div>
  </section>
  <section class="card mountains" >
    <div class="card-content">
    <div class="container" style=" ">
    
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>-->
</div>
</div>
<div class="bg-color-sec-2">
<div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> DevOps</h6>
        <h3>INTERGRATION.</h3>
        <p>The Planning Phase Focuses On Defining Project Scope, Identifying Requirements, And Setting Objectives Collabration with Stakeholders Ensures Alignment on Goals. Effective Palnnings Established Timelines Resourses Allocations And Risk Managment Stratergies, Lying The Foundation For Succsefull devepment and Deployment.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Define Project Scope</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Idenfy key Requirement </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Set Clear Objectives</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Establish Timelines Resources</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
</div>
<ul>
  <li></li>
</ul>
    </div>
  </section>
  <section class="card balloons" >
    <div class="card-content">
    <div class="container" style=" ">
    
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>--->
</div>
</div>
<div class="bg-color-sec-3">
<div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Architect</h6>
        <h3>DESIGN.</h3>
        <p>The Planning Phase Focuses On Defining Project Scope, Identifying Requirements, And Setting Objectives Collabration with Stakeholders Ensures Alignment on Goals. Effective Palnnings Established Timelines Resourses Allocations And Risk Managment Stratergies, Lying The Foundation For Succsefull devepment and Deployment.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Define Project Scope</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Idenfy key Requirement </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Set Clear Objectives</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Establish Timelines Resources</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
    </div>
</div>
</div>
  </section>




  <section class="card balloons" >
    <div class="card-content">
    <div class="container" style=" ">
    
        <div class="row">
            <div class="col-md-6">
      <!--<h2>DevSecOps</h2>-->
</div>
<div class="col-md-6">
      
      <!--<p>From Plannning To Production SNWN Brings Your Team Together</p>--->
</div>
</div>
<div class="bg-color-sec-3">
<div class="row">
    <div class="col-md-6">
    <h6><img src="img/goals_4716351.svg" class="img-fluid img-icn"> Security</h6>
        <h3>LIFECYCLE.</h3>
        <p>The Planning Phase Focuses On Defining Project Scope, Identifying Requirements, And Setting Objectives Collabration with Stakeholders Ensures Alignment on Goals. Effective Palnnings Established Timelines Resourses Allocations And Risk Managment Stratergies, Lying The Foundation For Succsefull devepment and Deployment.
        </p>
        <ul>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Define Project Scope</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Idenfy key Requirement </li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Set Clear Objectives</li>
            <li><img style="width:25px; height:25px;" src="img/goals_4716351.svg"> Establish Timelines Resources</li>
        </ul>
    </div>
    <div class="col-md-6">
    <img src="img/complex-delivery-img-3.png">
</div>
</div>
    </div>
</div>
</div>
  </section>
</div>



<section class="service-section">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <h2>Driving Excellence Through Cloud Solutions</h2>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
                                <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
                                    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"/>
                                    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"/>
                                    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"/>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>DevOps</h5>
                                <p>Leveraging our extensive experience in implementing successful DevOps practices, our experts are uniquely qualified to assist you in automating, optimising, and standardising processes for seamless infrastructure deployment.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"/>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"/>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>AWS Consulting</h5>
                                <p>Harnessing profound expertise of nurturing a robust DevOps culture, we propel your business ahead of competitors with efficient resource utilisation, minimised costs, enhanced security, accelerated time-to-market, and superior ROI.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"/>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"/>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>Azure Consulting</h5>
                                <p>As a premier Azure consultant, we excel in secure, cost-effective Azure migrations, backed by round-the-clock global client support. Our seasoned Azure consultants deliver managed services, ensuring optimal efficiency and client satisfaction.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="80.776" height="73.413" viewBox="0 0 118.776 105.413">
                                <g id="syncing-document-4157_bf6a2cf1-26c8-4688-ab14-85836d2a0562" transform="translate(0 -3.5)" style="isolation: isolate">
                                    <path id="layer1" d="M53.694,23.5H24V84.187A2.413,2.413,0,0,0,26.413,86.6h65.7a2.413,2.413,0,0,0,2.413-2.413V30.923H53.694Z" transform="translate(20.541 17.117)" fill="#253858"/>
                                    <path id="layer2" d="M74.235,33.194V10.923H33.406V3.5H3.712V64.187A2.413,2.413,0,0,0,6.124,66.6H37.117V33.194ZM32.292,93.7l-9.279-9.279a3.712,3.712,0,0,0-5.2,5.2l2.969,2.969H11.135A3.712,3.712,0,0,1,7.423,88.87V85.158a3.712,3.712,0,1,0-7.423,0V88.87a11.135,11.135,0,0,0,11.135,11.135h9.651l-2.969,2.969a3.712,3.712,0,0,0,5.2,5.2l9.279-9.279,2.6-2.6-2.6-2.6Z" transform="translate(0 0)" fill="#8d8ea3"/>
                                    <path id="layer1-2" data-name="layer1" d="M68.955,13.608H59.3l2.969-2.969a3.712,3.712,0,0,0-5.2-5.2L47.8,14.722l-2.6,2.6,2.6,2.6L57.078,29.2a3.712,3.712,0,0,0,5.2-5.2L59.3,21.032h9.651a3.712,3.712,0,0,1,3.712,3.712v3.712a3.712,3.712,0,1,0,7.423,0V24.743A11.135,11.135,0,0,0,68.955,13.608Z" transform="translate(38.685 1.027)" fill="#253858"/>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>Web Hosting Support</h5>
                                <p>As a leading provider of web hosting support, we ensure secure, cost-effective hosting solutions with 24/7 global client assistance. Our expert team guarantees seamless website performance and client satisfaction.</p>
                                
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-3">
                            <div class="project">
								<div class="img">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"/>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"/>
                                </g>
                                </svg>
                                </div>
                                <div class="freatures-project">
                                <h5>Google Cloud Consulting</h5>
                                <p>SNWN's cloud experts will evaluate your existing infrastructure to determine your organisation's cloud readiness. Based on this assessment, we will create a Google Cloud adoption strategy, ensuring a smooth transition to the cloud.</p>
                               
                                </div>
                            </div>      
                        </div>
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"/>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"/>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>Application Management</h5>
                                <p>We deliver top-tier web application management support, ensuring high performance, accessibility, security, and reliability. Leveraging cutting-edge technology, we provide productive solutions that resonate with users.</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="73.776" height="65.804" viewBox="0 0 119.756 95.804">
                                <g id="private-folder-4136_9a670d06-eeb7-4508-8533-143953499f9e" transform="translate(-2 -8)">
                                    <path id="layer2" d="M77.919,53.935V37.967a15.967,15.967,0,1,0-31.935,0V53.935H38V89.861H85.9V53.935ZM65.943,75.89a3.992,3.992,0,0,1-7.984,0V67.906a3.992,3.992,0,0,1,7.984,0ZM53.967,53.935V37.967a7.984,7.984,0,1,1,15.967,0V53.935Z" transform="translate(35.853 13.943)" fill="#8d8ea3"/>
                                    <path id="layer1" d="M65.87,67.878V59.894h7.984V51.91a23.951,23.951,0,0,1,31.935-22.554V19.976H41.919V8H2V88.436a3.393,3.393,0,0,0,3.393,3.393H65.87Z" fill="#253858"/>
                                </g>
                                </svg>
								</div>
                                <div class="freatures-project">
                                <h5>WooCommerce Development</h5>
                                <p>With over 13 years of expertise in WooCommerce development, SNWN builds tailored extensions to meet user needs worldwide. Our dedicated team offers 24/7 support, ensuring your online store's optimal performance and growth.</p>
                               
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="project">
								<div class="img">
								<svg xmlns="http://www.w3.org/2000/svg" width="80" height="60.284" viewBox="0 0 145 111.284">
                                <g id="clouds-4457_d5542600-b7b1-4580-98ab-746ad7a032fa" transform="translate(-2 -9)" style="isolation: isolate">
                                    <path id="layer2" d="M103.607,45.175h-2.9a38.708,38.708,0,0,0-74.27,9.677h-.242a24.192,24.192,0,0,0,0,48.384h77.657a29.031,29.031,0,0,0-.242-58.061Z" transform="translate(0 17.048)" fill="#8d8ea3"/>
                                    <path id="layer1" d="M86.061,28.354a31.45,31.45,0,0,0-58.061,0A48.142,48.142,0,0,1,69.853,52.788a38.707,38.707,0,0,1,30.24,19.354A24.192,24.192,0,0,0,86.061,28.354Z" transform="translate(36.9)" fill="#253858"/>
                                </g>
                                </svg>
                                </div>
                                <div class="freatures-project">
                                <h5>Helpdesk Support Services</h5>
                                <p>We've skilled IT specialists armed with advanced tools, adept at swiftly resolving technical issues. Our helpdesk support services ensure seamless operations, providing expert assistance and guidance for customer queries and technical challenges.</p>
                                
                                </div>
                            </div>      
                        </div>
                    </div>
                    <div class="btn-page">
                    <a href="#" class="bttn-read-page">Expore All Solution</a> 
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <hr>
</section>



<section class="excellence-section">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <h2>Industries We Serve</h2>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <h5>Financial</h5>
                                <p>Out DevOps Experts Streamline Your Software Development Process, Enhancing Collaboration</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <h5>Ecommerce</h5>
                                <p>Out DevOps Experts Streamline Your Software Development Process, Enhancing Collaboration</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <h5>Healthcare</h5>
                                <p>Out DevOps Experts Streamline Your Software Development Process, Enhancing Collaboration</p>
                                
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <h5>Automative</h5>
                                <p>Out DevOps Experts Streamline Your Software Development Process, Enhancing Collaboration</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <h5>Government</h5>
                                <p>Out DevOps Experts Streamline Your Software Development Process, Enhancing Collaboration</p>
                                
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="project">
								
                                <div class="freatures-project">
                                <h5>Media & Entertainment</h5>
                                <p>Out DevOps Experts Streamline Your Software Development Process, Enhancing Collaboration</p>
                               
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>  
</section>



<div class="ptnrs">
<div class="container">
    <div class="row">
        <div class="col-md-3">
<h2>Our Partner :</h2>
</div>
<div class="col-md-9">
   <section class="logos-slider slider">
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
      <div class="slide"><img class="icon-navigate" src="img/navigation-icon/aws.png"></div>
</div>
</div>
</div>
</div>

<section class="hero-section">
    
	</video>
        <div class="ready-to-take">
            <div class="hero__item set-bg">
                <div class="container-fluid new-cnt-fluid">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="hero__text hero-banner">
                            
                                <h1 class="ttl-heading animated-text fadeInDown" style="color:#000; font-weight: 700;">READY TO TAKE YOUR BUSINESS TO THE NEXT LEVEL?</span></h1>
                                
                                    <h3>Schedule Afree consultation With our  team To Discover How we Can Help</h3>
                                <a href="#" class="bttn-read-m-retake">Connect With US <!--<i class="fa fa-arrow-right arrw-rgt"></i>--></a>
								<!--<h4 style="margin-top: 36px;">Strategic partnerships and certifications :</h4>-->
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="hero__img">
                                <img src="../img/SNWN_hero.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
				
					
					
            </div>
        </div>
    </section>
    
    <section class="awardone">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="img-award-1">
                            <img src="img/award/Experience.png"> 
                            <div class="cnt">
                            <h5>COST-EFECTIVENESS</h5>
                            <p>We offer affordable IT solution that help you reduce cost and improve your button line</p>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="img-award">
                            <img src="img/award/Assurance.png">
                            <div class="cnt"> 
                            <h5>INNOVATIVE TECHNOLOGY</h5>
                            <p>We offer affordable IT solution that help you reduce cost and improve your button line</p>
                            
                            </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                            <div class="img-award">
                            <img src="img/award/Satisfied.png"> 
                            <div class="cnt">
                            <h5>INDUSTRY EXPERTISE</h5>
                            <p>We offer affordable IT solution that help you reduce cost and improve your button line</p>
                            </div>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cmp-award">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="img-award">
                                <img src="img/award/Completed.png">
                                <div class="cnt">
                                <h5>SCALABILITY</h5>
                                <p>We offer affordable IT solution that help you reduce cost and improve your button line</p>
                                </div> 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<div class="case-studies">
<div class="container">
<div class="row">
	<div class="col-md-12">
		<h3>Case studies</h3>
		<p>Enterprises from around the globe trust SNWN. These are some of the most innovative stories from
our customers about how they are using SNWN solutions to make the transition to multi-cloud infrastructure.</p>
		<div class="row" style="border:1px solid #eeebeb; border-radius:8px;">
			<div class="col-md-4" style="padding: 0;">
				<div class="case-st-img" >
			<img src="img/giant-building-with-sun.png">
					</div>
			</div>
			<div class="col-md-8" style="padding: 0;">
				<div class="global-leading">
			<h3>A bank, the cloud, and a paradigm shift</h3>
				<p>Leading global bank uses SNWN Terraform to
reduce risk and quickly deliver new services during
their shift to the cloud.</p>
				<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
					</div>
			</div>
		</div>
	
	</div>
	</div>
	</div>
</div>



<div class="bank">
<div class="container">
<div class="row">
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/smart-discussing-meeting-report-phone.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/engineers-looking-bridge-construction.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
			
		</div>
	</div>
	<div class="col-md-4">
		<div class="bx-img">
			<img src="img/pay-goods-by-credit-card-through-smartphone-coffee-shop.png">
			<h5>A bank, the cloud, and
a paradigm shift</h5>
			<a href="#" class="clr">Read case study <i class="fa fa-arrow-right mg-right"></i></a>
		
		</div>
	</div>
	<a href="#" class="clr-1">Read our case studies <i class="fa fa-arrow-right mg-right"></i></a>
</div>	
</div>
</div>




<section class="contact-sec sec-pad">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
		  <div class="" style="margin-top: 112px;"></div>
        <h2>LOOKING FOR
AN IT PARTNER?</h2>
		  <h4>Contact us today for a free quote
within 3 business days</h4>
      </div>

      <div class="col-md-7">
		  <div class="frm">
        <form action="#" class="contFrm" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" placeholder="Your Name" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="email" name="email" placeholder="Email Address" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="tel" name="phone" placeholder="Phone Number" class="inptFld" required />
            </div>

            <div class="col-sm-6">
              <input type="text" name="sub" placeholder="Subject" class="inptFld" required />
            </div>

            <div class="col-12">
              <textarea class="inptFld" rows="" cols="" placeholder="Your Message..." required></textarea>
            </div>

            <div class="col-12">
              <input type="submit" name="submit" value="SUBMIT" class="inptBtn" />
            </div>
          </div>
        </form>
			  </div>
      </div>
    </div>

    

  </div>
</section>



<?php include("footer.php"); ?>









    

			
	
	
		
	
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
	<script src="js/submail.min.js"></script>
		
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    
    
    <script>//logo slider

$('.logos-slider').slick({
slidesToShow: 4,
slidesToScroll: 1,
autoplay: true,
autoplaySpeed: 1500,
arrows: false,
dots: false,
pauseOnHover: false,
responsive: [{
breakpoint: 768,
settings: {
slidesToShow: 3
}
}, {
breakpoint: 520,
settings: {
slidesToShow: 2
}
}]
});</script>
    
    
    
    <script>
        class Timeline {
  constructor() {
    this.activeItem = 0;
    this.timelineItems = document.querySelectorAll('.timeline__item');
    this.timer = null;
    this.currentEl = this.timelineItems[this.activeItem];
    
    this.registerEvents();
    this.initialize();
  }
  
  initialize() {
    this.currentEl.classList.add('timeline__item--active');    
  }
  
  start() {
    this.timer = window.setInterval(this.manageSteps.bind(this), 5000);
  }
  
  registerEvents() {
    this.timelineItems.forEach((item) => item.addEventListener('click', this.selectStep.bind(this)));
  }
  
  setActive() {
    this.currentEl.classList.add('timeline__item--active');
  }
  
  removeActive() {
    this.currentEl.classList.remove('timeline__item--active');
  }
  
  isActive() {
    return this.currentEl.classList.contains('timeline__item--active');
  }
  
  manageSteps() {    
    if(this.isActive()) {
      this.removeActive();
      this.activeItem = this.activeItem < this.timelineItems.length - 1 ? this.activeItem + 1 : 0;
    }
    
    this.setActive();
  }
  
  selectStep(evt) {
    let idSelectItem = evt.target.getAttribute('data-id');
    this.timelineItems[this.activeItem].classList.remove('timeline__item--active');
    
    window.clearInterval(this.timer);
    this.activeItem = parseInt(idSelectItem);
    this.timelineItems[idSelectItem].classList.add('timeline__item--active');
    this.start();
  }
}

const TimelineComponent = new Timeline();
TimelineComponent.start();
    </script>
        

    <script type="text/javascript">
        $(document).ready(function() {
            var text = ['DEVOPS', 'CLOUD SERVICES', 'SITE SERVICES', 'SUPPORT']; //Text to be shown
            var index = 0; //Starting index for rotating text
            var changeText = setInterval(function() {
                if (index == text.length - 1)
                    index = 0;
                else
                    index++;
                $('.rotate-text').animate({
                    "opacity": "0",
                    "margin-left": "10px"
                }, 500, function() {
                    $('.rotate-text').text(text[index]);
                    $('.rotate-text').animate({
                        "opacity": "1",
                        "margin-left": "0"
                    }, 500);
                });

            }, 2000);
        });
    </script>
     <script>
        // Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});
    </script>
<script>
(function($) { "use strict";

$(document).ready(function(){"use strict";

		//Scroll back to top

		var progressPath = document.querySelector('.progress-wrap path');
		var pathLength = progressPath.getTotalLength();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'none';
		progressPath.style.strokeDasharray = pathLength + ' ' + pathLength;
		progressPath.style.strokeDashoffset = pathLength;
		progressPath.getBoundingClientRect();
		progressPath.style.transition = progressPath.style.WebkitTransition = 'stroke-dashoffset 10ms linear';
		var updateProgress = function () {
			var scroll = $(window).scrollTop();
			var height = $(document).height() - $(window).height();
			var progress = pathLength - (scroll * pathLength / height);
			progressPath.style.strokeDashoffset = progress;
		}
		updateProgress();
		$(window).scroll(updateProgress);
		var offset = 50;
		var duration = 550;
		jQuery(window).on('scroll', function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.progress-wrap').addClass('active-progress');
			} else {
				jQuery('.progress-wrap').removeClass('active-progress');
			}
		});
		jQuery('.progress-wrap').on('click', function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})


	});

})(jQuery);

</script>
		<script>
        $(document).ready(function()
        {
                $(".nav-pills li").hover(function() {
                        $(".tab-pane").hide();
                        $(".nav-pills li").removeClass('active');
                        $(this).addClass("active");
                        var selected_tab = $(this).find("a").attr("href");
                        $(selected_tab).stop().fadeIn();
                        return false;
                });
        });
    </script>
    <script>
        $(document).ready(function () {
  $(".customer-logos").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});
    </script>
        	
	
</body>

</html>